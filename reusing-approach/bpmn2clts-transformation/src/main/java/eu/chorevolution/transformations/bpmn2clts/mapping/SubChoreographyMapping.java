/*
  * Copyright 2015 The CHOReVOLUTION project
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  *      http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */
package eu.chorevolution.transformations.bpmn2clts.mapping;

import org.eclipse.bpmn2.Choreography;
import org.eclipse.bpmn2.EndEvent;
import org.eclipse.bpmn2.SequenceFlow;
import org.eclipse.bpmn2.StartEvent;
import org.eclipse.bpmn2.SubChoreography;

import eu.chorevolution.modelingnotations.clts.CLTSModel;
import eu.chorevolution.modelingnotations.clts.ControlflowTransition;
import eu.chorevolution.modelingnotations.clts.State;
import eu.chorevolution.modelingnotations.clts.Transition;
import eu.chorevolution.transformations.bpmn2clts.TransformatorException;
import eu.chorevolution.transformations.bpmn2clts.TransformatorManager;
import eu.chorevolution.transformations.bpmn2clts.util.BPMNUtil;

/**
 * This class maps a BPMN {@link SubChoreography}.
 * 
 * In the {@link CLTSModel} thereisn't a specific sub-choreography element because the choreography related to the specific sub-choreography It will be collapsed in the root Choreography
 *
 */
public class SubChoreographyMapping {
	private SubChoreography bpmnSubChoreography;
	// this object identify the first and the last enabling elements of the sub-choreography the can be CLTS state or CLTS transition
	private Object cltsFistObject;
	private Object cltsLastObject;

	/**
	 * Constructor of the {@link SubChoreographyMapping} class
	 * 
	 * @param manager
	 *            {@link TransformatorManager} that manage the transformation
	 * @param bpmnSubChoreography
	 *            {@link SubChoreography} to be mapped
	 * @throws TransformatorException
	 *             an exception is thrown if the mapping cannot be performed
	 */
	public SubChoreographyMapping(TransformatorManager manager, SubChoreography bpmnSubChoreography) throws TransformatorException {
		this.bpmnSubChoreography = bpmnSubChoreography;
		Choreography relatedChoreographyForSubChoreography = manager.getBpmnManager().getChoreography(bpmnSubChoreography);
		cltsFistObject = manager.getMappings().get(BPMNUtil.getFirstAcceptableObject(relatedChoreographyForSubChoreography).getId());
		cltsLastObject = manager.getMappings().get(BPMNUtil.getLastAcceptableObject(relatedChoreographyForSubChoreography).getId());

		fixMappings(manager, relatedChoreographyForSubChoreography);

		manager.getMappings().put(bpmnSubChoreography.getId(), this);

	}

	public SubChoreography getBpmnSubChoreography() {
		return bpmnSubChoreography;
	}

	public Object getCltsFistObject() {
		return cltsFistObject;
	}

	public Object getCltsLastObject() {
		return cltsLastObject;
	}

	/**
	 * This method remove StartEvent, EndEvent and respectively outgoing and incoming transitions in the mappings related for the Sub Choreography
	 * 
	 * @param manager
	 *            {@link TransformatorManager} that manage the Choreography transformation
	 * @param relatedChoreographyForSubChoreography
	 *            {@link Choreography} for the BPMN {@link SubChoreography} element
	 * @throws TransformatorException
	 */
	private void fixMappings(TransformatorManager manager, Choreography relatedChoreographyForSubChoreography) throws TransformatorException {

		StartEvent bpmnStartEvent = BPMNUtil.getStartEvent(relatedChoreographyForSubChoreography);
		EndEvent bpmnEndEvent = BPMNUtil.getEndEvent(relatedChoreographyForSubChoreography);

		SequenceFlow outgoingStartEvent = bpmnStartEvent.getOutgoing().get(0);
		fixCLTSDanglingReference(manager, outgoingStartEvent);
		SequenceFlow incomingEndEvent = bpmnEndEvent.getIncoming().get(0);
		fixCLTSDanglingReference(manager, incomingEndEvent);

		manager.getMappings().remove(bpmnStartEvent.getId());
		manager.getMappings().remove(bpmnEndEvent.getId());

		manager.getMappings().remove(outgoingStartEvent.getId());
		manager.getMappings().remove(incomingEndEvent.getId());

	}

	/**
	 * This method fix the dangling reference problem. The problem is checked during the {@link CLTSModel} serialization because if a BPMN {@link SequenceFlow} element is removed during the transformation, the CLTS source and target state still hold the reference for the related CLTS {@link ControlflowTransition} in the outgoing and incoming transition list
	 * 
	 * @param manager
	 *            {link {@link TransformatorManager} that manage the transformation
	 * @param sequenceFlow
	 *            a BPMN {@link SequenceFlow} which must be removed in the mapping
	 * @throws TransformatorException
	 */
	private void fixCLTSDanglingReference(TransformatorManager manager, SequenceFlow sequenceFlow) throws TransformatorException {
		// throw an exception if sourceRef and targetRef they are not in the mapping. This means that they are not considered during the transformation
		if (!manager.getMappings().containsKey(sequenceFlow.getSourceRef().getId())) {
			throw new TransformatorException("Cannot fix dangling reference for the BPMN SequenceFlow (" + sequenceFlow.getId() + ") because the sourceRef (" + sequenceFlow.getSourceRef().getId() + ") has not been trasformated");
		} else if (!manager.getMappings().containsKey(sequenceFlow.getTargetRef().getId())) {
			throw new TransformatorException("Cannot fix dangling reference for the BPMN SequenceFlow (" + sequenceFlow.getId() + ") because the targetRef (" + sequenceFlow.getTargetRef().getId() + ") has not been trasformated");
		}

		Object cltsSource = null;
		Object cltsTarget = null;

		/*
		 * check if sourceRef and targetRef BPMN elements are sub-choreography.
		 * 
		 * if true, in the mappings we have an instance of BPMNSubChoreography hence the code gets cltsLastObject and cltsFistObject respectively
		 */
		if (sequenceFlow.getSourceRef() instanceof SubChoreography) {
			cltsSource = ((SubChoreographyMapping) manager.getMappings().get(sequenceFlow.getSourceRef().getId())).getCltsLastObject();
		} else {
			cltsSource = manager.getMappings().get(sequenceFlow.getSourceRef().getId());
		}

		if (sequenceFlow.getTargetRef() instanceof SubChoreography) {
			cltsTarget = ((SubChoreographyMapping) manager.getMappings().get(sequenceFlow.getTargetRef().getId())).getCltsFistObject();
		} else {
			cltsTarget = manager.getMappings().get(sequenceFlow.getTargetRef().getId());
		}

		/*
		 * throw an exception if cltsSource and cltsTarget they are not in the mapping. This means that they are not considered in the transformation.
		 *
		 * this exception will be thrown if cltsSource or cltsTarget are null. cltsSource or cltsTarget are null if bpmnSequenceFlow.getSourceRef() instanceof SubChoreography or bpmnSequenceFlow.getTargetRef() instanceof SubChoreography rispectively the case in which cltsSource and cltsTarget are not SubChoreography element it is already considered at the beginning of the method
		 */
		if (cltsSource == null) {
			throw new TransformatorException("Cannot fix dangling reference for the BPMN SequenceFlow (" + sequenceFlow.getId() + ") because it was not been created the related CLTS source");
		} else if (cltsTarget == null) {
			throw new TransformatorException("Cannot fix dangling reference for the BPMN SequenceFlow (" + sequenceFlow.getId() + ") because it was not been created the related CLTS target");
		}

		// if cltsSource is a Transition then the target state of the transition is considered
		if (cltsSource instanceof Transition)
			cltsSource = ((Transition) manager.getMappings().get(sequenceFlow.getSourceRef().getId())).getTarget();
		// if cltsTarget is a Transition then the source state of the transition is considered
		if (cltsTarget instanceof Transition)
			cltsTarget = ((Transition) manager.getMappings().get(sequenceFlow.getTargetRef().getId())).getSource();

		// remove the following reference before remove the sequenceFlow
		Transition cltsTransition = (Transition) manager.getMappings().get(sequenceFlow.getId());
		((State) cltsSource).getOutgoingTransition().remove(cltsTransition);
		((State) cltsTarget).getIncomingTransition().remove(cltsTransition);
	}

}
