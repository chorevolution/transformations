/*
  * Copyright 2015 The CHOReVOLUTION project
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  *      http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */
package eu.chorevolution.transformations.bpmn2clts.mapping;

import org.eclipse.bpmn2.ExclusiveGateway;
import org.eclipse.bpmn2.GatewayDirection;

import eu.chorevolution.modelingnotations.clts.CltsFactory;
import eu.chorevolution.modelingnotations.clts.State;
import eu.chorevolution.transformations.bpmn2clts.TransformatorException;
import eu.chorevolution.transformations.bpmn2clts.TransformatorManager;
import eu.chorevolution.transformations.bpmn2clts.util.CLTSUtil;

/**
 * This class maps a BPMN {@link ExclusiveGateway} into CLTS {@link State}
 *
 */
public class ExclusiveGatewayMapping {
	private ExclusiveGateway bpmnExclusiveGateway;
	private State cltsState;

	/**
	 * Constructor of the {@link ExclusiveGatewayMapping} class
	 * 
	 * @param manager
	 *            {@link TransformatorManager} that manage the transformation
	 * @param bpmnExclusiveGateway
	 *            {@link ExclusiveGateway} to be mapped
	 * @throws TransformatorException
	 *             an exception is thrown if the mapping cannot be performed
	 */
	public ExclusiveGatewayMapping(TransformatorManager manager, ExclusiveGateway bpmnExclusiveGateway) throws TransformatorException {
		this.bpmnExclusiveGateway = bpmnExclusiveGateway;
		if (bpmnExclusiveGateway.getGatewayDirection().getValue() == GatewayDirection.DIVERGING_VALUE) {
			mappingExclusiveGatewayDiverging();
		} else if (bpmnExclusiveGateway.getGatewayDirection().getValue() == GatewayDirection.CONVERGING_VALUE) {
			mappingExclusiveGatewayConverging();
		} else {
			throw new TransformatorException("the GatewayDirection not specified for Exclusive Gateway: " + bpmnExclusiveGateway.getName());
		}

		manager.getMappings().put(bpmnExclusiveGateway.getId(), cltsState);
	}

	private void mappingExclusiveGatewayDiverging() {
		cltsState = CltsFactory.eINSTANCE.createAlternative();
		cltsState.setName(CLTSUtil.getUniqueElementName());
	}

	private void mappingExclusiveGatewayConverging() {
		cltsState = CltsFactory.eINSTANCE.createSimpleState();
		cltsState.setName(CLTSUtil.getUniqueElementName());
	}

	public ExclusiveGateway getBpmnExclusiveGateway() {
		return bpmnExclusiveGateway;
	}

	public State getCltsState() {
		return cltsState;
	}

}
