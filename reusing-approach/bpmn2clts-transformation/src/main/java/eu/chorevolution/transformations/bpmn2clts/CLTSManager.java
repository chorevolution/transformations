/*
  * Copyright 2015 The CHOReVOLUTION project
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  *      http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */
package eu.chorevolution.transformations.bpmn2clts;

import java.util.HashMap;
import java.util.Map;

import eu.chorevolution.modelingnotations.clts.CLTSModel;
import eu.chorevolution.modelingnotations.clts.Choreography;
import eu.chorevolution.modelingnotations.clts.CltsFactory;
import eu.chorevolution.modelingnotations.clts.Participant;

/**
 * This class provides a manager for a {@link CLTSModel}
 * 
 */
public class CLTSManager {

	private CLTSModel cltsModel;
	
	private Map<String , Choreography> choreographies;
	private Map<String, Participant> participants;

	public CLTSManager(String name) {
		participants = new HashMap<String, Participant>();
		choreographies = new HashMap<String, Choreography>();
		cltsModel = CltsFactory.eINSTANCE.createCLTSModel();
		cltsModel.setName(name);
	}
	
	public CLTSModel createCltsModel() {
		cltsModel.getChoreographies().addAll(choreographies.values());
		return cltsModel;
	}

	public Map<String, Participant> getParticipants() {
		return participants;
	}


	public Participant getParticipant(org.eclipse.bpmn2.Participant bpmnParticipant) {
		if (participants.containsKey(bpmnParticipant.getId())) {
			return participants.get(bpmnParticipant.getId());
		} else {
			Participant participant = CltsFactory.eINSTANCE.createParticipant();
			participant.setName(bpmnParticipant.getName());
			participants.put(bpmnParticipant.getId(), participant);
			return participant;
		}
	}
	
	public Map<String, Choreography> getChoreographies() {
		return choreographies;
	}
	
}
