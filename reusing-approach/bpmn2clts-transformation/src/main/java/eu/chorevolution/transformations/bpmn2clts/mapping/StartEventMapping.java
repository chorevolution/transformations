/*
  * Copyright 2015 The CHOReVOLUTION project
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  *      http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */
package eu.chorevolution.transformations.bpmn2clts.mapping;

import eu.chorevolution.modelingnotations.clts.StartEvent;
import eu.chorevolution.transformations.bpmn2clts.TransformatorManager;

/**
 * This class maps a BPMN {@link org.eclipse.bpmn2.StartEvent} into CLTS {@link StartEvent}
 *
 */
public class StartEventMapping {
	private org.eclipse.bpmn2.StartEvent bpmnStartEvent;
	private StartEvent cltsStartEvent;

	/**
	 * Constructor of the {@link StartEventMapping} class
	 * 
	 * @param manager
	 *            {@link TransformatorManager} that manage the transformation
	 * @param bpmnStartEvent
	 *            {@link org.eclipse.bpmn2.StartEvent} to be mapped
	 */
	public StartEventMapping(TransformatorManager manager, org.eclipse.bpmn2.StartEvent bpmnStartEvent) {
		this.bpmnStartEvent = bpmnStartEvent;
		cltsStartEvent = manager.getChoreographyMapping().getCltsStartEvent();

		manager.getMappings().put(bpmnStartEvent.getId(), cltsStartEvent);
	}

	public org.eclipse.bpmn2.StartEvent getBpmnStartEvent() {
		return bpmnStartEvent;
	}

	public StartEvent getCltsStartEvent() {
		return cltsStartEvent;
	}

}
