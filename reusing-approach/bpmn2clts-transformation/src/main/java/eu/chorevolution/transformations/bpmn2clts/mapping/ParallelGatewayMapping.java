/*
  * Copyright 2015 The CHOReVOLUTION project
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  *      http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */
package eu.chorevolution.transformations.bpmn2clts.mapping;

import org.eclipse.bpmn2.GatewayDirection;
import org.eclipse.bpmn2.ParallelGateway;

import eu.chorevolution.modelingnotations.clts.CltsFactory;
import eu.chorevolution.modelingnotations.clts.State;
import eu.chorevolution.transformations.bpmn2clts.TransformatorException;
import eu.chorevolution.transformations.bpmn2clts.TransformatorManager;
import eu.chorevolution.transformations.bpmn2clts.util.CLTSUtil;

/**
 * This class manage a BPMN {@link ParallelGateway} into CLTS {@link State}
 *
 */
public class ParallelGatewayMapping {
	private ParallelGateway bpmnParallelGateway;
	private State cltsState;

	/**
	 * Constructor of the {@link ParallelGatewayMapping} class
	 * 
	 * @param manager
	 *            {@link TransformatorManager} that manage the transformation
	 * @param bpmnParallelGateway
	 *            {@link ParallelGateway} to be mapped
	 * @throws TransformatorException
	 *             an exception is thrown if the mapping cannot be performed
	 */
	public ParallelGatewayMapping(TransformatorManager manager, ParallelGateway bpmnParallelGateway) throws TransformatorException {
		this.bpmnParallelGateway = bpmnParallelGateway;
		if (bpmnParallelGateway.getGatewayDirection().getValue() == GatewayDirection.DIVERGING_VALUE) {
			mappingParallelGatewayDiverging();
		} else if (bpmnParallelGateway.getGatewayDirection().getValue() == GatewayDirection.CONVERGING_VALUE) {
			mappingParallelGatewayConverging();
		} else {
			throw new TransformatorException("the GatewayDirection not specified for Parallel Gateway: " + bpmnParallelGateway.getName());
		}

		manager.getMappings().put(bpmnParallelGateway.getId(), cltsState);
	}

	private void mappingParallelGatewayDiverging() {
		cltsState = CltsFactory.eINSTANCE.createFork();
		cltsState.setName(CLTSUtil.getUniqueElementName());
	}

	private void mappingParallelGatewayConverging() {
		cltsState = CltsFactory.eINSTANCE.createJoin();
		cltsState.setName(CLTSUtil.getUniqueElementName());
	}

	public ParallelGateway getBpmnParallelGateway() {
		return bpmnParallelGateway;
	}

	public State getCltsState() {
		return cltsState;
	}

}
