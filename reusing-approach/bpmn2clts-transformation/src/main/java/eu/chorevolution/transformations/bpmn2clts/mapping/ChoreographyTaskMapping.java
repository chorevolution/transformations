/*
  * Copyright 2015 The CHOReVOLUTION project
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  *      http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */
package eu.chorevolution.transformations.bpmn2clts.mapping;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.bpmn2.ChoreographyTask;
import org.eclipse.bpmn2.MessageFlow;

import eu.chorevolution.modelingnotations.clts.CltsFactory;
import eu.chorevolution.modelingnotations.clts.Message;
import eu.chorevolution.modelingnotations.clts.SimpleState;
import eu.chorevolution.modelingnotations.clts.TaskTransition;
import eu.chorevolution.transformations.bpmn2clts.BPMNMessageType;
import eu.chorevolution.transformations.bpmn2clts.TransformatorException;
import eu.chorevolution.transformations.bpmn2clts.TransformatorManager;
import eu.chorevolution.transformations.bpmn2clts.util.BPMNUtil;
import eu.chorevolution.transformations.bpmn2clts.util.CLTSUtil;

/**
 * This class maps a BPMN {@link ChoreographyTask} into CLTS
 * {@link TaskTransition}
 *
 */
public class ChoreographyTaskMapping {
	private ChoreographyTask bpmnChoreographyTask;
	private TaskTransition cltsTaskTransition;

	/**
	 * Constructor of the {@link ChoreographyTaskMapping} class
	 * 
	 * @param manager
	 *            {@link TransformatorManager} that manage the transformation
	 * @param bpmnChoreographyTask
	 *            {@link ChoreographyTask} to be mapped
	 * @throws TransformatorException
	 *             an exception is thrown if the mapping cannot be performed
	 */
	public ChoreographyTaskMapping(TransformatorManager manager, ChoreographyTask bpmnChoreographyTask)
			throws TransformatorException {
		this.bpmnChoreographyTask = bpmnChoreographyTask;
		// create CLTS task transition
		cltsTaskTransition = CltsFactory.eINSTANCE.createTaskTransition();
		cltsTaskTransition.setTaskName(bpmnChoreographyTask.getName());

		// create CLTS source state
		SimpleState source = CltsFactory.eINSTANCE.createSimpleState();
		source.setName(CLTSUtil.getUniqueElementName());
		cltsTaskTransition.setSource(source);

		// create CLTS target state
		SimpleState target = CltsFactory.eINSTANCE.createSimpleState();
		target.setName(CLTSUtil.getUniqueElementName());
		cltsTaskTransition.setTarget(target);

		// create CLTS initiating participant
		org.eclipse.bpmn2.Participant bpmnInitiatingParticipant = BPMNUtil
				.getInitiatingParticipant(bpmnChoreographyTask);
		cltsTaskTransition.setInitiatingParticipant(manager.getCltsManager().getParticipant(bpmnInitiatingParticipant));

		// create CLTS receiving participant
		org.eclipse.bpmn2.Participant bpmnReceivingParticipant = BPMNUtil.getReceivingParticipant(bpmnChoreographyTask);
		cltsTaskTransition.setReceivingParticipant(manager.getCltsManager().getParticipant(bpmnReceivingParticipant));

		// create CLTS I/O message

		for (MessageFlow messageFlow : bpmnChoreographyTask.getMessageFlowRef()) {
			Message message = CltsFactory.eINSTANCE.createMessage();

			message.setContent(getContentForBPMNMessage(manager,messageFlow));
					
			//set input or output messagge
			if (messageFlow.getSourceRef().equals(bpmnInitiatingParticipant)
					&& messageFlow.getTargetRef().equals(bpmnReceivingParticipant)) {
				// create CLTS input message
				cltsTaskTransition.setInMessage(message);
			} else if (messageFlow.getSourceRef().equals(bpmnReceivingParticipant)
					&& messageFlow.getTargetRef().equals(bpmnInitiatingParticipant)) {
				// create CLTS output message
				cltsTaskTransition.setOutMessage(message);
			}
		}
		manager.getMappings().put(bpmnChoreographyTask.getId(), cltsTaskTransition);
	}

	public ChoreographyTask getBpmnChoreographyTask() {
		return bpmnChoreographyTask;
	}

	public TaskTransition getCltsTaskTransition() {
		return cltsTaskTransition;
	}
	
	private String getContentForBPMNMessage(TransformatorManager manager, MessageFlow messageFlow) throws TransformatorException{
		BPMNMessageType bpmnMessageType = manager.getBpmnManager().getBPMNMessageTypes(messageFlow.getMessageRef().getName());
		List<BPMNMessageType> allEmbeddedTypes = createContents(bpmnMessageType);
		
		StringBuilder builder = new StringBuilder(bpmnMessageType.getContent()); 
		
		for (BPMNMessageType embeddedBpmnMessageType : allEmbeddedTypes) {
			builder.append(embeddedBpmnMessageType.getContent());
		}
				
		return builder.toString();
	}

	private List<BPMNMessageType> createContents(BPMNMessageType bpmnMessageType){
		List<BPMNMessageType> results = new ArrayList<BPMNMessageType>();
		
		for (BPMNMessageType embeddedBpmnMessageType : bpmnMessageType.getTypes()) {
			for (BPMNMessageType xxxx : createContents(embeddedBpmnMessageType)){
				if(!results.contains(xxxx)){
					results.add(xxxx);	
				}
			}
			
			if(!results.contains(embeddedBpmnMessageType)){
				results.add(embeddedBpmnMessageType);				
			}
			
		}
		
		return results;	
		
	}
	

}
