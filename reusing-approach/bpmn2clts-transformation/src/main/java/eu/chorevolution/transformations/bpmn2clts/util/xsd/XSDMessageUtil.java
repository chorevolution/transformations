/*
  * Copyright 2015 The CHOReVOLUTION project
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  *      http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */
package eu.chorevolution.transformations.bpmn2clts.util.xsd;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Attr;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import eu.chorevolution.transformations.bpmn2clts.BPMNMessageType;
import eu.chorevolution.transformations.bpmn2clts.TransformatorException;
import eu.chorevolution.transformations.bpmn2clts.util.LoadPropertiesFile;

public class XSDMessageUtil {
	private static final String CONFIG = "xsdmessagetypequery.config.properties";
	private static final LoadPropertiesFile config;
	
	static {
		config = new LoadPropertiesFile(CONFIG);
	}
	
	
	public static List<BPMNMessageType> getImports(File location) throws TransformatorException {

		List<BPMNMessageType> bpmnMessageTypes = new ArrayList<BPMNMessageType>();

		try {
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			dbf.setValidating(false);
			dbf.setNamespaceAware(true);
			DocumentBuilder builder = dbf.newDocumentBuilder();

			dbf.setNamespaceAware(false);
			XPathFactory xpf = XPathFactory.newInstance();

			XPath xpath = xpf.newXPath();
			NamespaceResolver namespaceResolver = new NamespaceResolver();
			xpath.setNamespaceContext(namespaceResolver);

			// get document			
			Document document = builder.parse(location);
			scanNamespaces(document, xpath, namespaceResolver);

			// the document is ready to readed
			
			//get all and store complextype type
			NodeList complexTypes = (NodeList) xpath.evaluate(config.get("bpmn.messagetype.xsd.complex.or.simple"), document, XPathConstants.NODESET);
			
			for (int i = 0; i < complexTypes.getLength(); ++i) {
				BPMNMessageType bpmnMessageType = createBPMNMessageType(xpath, document, complexTypes.item(i));
				bpmnMessageTypes.add(bpmnMessageType);
			}
			

		} catch (ParserConfigurationException e) {
			throw new TransformatorException("Error to load and/or parse the BPMN Import file: " + location.getAbsolutePath());
		} catch (SAXException e) {
			throw new TransformatorException("Error to load and/or parse the BPMN Import file: " + location.getAbsolutePath());
		} catch (IOException e) {
			throw new TransformatorException("Error to load and/or parse the BPMN Import file: " + location.getAbsolutePath());
		} catch (XPathExpressionException e) {
			throw new TransformatorException("Error to load and/or parse the BPMN Import file: " + location.getAbsolutePath());
		}

		return bpmnMessageTypes;
	}
	
	private static BPMNMessageType createBPMNMessageType(XPath xpath,Document document, Node node) throws TransformatorException, XPathExpressionException{
		BPMNMessageType bpmnMessageType = new BPMNMessageType(node.getAttributes().getNamedItem("name").getTextContent(),nodeElementToString(node));
		bpmnMessageType.setTypes(setAllTypes(xpath, document, bpmnMessageType));
		
		return bpmnMessageType;
		
	}

	private static List<BPMNMessageType> setAllTypes(XPath xpath,Document document, BPMNMessageType bpmnMessageType) throws TransformatorException, XPathExpressionException{
		// get all embedded types
		List<BPMNMessageType> embeddedMessageTypes = new ArrayList<BPMNMessageType>();
		NodeList embeddedTypes = (NodeList) xpath.evaluate("/xsd:schema/xsd:complexType[@name='"+bpmnMessageType.getName()+"']//xsd:element[starts-with(@type, 'tns:')] | /xsd:schema/xsd:simpleType[@name='"+bpmnMessageType.getName()+"']//xsd:element[starts-with(@type, 'tns:')]", document, XPathConstants.NODESET);
		for (int j = 0; j < embeddedTypes.getLength(); ++j) {
			String embeddedTypeName= embeddedTypes.item(j).getAttributes().getNamedItem("type").getTextContent().replace(config.get("bpmn.messagetype.embedded.schemaname"), "");
			//search complextype or simple for embeddedTypeName
			Node embeddedComplexTypes = (Node) xpath.evaluate("/xsd:schema/xsd:complexType[@name='"+embeddedTypeName+"'] | /xsd:schema/xsd:simpleType[@name='"+embeddedTypeName+"']", document, XPathConstants.NODE);
			BPMNMessageType bpmnMessageComplexType = createBPMNMessageType( xpath, document, embeddedComplexTypes);
			if (!embeddedMessageTypes.contains(bpmnMessageComplexType)){
				embeddedMessageTypes.add(bpmnMessageComplexType);
			}	
		}
		return embeddedMessageTypes;
	}
	
	private static void scanNamespaces(Document document, XPath xpath, NamespaceResolver namespaceResolver) throws TransformatorException {
		int c = 0;
		try {
			NodeList nm = (NodeList) xpath.evaluate("//*/namespace::*", document, XPathConstants.NODESET);
			for (int i = 0; i < nm.getLength(); ++i) {
				Attr a = (Attr) nm.item(i);
				String prfx = a.getName();
				prfx = prfx.equals("xmlns") ? "def" : prfx.substring(6);
				while (!namespaceResolver.getNamespaceURI(prfx).equals("")) {
					prfx = prfx + ++c;
				}
				namespaceResolver.addBinding(prfx, a.getValue());
			}
		} catch (XPathExpressionException e) {
			throw new TransformatorException("Error to scanning Namespaces in the BPMN Import file: " + e.getMessage());
		}
	}

	private static String nodeElementToString(Node node) throws TransformatorException {
		StringWriter writer = new StringWriter();
		Transformer transformer;
		String nodeString = "";
		try {
			transformer = TransformerFactory.newInstance().newTransformer();
			transformer.transform(new DOMSource(node), new StreamResult(writer));
			nodeString = writer.toString();
			nodeString = nodeString.substring(nodeString.indexOf(">") + 1);
			
		} catch (TransformerConfigurationException | TransformerFactoryConfigurationError e1) {
			throw new TransformatorException("Error to create a String for the node: " + node.getAttributes().getNamedItem("name").getTextContent());
		} catch (TransformerException e) {
			throw new TransformatorException("Error to create a String for the node: " + node.getAttributes().getNamedItem("name").getTextContent());
		}catch (DOMException e) {
			throw new TransformatorException("Error to create a String for the node: " + node.getAttributes().getNamedItem("name").getTextContent());
		}finally {
			writer.flush();
			try {
				writer.close();
			} catch (IOException e) {
			}
		}

		return nodeString;
	}
}
