/*
  * Copyright 2015 The CHOReVOLUTION project
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  *      http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */
package eu.chorevolution.transformations.bpmn2clts.util;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.bpmn2.Choreography;
import org.eclipse.bpmn2.ChoreographyActivity;
import org.eclipse.bpmn2.Definitions;
import org.eclipse.bpmn2.DocumentRoot;
import org.eclipse.bpmn2.EndEvent;
import org.eclipse.bpmn2.FlowElement;
import org.eclipse.bpmn2.Participant;
import org.eclipse.bpmn2.StartEvent;
import org.eclipse.bpmn2.SubChoreography;
import org.eclipse.bpmn2.util.Bpmn2ResourceFactoryImpl;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;

import eu.chorevolution.transformations.bpmn2clts.TransformatorException;

/**
 * This class provides static utility methods for manipulate a BPMN model
 * 
 */
public class BPMNUtil {

	/**
	 * Loads a BPMN2 model from the specified {@link URI}, checking if it can be
	 * loaded and it contains {@link Choreography}.
	 * <p>
	 * Returns a list of {@link Choreography} founded in the BPMN2 model.
	 * <p>
	 * A {@link TransformatorException} is thrown if the BPMN2 model can not be
	 * loaded. A {@link TransformatorException} is thrown if the BPMN2 model is
	 * loaded but not contains any {@link Choreography}.
	 * 
	 * @param bpmnURI
	 *            the {@link File} that represents the BPMN2 Choreography
	 * @return the list of {@link Choreography} founded in BPMN2 the model.
	 * @throws TransformatorException
	 *             if the BPMN2 file can not be loaded or the BPMN2 model not
	 *             contains any Choreography.
	 */
	public static List<Choreography> loadBPMNModel(File bpmnFile) throws TransformatorException {
		URI bpmnURI = URI.createURI(bpmnFile.toURI().toString());

		List<Choreography> choreographies = new ArrayList<Choreography>();

		// register the BPMN2ResourceFactory in Factory registry
		Resource.Factory.Registry reg = Resource.Factory.Registry.INSTANCE;
		reg.getExtensionToFactoryMap().put("bpmn", new Bpmn2ResourceFactoryImpl());
		reg.getExtensionToFactoryMap().put("bpmn2", new Bpmn2ResourceFactoryImpl());

		// load the resource and resolve
		ResourceSet resourceSet = new ResourceSetImpl();
		Resource resource = resourceSet.createResource(bpmnURI);

		try {
			// load the resource
			resource.load(null);
			// avax.xml.XMLConstants.W3C_XML_SCHEMA_NS_URI
			// get all Choreography
			EObject root = resource.getContents().get(0);
			Definitions definitions;

			if (root instanceof DocumentRoot) {
				definitions = ((DocumentRoot) root).getDefinitions();
			} else {
				definitions = (Definitions) root;
			}

			for (EObject definition : definitions.eContents()) {
				if (definition instanceof Choreography) {
					choreographies.add((Choreography) definition);
				}
			}

		} catch (IOException e) {
			throw new TransformatorException("Error to load the resource: " + resource.getURI().toFileString());
		}

		if (choreographies.isEmpty()) {
			throw new TransformatorException(
					"None choreography founded in the model: " + resource.getURI().toFileString());
		}

		return choreographies;
	}

	public static Participant getInitiatingParticipant(ChoreographyActivity choreographyActivity)
			throws TransformatorException {
		if (choreographyActivity.getInitiatingParticipantRef() != null)
			return choreographyActivity.getInitiatingParticipantRef();
		throw new TransformatorException(
				"None Initiating participant founded in the choreography activity: " + choreographyActivity.getName());

	}

	public static Participant getReceivingParticipant(ChoreographyActivity choreographyActivity)
			throws TransformatorException {
		for (Participant participant : choreographyActivity.getParticipantRefs()) {
			if (!participant.equals(choreographyActivity.getInitiatingParticipantRef())) {
				return participant;
			}
		}
		throw new TransformatorException(
				"None target participant founded in the choreography activity: " + choreographyActivity.getName());
	}

	/* utilities to manipulate sub-choreography */

	public static StartEvent getStartEvent(Choreography subChoreography) throws TransformatorException {
		// we assume that the sub-choreography has only one start event
		for (FlowElement flowElement : subChoreography.getFlowElements()) {
			if (flowElement instanceof StartEvent) {
				return (StartEvent) flowElement;
			}
		}
		throw new TransformatorException(
				"None start event founded in the sub-choreography: " + subChoreography.getName());
	}

	public static EndEvent getEndEvent(Choreography subChoreography) throws TransformatorException {
		// we assume that the sub-choreography has only one end event
		for (FlowElement flowElement : subChoreography.getFlowElements()) {
			if (flowElement instanceof EndEvent) {
				return (EndEvent) flowElement;
			}
		}
		throw new TransformatorException(
				"None end event founded in the sub-choreography: " + subChoreography.getName());
	}

	/**
	 * This method try to detect in the {@link SubChoreography} the first
	 * acceptable element. This element will be used during the transformation
	 * when the transformator needed to collapse the sub-choreography
	 * 
	 * @param subChoreography
	 *            the {@link Choreography} that refers the
	 *            {@link SubChoreography}
	 * @return {@link FlowElement} the first acceptable element used when the
	 *         transformator collapse the sub-choreography
	 * @throws TransformatorException
	 *             a {@link TransformatorException} is thrown if none first
	 *             acceptable element is founded in the sub-choreography
	 */
	public static FlowElement getFirstAcceptableObject(Choreography subChoreography) throws TransformatorException {
		// we assume that the start event of the sub-choreography has only one
		// outgoing transition
		try {
			return getStartEvent(subChoreography).getOutgoing().get(0).getTargetRef();
		} catch (TransformatorException e) {
			throw new TransformatorException(
					"None first acceptable element is founded in the sub-choreography: " + subChoreography.getName());
		}
	}

	/**
	 * This method try to detect in the {@link SubChoreography} the last
	 * acceptable element. This element will be used during the transformation
	 * when the transformator needed to collapse the sub-choreography
	 * 
	 * @param subChoreography
	 *            the {@link Choreography} that refers the
	 *            {@link SubChoreography}
	 * @return {@link FlowElement} the last acceptable element used when the
	 *         transformator collapse the sub-choreography
	 * @throws TransformatorException
	 *             a {@link TransformatorException} is thrown if none last
	 *             acceptable element is founded in the sub-choreography
	 */
	public static FlowElement getLastAcceptableObject(Choreography subChoreography) throws TransformatorException {
		// we assume that the end event of the sub-choreography has only one
		// incoming transition
		try {
			return getEndEvent(subChoreography).getIncoming().get(0).getSourceRef();
		} catch (TransformatorException e) {
			throw new TransformatorException(
					"None last accettable element is founded in the sub-choreography: " + subChoreography.getName());
		}
	}

}
