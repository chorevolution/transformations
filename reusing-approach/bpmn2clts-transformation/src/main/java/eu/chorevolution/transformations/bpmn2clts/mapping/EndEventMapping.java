/*
  * Copyright 2015 The CHOReVOLUTION project
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  *      http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */
package eu.chorevolution.transformations.bpmn2clts.mapping;

import eu.chorevolution.modelingnotations.clts.EndEvent;
import eu.chorevolution.transformations.bpmn2clts.TransformatorManager;

/**
 * This class maps a BPMN {@link org.eclipse.bpmn2.EndEvent} into CLTS {@link EndEvent}
 *
 */
public class EndEventMapping {
	private org.eclipse.bpmn2.EndEvent bpmnEndEvent;
	private EndEvent cltsEndEvent;

	/**
	 * Constructor of the {@link EndEventMapping} class
	 * 
	 * @param manager
	 *            {@link TransformatorManager} that manage the transformation
	 * @param bpmnEndEvent
	 *            {@link org.eclipse.bpmn2.EndEvent} to be mapped
	 */
	public EndEventMapping(TransformatorManager manager, org.eclipse.bpmn2.EndEvent bpmnEndEvent) {
		this.bpmnEndEvent = bpmnEndEvent;
		cltsEndEvent = manager.getChoreographyMapping().getCltsEndEvent();

		manager.getMappings().put(bpmnEndEvent.getId(), cltsEndEvent);
	}

	public org.eclipse.bpmn2.EndEvent getBpmnEndEvent() {
		return bpmnEndEvent;
	}

	public EndEvent getCltsEndEvent() {
		return cltsEndEvent;
	}

}
