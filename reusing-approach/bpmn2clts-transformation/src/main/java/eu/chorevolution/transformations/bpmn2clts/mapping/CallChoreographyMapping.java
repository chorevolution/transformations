/*
  * Copyright 2015 The CHOReVOLUTION project
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  *      http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */
package eu.chorevolution.transformations.bpmn2clts.mapping;

import org.eclipse.bpmn2.CallChoreography;
import org.eclipse.bpmn2.Choreography;

import eu.chorevolution.modelingnotations.clts.CallTransition;
import eu.chorevolution.modelingnotations.clts.CltsFactory;
import eu.chorevolution.modelingnotations.clts.SimpleState;
import eu.chorevolution.transformations.bpmn2clts.TransformatorException;
import eu.chorevolution.transformations.bpmn2clts.TransformatorManager;
import eu.chorevolution.transformations.bpmn2clts.util.BPMNUtil;
import eu.chorevolution.transformations.bpmn2clts.util.CLTSUtil;

/**
 * This class maps a BPMN {@link CallChoreography} into CLTS {@link CallTransition}
 */
public class CallChoreographyMapping {
	private CallChoreography bpmnCallChoreography;
	private CallTransition cltsCallTransition;

	/**
	 * Constructor of the {@link CallChoreographyMapping} class
	 * 
	 * @param manager
	 *            {@link TransformatorManager} that manage the transformation
	 * @param bpmnCallChoreography
	 *            {@link CallChoreography} to be mapped
	 * @throws TransformatorException
	 *             an exception is thrown if the mapping cannot be performed
	 */
	public CallChoreographyMapping(TransformatorManager manager, CallChoreography bpmnCallChoreography) throws TransformatorException {
		this.bpmnCallChoreography = bpmnCallChoreography;
		// create CLTS call transition
		cltsCallTransition = CltsFactory.eINSTANCE.createCallTransition();
		cltsCallTransition.setTaskName(bpmnCallChoreography.getName());

		// create CLTS source state
		SimpleState source = CltsFactory.eINSTANCE.createSimpleState();
		source.setName(CLTSUtil.getUniqueElementName());
		cltsCallTransition.setSource(source);

		// create CLTS target state
		SimpleState target = CltsFactory.eINSTANCE.createSimpleState();
		target.setName(CLTSUtil.getUniqueElementName());
		cltsCallTransition.setTarget(target);
		// create CLTS initiating participant
		org.eclipse.bpmn2.Participant bpmnInitiatingParticipant = BPMNUtil.getInitiatingParticipant(bpmnCallChoreography);
		cltsCallTransition.setInitiatingParticipant(manager.getCltsManager().getParticipant(bpmnInitiatingParticipant));

		// create CLTS receiving participant
		org.eclipse.bpmn2.Participant bpmnReceivingParticipant = BPMNUtil.getReceivingParticipant(bpmnCallChoreography);
		cltsCallTransition.setReceivingParticipant(manager.getCltsManager().getParticipant(bpmnReceivingParticipant));

		// TODO create CLTS input message

		// TODO create CLTS output message

		// get choreography which represents the call-choreography
		Choreography callChoreography =  manager.getBpmnManager().getChoreography(bpmnCallChoreography);
		
		// set reference call choreography
		cltsCallTransition.getChoreography().add(manager.getCltsManager().getChoreographies().get(callChoreography.getId()));

		manager.getMappings().put(bpmnCallChoreography.getId(), cltsCallTransition);
	}

	public CallChoreography getBpmnCallChoreography() {
		return bpmnCallChoreography;
	}

	public CallTransition getCltsCallTransition() {
		return cltsCallTransition;
	}

}
