/*
  * Copyright 2015 The CHOReVOLUTION project
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  *      http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */
package eu.chorevolution.transformations.bpmn2clts.mapping;

import eu.chorevolution.modelingnotations.clts.CLTSModel;
import eu.chorevolution.modelingnotations.clts.Choreography;
import eu.chorevolution.modelingnotations.clts.CltsFactory;
import eu.chorevolution.modelingnotations.clts.ControlflowTransition;
import eu.chorevolution.modelingnotations.clts.State;
import eu.chorevolution.modelingnotations.clts.TaskTransition;
import eu.chorevolution.modelingnotations.clts.Transition;
import eu.chorevolution.transformations.bpmn2clts.TransformatorManager;
import eu.chorevolution.transformations.bpmn2clts.util.LoadPropertiesFile;

/**
 * This class provides a manager for transform a {@link CLTSModel}
 * 
 */
public class ChoreographyMapping {
	private static final String CONFIG = "bpmn2clts.config.properties";
	private org.eclipse.bpmn2.Choreography bpmnChoreography;
	private Choreography cltsChoreography;
	private eu.chorevolution.modelingnotations.clts.StartEvent cltsStartEvent;
	private eu.chorevolution.modelingnotations.clts.EndEvent cltsEndEvent;
	private boolean isCltsChoreographyCreated;
	
	public ChoreographyMapping(org.eclipse.bpmn2.Choreography bpmnChoreography) {
		LoadPropertiesFile config = new LoadPropertiesFile(CONFIG);
		this.bpmnChoreography = bpmnChoreography;
		this.isCltsChoreographyCreated = false;
		// create CLTS Choreography
		cltsChoreography = CltsFactory.eINSTANCE.createChoreography();
		cltsChoreography.setChoreographyID(bpmnChoreography.getId());
		cltsChoreography.setChoreographyName(bpmnChoreography.getName());
		cltsStartEvent = CltsFactory.eINSTANCE.createStartEvent();
		cltsStartEvent.setName(config.get("startevent.name"));
		cltsChoreography.getStates().add(cltsStartEvent);
		cltsEndEvent = CltsFactory.eINSTANCE.createEndEvent();
		cltsEndEvent.setName(config.get("endevent.name"));
		cltsChoreography.getStates().add(cltsEndEvent);
	}

	public eu.chorevolution.modelingnotations.clts.StartEvent getCltsStartEvent() {
		return cltsStartEvent;
	}

	public eu.chorevolution.modelingnotations.clts.EndEvent getCltsEndEvent() {
		return cltsEndEvent;
	}

 

	public org.eclipse.bpmn2.Choreography getBpmnChoreography() {
		return bpmnChoreography;
	}

	/**
	 * This method create a {@link Choreography} element and returns  it
	 * 
	 * @param manager a {@link TransformatorManager} that manage the transformation
	 * @return a {@link Choreography} element
	 */
	public Choreography getCltsChoreography(TransformatorManager manager) {
		if (!isCltsChoreographyCreated) {
		
		for (Object element : manager.getMappings().values()) {
			/*
			 * Note that: element can be also instanceof State BPMNSubChoreography but this should not be considered
			 */
			if (element instanceof State) {
				/*
				 * add states in the CLTS model.
				 * 
				 * This code add states in the CLTS model. A state can be only start, end, fork, join , alternative. These states are directly mapped from BPMN element as: start, end, exclusive, parallel.
				 * 
				 * Note that: the states create to map BPMN Choreography Task they will be added later.
				 */
				cltsChoreography.getStates().add((State) element);
			} else if (element instanceof ControlflowTransition) {
				/*
				 * add controlflow transition in the CLTS model.
				 */
				cltsChoreography.getTransitions().add((Transition) element);
			} else if (element instanceof TaskTransition) {
				/*
				 * add a task transition in the CLTS model.
				 * 
				 * Note that: before to add the transition, the code MUST add state (source and target) and participant (initiating and receiving) create during the mapping.
				 */

				//add InitiatingParticipant
				//if (!cltsChoreography.getParticipants().contains(((TaskTransition) element).getInitiatingParticipant()))
					cltsChoreography.getParticipants().add(((TaskTransition) element).getInitiatingParticipant());
				// add receiving participant
				//if (!cltsChoreography.getParticipants().contains(((TaskTransition) element).getReceivingParticipant()))
					cltsChoreography.getParticipants().add(((TaskTransition) element).getReceivingParticipant());
				
				// add source state
				//if (!cltsChoreography.getStates().contains(((TaskTransition) element).getSource()))
					cltsChoreography.getStates().add(((TaskTransition) element).getSource());
				// add target state
				//if (!cltsChoreography.getStates().contains(((TaskTransition) element).getTarget()))
					cltsChoreography.getStates().add(((TaskTransition) element).getTarget());

				cltsChoreography.getTransitions().add((Transition) element);
			}

		}
		}
		return cltsChoreography;
	}

}
