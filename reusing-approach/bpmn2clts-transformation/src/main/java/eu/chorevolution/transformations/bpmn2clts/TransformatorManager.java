/*
  * Copyright 2015 The CHOReVOLUTION project
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  *      http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */
package eu.chorevolution.transformations.bpmn2clts;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.bpmn2.CallChoreography;
import org.eclipse.bpmn2.Choreography;
import org.eclipse.bpmn2.ChoreographyTask;
import org.eclipse.bpmn2.EndEvent;
import org.eclipse.bpmn2.ExclusiveGateway;
import org.eclipse.bpmn2.FlowElement;
import org.eclipse.bpmn2.ParallelGateway;
import org.eclipse.bpmn2.SequenceFlow;
import org.eclipse.bpmn2.StartEvent;
import org.eclipse.bpmn2.SubChoreography;

import eu.chorevolution.transformations.bpmn2clts.mapping.ChoreographyTaskMapping;
import eu.chorevolution.transformations.bpmn2clts.mapping.EndEventMapping;
import eu.chorevolution.transformations.bpmn2clts.mapping.ExclusiveGatewayMapping;
import eu.chorevolution.transformations.bpmn2clts.mapping.ParallelGatewayMapping;
import eu.chorevolution.transformations.bpmn2clts.mapping.SequenceFlowMapping;
import eu.chorevolution.transformations.bpmn2clts.mapping.StartEventMapping;
import eu.chorevolution.transformations.bpmn2clts.mapping.SubChoreographyMapping;
import eu.chorevolution.transformations.bpmn2clts.mapping.CallChoreographyMapping;
import eu.chorevolution.transformations.bpmn2clts.mapping.ChoreographyMapping;

public class TransformatorManager {

	private BPMNManager bpmnManager;
	private CLTSManager cltsManager;
	private Map<String, Object> mappings;
	private ChoreographyMapping choreographyMapping;
	
	public TransformatorManager(BPMNManager bpmnManager, CLTSManager cltsManager, Choreography bpmnChoreography) throws TransformatorException {

		// initialize Objects
		this.bpmnManager = bpmnManager;
		this.cltsManager = cltsManager;
		this.mappings = new HashMap<String, Object>();
		choreographyMapping = new ChoreographyMapping(bpmnChoreography);
		// perform BPMN to CLTS mapping from the input BPMN Choreography
		performBpmnToCltsMapping(bpmnChoreography);
		cltsManager.getChoreographies().put(bpmnChoreography.getId(),choreographyMapping.getCltsChoreography(this));
	}

	public Map<String, Object> getMappings() {
		return mappings;
	}

	public BPMNManager getBpmnManager() {
		return bpmnManager;
	}

	public CLTSManager getCltsManager() {
		return cltsManager;
	}

	public ChoreographyMapping getChoreographyMapping() {
		return choreographyMapping;
	}

	/**
	 * This method perform the mapping between BPMN elements and CLTS elements
	 * 
	 * @param bpmnChoreography
	 *            a BPMN {@link Choreography} that it can be transformed in a CLTS {@link eu.chorevolution.modelingnotations.clts.Choreography}
	 * @throws TransformatorException
	 */
	private void performBpmnToCltsMapping(Choreography bpmnChoreography) throws TransformatorException {
		// mapping: ChoreographyTask, StartEvent, EndEvent, ExclusiveGateway, ParallelGateway, SubChoreography
		for (FlowElement flowElement : bpmnChoreography.getFlowElements()) {
			if (flowElement instanceof ChoreographyTask) {
				new ChoreographyTaskMapping(this, (ChoreographyTask) flowElement);
			} else if (flowElement instanceof StartEvent) {
				new StartEventMapping(this, (org.eclipse.bpmn2.StartEvent) flowElement);
			} else if (flowElement instanceof EndEvent) {
				new EndEventMapping(this, (org.eclipse.bpmn2.EndEvent) flowElement);
			} else if (flowElement instanceof ExclusiveGateway) {
				new ExclusiveGatewayMapping(this, (ExclusiveGateway) flowElement);
			} else if (flowElement instanceof ParallelGateway) {
				new ParallelGatewayMapping(this, (ParallelGateway) flowElement);
			} else if (flowElement instanceof SubChoreography) {
				// get choreography which represents the sub-choreography
				performBpmnToCltsMapping(bpmnManager.getChoreography((SubChoreography) flowElement));
				new SubChoreographyMapping(this, (SubChoreography) flowElement);
			} else if (flowElement instanceof CallChoreography){
				// get choreography which represents the call-choreography
				Choreography callChoreography =  bpmnManager.getChoreography((CallChoreography) flowElement);
				// check if the callChoreography is already transformed
				if (!cltsManager.getChoreographies().containsKey(callChoreography.getId())){
					TransformatorManager t = new TransformatorManager(bpmnManager, cltsManager, callChoreography);
					cltsManager.getChoreographies().put(callChoreography.getId(), t.getChoreographyMapping().getCltsChoreography(this));				
				}
				new CallChoreographyMapping(this, (CallChoreography) flowElement);
			}

		}
		// mapping: SequenceFlow
		for (FlowElement flowElement : bpmnChoreography.getFlowElements()) {
			if (flowElement instanceof SequenceFlow) {
				new SequenceFlowMapping(this, (SequenceFlow) flowElement);
			}
		}
	}
}
