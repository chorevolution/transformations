/*
  * Copyright 2015 The CHOReVOLUTION project
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  *      http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */
package eu.chorevolution.transformations.bpmn2clts;

import java.util.ArrayList;
import java.util.List;

public class BPMNMessageType {
	private String name;
	private String content;
	private List<BPMNMessageType> types;

	public BPMNMessageType() {
		super();
		this.types = new ArrayList<BPMNMessageType>();
	}
	

	public BPMNMessageType(String name, String content) {
		super();
		this.name = name;
		this.content = content;
		this.types = new ArrayList<BPMNMessageType>();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}


	public List<BPMNMessageType> getTypes() {
		return types;
	}


	public void setTypes(List<BPMNMessageType> types) {
		this.types = types;
	}

	@Override
	public boolean equals(Object object){
	    if (object == null) return false;
	    if (object == this) return true;
	    if (!(object instanceof BPMNMessageType))return false;
	    
	    BPMNMessageType bpmnMessageType = (BPMNMessageType)object;
	    if (!this.name.equals(bpmnMessageType.getName())){
	    	return false;
	    }
	    
	    return true;
	}	
}
