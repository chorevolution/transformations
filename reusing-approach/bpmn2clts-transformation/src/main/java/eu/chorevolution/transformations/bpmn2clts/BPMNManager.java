/*
  * Copyright 2015 The CHOReVOLUTION project
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  *      http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */
package eu.chorevolution.transformations.bpmn2clts;

import java.io.File;
import java.util.List;

import org.eclipse.bpmn2.CallChoreography;
import org.eclipse.bpmn2.Choreography;
import org.eclipse.bpmn2.Definitions;
import org.eclipse.bpmn2.Import;
import org.eclipse.bpmn2.MessageFlow;
import org.eclipse.bpmn2.SubChoreography;

import eu.chorevolution.transformations.bpmn2clts.util.BPMNUtil;
import eu.chorevolution.transformations.bpmn2clts.util.LoadPropertiesFile;
import eu.chorevolution.transformations.bpmn2clts.util.xsd.XSDMessageUtil;

/**
 * This class provides a manager for a BPMN model
 */
public class BPMNManager {
	private static final String CONFIG = "bpmn2clts.config.properties";

	private File bpmnFile;

	/**
	 * this property contains all the choreography containd in the BPMN file
	 */
	private List<Choreography> choreographies;

	/**
	 * this property is a reference of default BPMN choreography
	 */
	private Choreography defaultChoreography;

	private List<BPMNMessageType> bpmnMessageTypes;

	public BPMNManager(File bpmnFile) throws TransformatorException {
		LoadPropertiesFile config = new LoadPropertiesFile(CONFIG);
		this.bpmnFile = bpmnFile;

		// load and extract choreographies contained in the BPMN model .
		choreographies = BPMNUtil.loadBPMNModel(bpmnFile);

		// set default choreography
		this.defaultChoreography = null;
		for (Choreography choreography : choreographies) {
			boolean isDefaultChoreography = (choreography.getId()
					.equalsIgnoreCase(config.get("default.choreography.id"))
					|| choreography.getName().equalsIgnoreCase(config.get("default.choreography.name"))) ? true : false;
			if (isDefaultChoreography) {
				defaultChoreography = choreography;
				break;
			}
		}
		if (defaultChoreography == null) {
			throw new TransformatorException(
					"no default choreography founded in the BPMN file: " + bpmnFile.getAbsolutePath());
		}

		List<Import> imports = ((Definitions) defaultChoreography.eContainer()).getImports();
		if (!imports.isEmpty()) {
			// TODO for now we assume that we have only one import
			bpmnMessageTypes = XSDMessageUtil.getImports(new File(bpmnFile.getParent(), imports.get(0).getLocation()));
		}

	}

	public File getBpmnFile() {
		return bpmnFile;
	}

	public List<Choreography> getChoreographies() {
		return choreographies;
	}

	public Choreography getDefaultChoreography() {
		return defaultChoreography;
	}

	/**
	 * This method it is used to get a {@link Choreography} instance for the
	 * {@link SubChoreography} element
	 * 
	 * @param subChoreography
	 *            the {@link SubChoreography} element
	 * @return a {@link Choreography} instance for the {@link SubChoreography}
	 *         element
	 * @throws TransformatorException
	 *             a {@link TransformatorException} is thrown if the BPMN model
	 *             not contains a instance for the {@link SubChoreography}
	 *             element
	 */
	public Choreography getChoreography(SubChoreography subChoreography) throws TransformatorException {
		for (Choreography choreography : choreographies) {
			if (choreography.getName().equalsIgnoreCase(subChoreography.getName()))
				return choreography;
		}

		throw new TransformatorException(
				"the BPMN model not contains a choreography named: " + subChoreography.getName());
	}

	/**
	 * This method it is used to get a {@link Choreography} instance for the
	 * {@link CallChoreography} element
	 * 
	 * @param callChoreography
	 *            the {@link CallChoreography} element
	 * @return a {@link Choreography} instance for the {@link CallChoreography}
	 *         element
	 * @throws TransformatorException
	 *             a {@link TransformatorException} is thrown if the BPMN model
	 *             not contains a instance for the {@link CallChoreography}
	 *             element
	 */
	public Choreography getChoreography(CallChoreography callChoreography) throws TransformatorException {
		for (Choreography choreography : choreographies) {
			if (choreography.getName().equalsIgnoreCase(callChoreography.getName()))
				return choreography;
		}
		throw new TransformatorException(
				"the BPMN model not contains a choreography named: " + callChoreography.getName());

	}

	/**
	 * This method it is used to get the content of the {@link MessageFlow}
	 * element
	 * 
	 * @param messageTypeName
	 *            name of the {@link MessageFlow} element
	 * @return the content of the {@link MessageFlow} element
	 * @throws TransformatorException
	 *             a {@link TransformatorException} is thrown if the BPMN model
	 *             not contains a {@link MessageFlow} element for the
	 *             messageTypeName
	 */
	public BPMNMessageType getBPMNMessageTypes(String messageTypeName) throws TransformatorException {
		for (BPMNMessageType bpmnMessageType : bpmnMessageTypes) {
			if (bpmnMessageType.getName().equals(messageTypeName)) {
				return bpmnMessageType;
			}
		}
		throw new TransformatorException("the BPMN model not contains a message type named: " + messageTypeName);
	}
	
	public List<BPMNMessageType> getBPMNMessageTypes(){
		return this.bpmnMessageTypes;
	}

}
