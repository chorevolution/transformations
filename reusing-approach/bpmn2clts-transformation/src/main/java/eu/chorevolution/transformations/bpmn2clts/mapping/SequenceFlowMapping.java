/*
  * Copyright 2015 The CHOReVOLUTION project
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  *      http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */
package eu.chorevolution.transformations.bpmn2clts.mapping;

import org.eclipse.bpmn2.ChoreographyTask;
import org.eclipse.bpmn2.SequenceFlow;
import org.eclipse.bpmn2.SubChoreography;

import eu.chorevolution.modelingnotations.clts.CltsFactory;
import eu.chorevolution.modelingnotations.clts.ControlflowTransition;
import eu.chorevolution.modelingnotations.clts.State;
import eu.chorevolution.modelingnotations.clts.TaskTransition;
import eu.chorevolution.modelingnotations.clts.Transition;
import eu.chorevolution.transformations.bpmn2clts.TransformatorException;
import eu.chorevolution.transformations.bpmn2clts.TransformatorManager;

/**
 * This class maps a BPMN {@link SequenceFlow} into CLTS {@link ControlflowTransition}
 *
 */
public class SequenceFlowMapping {
	private SequenceFlow bpmnSequenceFlow;
	private ControlflowTransition cltsControlflowTransition;

	/**
	 * Constructor of the {@link SequenceFlowMapping} class
	 * 
	 * @param manager
	 *            {@link TransformatorManager} that manage the transformation
	 * @param bpmnSequenceFlow
	 *            {@link SequenceFlow} to be mapped
	 * @throws TransformatorException
	 *             an exception is thrown if the mapping cannot be performed
	 */
	public SequenceFlowMapping(TransformatorManager manager, SequenceFlow bpmnSequenceFlow) throws TransformatorException {
		// throw an exception if sourceRef and targetRef they are not in the mapping. This means that they are not considered during the transformation
		if (!manager.getMappings().containsKey(bpmnSequenceFlow.getSourceRef().getId())) {
			throw new TransformatorException("the sourceRef(" + bpmnSequenceFlow.getSourceRef().getId() + ") for the BPMN SequenceFlow (" + bpmnSequenceFlow.getId() + ") she has not been trasformated");
		} else if (!manager.getMappings().containsKey(bpmnSequenceFlow.getTargetRef().getId())) {
			throw new TransformatorException("the targetRef(" + bpmnSequenceFlow.getTargetRef().getId() + ") for the BPMN SequenceFlow (" + bpmnSequenceFlow.getId() + ") she has not been trasformated");
		}

		this.bpmnSequenceFlow = bpmnSequenceFlow;
		cltsControlflowTransition = CltsFactory.eINSTANCE.createControlflowTransition();
		// TODO set the condition
		cltsControlflowTransition.setCondition("");

		/*
		 * NOTE THAT: a SourceRef and TargetRef elements can be: - ChoreographyActivity: ChoreographyTask, SubChoreography, CallChoreography - Event: StartEvent, EndEvent - Gateway: Parallel, Exclusive, Inclusive
		 */

		Object cltsSource = null;
		Object cltsTarget = null;

		/*
		 * check if sourceRef and targetRef BPMN elements are sub-choreography.
		 * 
		 * if true, in the mappings we have an instance of BPMNSubChoreography hence the code gets cltsLastObject and cltsFistObject respectively
		 */
		if (bpmnSequenceFlow.getSourceRef() instanceof SubChoreography) {
			cltsSource = ((SubChoreographyMapping) manager.getMappings().get(bpmnSequenceFlow.getSourceRef().getId())).getCltsLastObject();
		} else {
			cltsSource = manager.getMappings().get(bpmnSequenceFlow.getSourceRef().getId());
		}

		if (bpmnSequenceFlow.getTargetRef() instanceof SubChoreography) {
			cltsTarget = ((SubChoreographyMapping) manager.getMappings().get(bpmnSequenceFlow.getTargetRef().getId())).getCltsFistObject();
		} else {
			cltsTarget = manager.getMappings().get(bpmnSequenceFlow.getTargetRef().getId());
		}

		/*
		 * throw an exception if cltsSource and cltsTarget they are not in the mapping. This means that they are not considered in the transformation.
		 *
		 * this exception will be thrown if cltsSource or cltsTarget are null. cltsSource or cltsTarget are null if bpmnSequenceFlow.getSourceRef() instanceof SubChoreography or bpmnSequenceFlow.getTargetRef() instanceof SubChoreography rispectively the case in which cltsSource and cltsTarget are not SubChoreography element it is already considered at the beginning of the method
		 */
		if (cltsSource == null) {
			throw new TransformatorException("the BPMN SequenceFlow (" + bpmnSequenceFlow.getId() + ") It cannot be transformed because has not been created the CLTS source state");
		} else if (cltsTarget == null) {
			throw new TransformatorException("the BPMN SequenceFlow (" + bpmnSequenceFlow.getId() + ") It cannot be transformed because has not been created the CLTS target state");
		}

		// now cltsSource and cltsTarget can be only CLTS State or Transition
		if (cltsSource instanceof State && cltsTarget instanceof State) {
			mappingStateState(manager, (State) cltsSource, (State) cltsTarget);
		} else if (cltsSource instanceof State && cltsTarget instanceof Transition) {
			mappingStateTransition(manager, (State) cltsSource, (Transition) cltsTarget);
		} else if (cltsSource instanceof Transition && cltsTarget instanceof State) {
			mappingTransitionState(manager, (Transition) cltsSource, (State) cltsTarget);
		} else if (cltsSource instanceof Transition && cltsTarget instanceof Transition) {
			mappingTransitionTransition((Transition) cltsSource, (Transition) cltsTarget);
		}

	}

	/**
	 * This method replace the source {@link State} of the transitionTarget {@link TaskTransition} with the target {@link State} of the transitionSource {@link TaskTransition}.
	 * <p>
	 * This method is necessary when the transformation consider a {@link SequenceFlow} between two {@link ChoreographyTask}
	 * 
	 * @param source
	 *            source {@link TaskTransition}
	 * @param target
	 *            target {@link TaskTransition}
	 */
	private void mappingTransitionTransition(Transition source, Transition target) {
		target.setSource(source.getTarget());
	}

	private void mappingTransitionState(TransformatorManager manager, Transition source, State target) {
		cltsControlflowTransition.setSource(source.getTarget());
		cltsControlflowTransition.setTarget(target);
		manager.getMappings().put(bpmnSequenceFlow.getId(), cltsControlflowTransition);
	}

	private void mappingStateTransition(TransformatorManager manager, State source, Transition target) {
		cltsControlflowTransition.setSource(source);
		cltsControlflowTransition.setTarget(target.getSource());
		manager.getMappings().put(bpmnSequenceFlow.getId(), cltsControlflowTransition);
	}

	private void mappingStateState(TransformatorManager manager, State source, State target) {
		cltsControlflowTransition.setSource(source);
		cltsControlflowTransition.setTarget(target);
		manager.getMappings().put(bpmnSequenceFlow.getId(), cltsControlflowTransition);
	}

	public SequenceFlow getBpmnSequenceFlow() {
		return bpmnSequenceFlow;
	}

	public ControlflowTransition getCltsControlflowTransition() {
		return cltsControlflowTransition;
	}

}
