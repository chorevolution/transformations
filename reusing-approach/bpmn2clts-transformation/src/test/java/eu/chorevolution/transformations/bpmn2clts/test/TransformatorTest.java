/*
  * Copyright 2015 The CHOReVOLUTION project
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  *      http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */
package eu.chorevolution.transformations.bpmn2clts.test;

import java.io.File;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.chorevolution.modelingnotations.clts.CLTSModel;
import eu.chorevolution.transformations.bpmn2clts.Transformator;
import eu.chorevolution.transformations.bpmn2clts.TransformatorException;
import eu.chorevolution.transformations.bpmn2clts.test.util.TestUtils;


public class TransformatorTest {
	private static final String TEST_RESOURCES = "." + File.separatorChar + "src" + File.separatorChar + "test" + File.separatorChar + "resources" + File.separatorChar;

	private static Logger logger = LoggerFactory.getLogger(TransformatorTest.class);

	@Before
	public void setUp() {
	}

	@Test
	public void generateCLTS_01() {
		try {
			String bpmnIn = TEST_RESOURCES + "test_01" + File.separatorChar + "in_bpmn" + File.separatorChar + "test_01.bpmn2";
			Transformator t = new Transformator(new File(bpmnIn));
			CLTSModel cltsModel = t.transform();

			TestUtils.save(cltsModel, TEST_RESOURCES + "test_01" + File.separatorChar + "out_clts" + File.separatorChar + cltsModel.getName() + ".clts");

			Assert.assertTrue(true);

		} catch (TransformatorException e) {
			logger.error("generateCLTS_01 > " + e.getMessage());
			Assert.assertTrue(false);
		} catch (Exception e) {
			logger.error("generateCLTS_01 > " + e.getMessage());
			Assert.assertTrue(false);
		}
	}

	@Test
	public void generateCLTS_02() {
		try {
			String bpmnIn = TEST_RESOURCES + "test_02" + File.separatorChar + "in_bpmn" + File.separatorChar + "test_02.bpmn2";
			Transformator t = new Transformator(new File(bpmnIn));
			CLTSModel cltsModel = t.transform();

			TestUtils.save(cltsModel, TEST_RESOURCES + "test_02" + File.separatorChar + "out_clts" + File.separatorChar + cltsModel.getName() + ".clts");

			Assert.assertTrue(true);

		} catch (TransformatorException e) {
			logger.error("generateCLTS_02 > " + e.getMessage());
			Assert.assertTrue(false);
		} catch (Exception e) {
			logger.error("generateCLTS_02 > " + e.getMessage());
			Assert.assertTrue(false);
		}
	}

	@Test
	public void generateCLTS_03() {
		try {
			String bpmnIn = TEST_RESOURCES + "test_03" + File.separatorChar + "in_bpmn" + File.separatorChar + "test_03.bpmn2";
			Transformator t = new Transformator(new File(bpmnIn));
			CLTSModel cltsModel = t.transform();

			TestUtils.save(cltsModel, TEST_RESOURCES + "test_03" + File.separatorChar + "out_clts" + File.separatorChar + cltsModel.getName() + ".clts");

			Assert.assertTrue(true);

		} catch (TransformatorException e) {
			logger.error("generateCLTS_03 > " + e.getMessage());
			Assert.assertTrue(false);
		} catch (Exception e) {
			logger.error("generateCLTS_03 > " + e.getMessage());
			Assert.assertTrue(false);
		}
	}

	@Test
	public void generateCLTS_04() {
		try {
			String bpmnIn = TEST_RESOURCES + "test_04" + File.separatorChar + "in_bpmn" + File.separatorChar + "test_04.bpmn2";
			Transformator t = new Transformator(new File(bpmnIn));
			CLTSModel cltsModel = t.transform();

			TestUtils.save(cltsModel, TEST_RESOURCES + "test_04" + File.separatorChar + "out_clts" + File.separatorChar + cltsModel.getName() + ".clts");

			Assert.assertTrue(true);

		} catch (TransformatorException e) {
			logger.error("generateCLTS_04 > " + e.getMessage());
			Assert.assertTrue(false);
		} catch (Exception e) {
			logger.error("generateCLTS_04 > " + e.getMessage());
			Assert.assertTrue(false);
		}
	}

	@Test
	public void generateCLTS_05() {
		try {
			String bpmnIn = TEST_RESOURCES + "test_05" + File.separatorChar + "in_bpmn" + File.separatorChar + "test_05.bpmn2";
			Transformator t = new Transformator(new File(bpmnIn));
			CLTSModel cltsModel = t.transform();

			TestUtils.save(cltsModel, TEST_RESOURCES + "test_05" + File.separatorChar + "out_clts" + File.separatorChar + cltsModel.getName() + ".clts");

			Assert.assertTrue(true);

		} catch (TransformatorException e) {
			logger.error("generateCLTS_05 > " + e.getMessage());
			Assert.assertTrue(false);
		} catch (Exception e) {
			logger.error("generateCLTS_05 > " + e.getMessage());
			Assert.assertTrue(false);
		}
	}

	@Test
	public void generateCLTS_06() {
		try {
			String bpmnIn = TEST_RESOURCES + "test_06" + File.separatorChar + "in_bpmn" + File.separatorChar + "test_06.bpmn2";
			Transformator t = new Transformator(new File(bpmnIn));
			CLTSModel cltsModel = t.transform();

			TestUtils.save(cltsModel, TEST_RESOURCES + "test_06" + File.separatorChar + "out_clts" + File.separatorChar + cltsModel.getName() + ".clts");

			Assert.assertTrue(true);

		} catch (TransformatorException e) {
			logger.error("generateCLTS_06 > " + e.getMessage());
			Assert.assertTrue(false);
		} catch (Exception e) {
			logger.error("generateCLTS_06 > " + e.getMessage());
			Assert.assertTrue(false);
		}
	}

	@Test
	public void generateCLTS_07() {
		try {
			String bpmnIn = TEST_RESOURCES + "test_07" + File.separatorChar + "in_bpmn" + File.separatorChar + "test_07.bpmn2";
			Transformator t = new Transformator(new File(bpmnIn));
			CLTSModel cltsModel = t.transform();

			TestUtils.save(cltsModel, TEST_RESOURCES + "test_07" + File.separatorChar + "out_clts" + File.separatorChar + cltsModel.getName() + ".clts");

			Assert.assertTrue(true);

		} catch (TransformatorException e) {
			logger.error("generateCLTS_07 > " + e.getMessage());
			Assert.assertTrue(false);
		} catch (Exception e) {
			logger.error("generateCLTS_07 > " + e.getMessage());
			Assert.assertTrue(false);
		}
	}

	@Test
	public void generateCLTS_08() {
		try {
			String bpmnIn = TEST_RESOURCES + "test_08" + File.separatorChar + "in_bpmn" + File.separatorChar + "test_08.bpmn2";
			Transformator t = new Transformator(new File(bpmnIn));
			CLTSModel cltsModel = t.transform();

			TestUtils.save(cltsModel, TEST_RESOURCES + "test_08" + File.separatorChar + "out_clts" + File.separatorChar + cltsModel.getName() + ".clts");

			Assert.assertTrue(true);

		} catch (TransformatorException e) {
			logger.error("generateCLTS_08 > " + e.getMessage());
			Assert.assertTrue(false);
		} catch (Exception e) {
			logger.error("generateCLTS_08 > " + e.getMessage());
			Assert.assertTrue(false);
		}
	}

	@Test
	public void generateCLTS_09() {
		try {
			String bpmnIn = TEST_RESOURCES + "test_09" + File.separatorChar + "in_bpmn" + File.separatorChar + "test_09.bpmn2";
			Transformator t = new Transformator(new File(bpmnIn));
			CLTSModel cltsModel = t.transform();

			TestUtils.save(cltsModel, TEST_RESOURCES + "test_09" + File.separatorChar + "out_clts" + File.separatorChar + cltsModel.getName() + ".clts");

			Assert.assertTrue(true);

		} catch (TransformatorException e) {
			logger.error("generateCLTS_09 > " + e.getMessage());
			Assert.assertTrue(false);
		} catch (Exception e) {
			logger.error("generateCLTS_09 > " + e.getMessage());
			Assert.assertTrue(false);
		}
	}

	@Test
	public void generateCLTS_10() {
		try {
			String bpmnIn = TEST_RESOURCES + "test_10" + File.separatorChar + "in_bpmn" + File.separatorChar + "wp4.bpmn2";
			Transformator t = new Transformator(new File(bpmnIn));
			CLTSModel cltsModel = t.transform();

			TestUtils.save(cltsModel, TEST_RESOURCES + "test_10" + File.separatorChar + "out_clts" + File.separatorChar + cltsModel.getName() + ".clts");

			Assert.assertTrue(true);

		} catch (TransformatorException e) {
			logger.error("generateCLTS_10 > " + e.getMessage());
			Assert.assertTrue(false);
		} catch (Exception e) {
			logger.error("generateCLTS_10 > " + e.getMessage());
			Assert.assertTrue(false);
		}
	}
	
	@Test
	public void generateCLTS_11() {
		try {
			String bpmnIn = TEST_RESOURCES + "test_11" + File.separatorChar + "in_bpmn" + File.separatorChar + "choreography_1.bpmn";
			Transformator t = new Transformator(new File(bpmnIn));
			CLTSModel cltsModel = t.transform();

			TestUtils.save(cltsModel, TEST_RESOURCES + "test_11" + File.separatorChar + "out_clts" + File.separatorChar + cltsModel.getName() + ".clts");

			Assert.assertTrue(true);

		} catch (TransformatorException e) {
			logger.error("generateCLTS_11 > " + e.getMessage());
			Assert.assertTrue(false);
		} catch (Exception e) {
			logger.error("generateCLTS_11 > " + e.getMessage());
			Assert.assertTrue(false);
		}
	}
	
	@Test
	public void generateCLTS_12_wp5() {
		try {
			String bpmnIn = TEST_RESOURCES + "test_12_wp5" + File.separatorChar + "in_bpmn" + File.separatorChar + "wp5.bpmn";
			Transformator t = new Transformator(new File(bpmnIn));
			CLTSModel cltsModel = t.transform();

			TestUtils.save(cltsModel, TEST_RESOURCES + "test_12_wp5" + File.separatorChar + "out_clts" + File.separatorChar + cltsModel.getName() + ".clts");

			Assert.assertTrue(true);

		} catch (TransformatorException e) { 
			logger.error("generateCLTS_12_wp5 > " + e.getMessage());
			Assert.assertTrue(false);
		} catch (Exception e) {
			logger.error("generateCLTS_12_wp5 > " + e.getMessage());
			Assert.assertTrue(false);
		}
	}
	
	@Test
	public void generateCLTS_13() {
		try {
			String bpmnIn = TEST_RESOURCES + "test_13" + File.separatorChar + "in_bpmn" + File.separatorChar + "test_13.bpmn";
			Transformator t = new Transformator(new File(bpmnIn));
			CLTSModel cltsModel = t.transform();

			TestUtils.save(cltsModel, TEST_RESOURCES + "test_13" + File.separatorChar + "out_clts" + File.separatorChar + cltsModel.getName() + ".clts");

			Assert.assertTrue(true);

		} catch (TransformatorException e) {
			logger.error("generateCLTS_13 > " + e.getMessage());
			Assert.assertTrue(false);
		} catch (Exception e) {
			logger.error("generateCLTS_13 > " + e.getMessage());
			Assert.assertTrue(false);
		}
	}
}
