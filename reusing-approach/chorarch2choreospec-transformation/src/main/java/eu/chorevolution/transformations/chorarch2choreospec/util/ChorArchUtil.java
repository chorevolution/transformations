/*
  * Copyright 2015 The CHOReVOLUTION project
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  *      http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */
package eu.chorevolution.transformations.chorarch2choreospec.util;

import java.io.IOException;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;

import eu.chorevolution.modelingnotations.chorarch.ChorArchModel;
import eu.chorevolution.modelingnotations.chorarch.impl.ChorarchPackageImpl;
import eu.chorevolution.transformations.chorarch2choreospec.TransformatorException;

/**
 * This class provides static utility methods for manipulate a ChorArch model
 */
public class ChorArchUtil {

	/**
	 * Loads a ChorArch model from the specified {@link URI}
	 * <p>
	 * Returns the {@link ChorArchModel} loaded.
	 * <p>
	 * A {@link TransformatorException} is thrown if the ChorArch model can not be loaded.
	 * 
	 * @param chorArchURI
	 *            the {@link URI} of the .chorarch file.
	 * @return the {@link ChorArchModel} loaded.
	 * @throws TransformatorException
	 *             if the ChorArch file can not be loaded
	 */
	public static ChorArchModel loadChorArchModel(URI chorArchURI) throws TransformatorException{
		ChorarchPackageImpl.init();

		Resource resource = new XMIResourceFactoryImpl().createResource(chorArchURI);

		try {
			// load the resource
			resource.load(null);

		} catch (IOException e) {
			throw new TransformatorException("Error to load the resource: " + resource.getURI().toFileString());
		}

		ChorArchModel chorArchModel = (ChorArchModel) resource.getContents().get(0);

		return chorArchModel;
	}

}
