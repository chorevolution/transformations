/*
  * Copyright 2015 The CHOReVOLUTION project
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  *      http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */
package eu.chorevolution.transformations.chorarch2choreospec;

import java.io.StringWriter;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.PropertyException;

import eu.chorevolution.chors.datamodel.ChoreographySpec;

public class ChoreospecManager {
	// enactment datamodel
	private ChoreographySpec choreographySpecification;

	public ChoreospecManager() {
		choreographySpecification = new ChoreographySpec();
	}

	public ChoreographySpec getChoreographySpecification() {
		return choreographySpecification;
	}

	public void setChoreographySpecification(ChoreographySpec choreographySpecification) {
		this.choreographySpecification = choreographySpecification;
	}
	
	public String createXMLFile() throws JAXBException, PropertyException {
		JAXBContext jaxbContext = JAXBContext.newInstance(ChoreographySpec.class);
		Marshaller marshaller = jaxbContext.createMarshaller();
		marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
		StringWriter stringWriter = new StringWriter();
		marshaller.marshal(choreographySpecification, stringWriter);
		return stringWriter.toString();
	}

}
