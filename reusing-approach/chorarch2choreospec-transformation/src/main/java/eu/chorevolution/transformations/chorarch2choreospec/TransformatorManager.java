/*
  * Copyright 2015 The CHOReVOLUTION project
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  *      http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */
package eu.chorevolution.transformations.chorarch2choreospec;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import eu.chorevolution.modelingnotations.chorarch.AdditionalComponent;
import eu.chorevolution.modelingnotations.chorarch.BusinessComponent;
import eu.chorevolution.modelingnotations.chorarch.Component;
import eu.chorevolution.modelingnotations.chorarch.CoordinationDelegate;
import eu.chorevolution.modelingnotations.chorarch.RestServiceComponent;
import eu.chorevolution.modelingnotations.chorarch.ThingComponent;
import eu.chorevolution.modelingnotations.chorarch.WebServiceComponent;
import eu.chorevolution.services.datamodel.DeployableServiceSpec;
import eu.chorevolution.services.datamodel.LegacyServiceSpec;
import eu.chorevolution.services.datamodel.ServiceDependency;
import eu.chorevolution.services.datamodel.ServiceSpec;
import eu.chorevolution.transformations.chorarch2choreospec.util.LoadPropertiesFile;

public class TransformatorManager {
	private static final String CONFIG = "chorarch2choreospec.config.properties";
	
	LoadPropertiesFile config;

	private ChorArchManager chorArchManager;

	private ChoreospecManager choreospecManager;

	public TransformatorManager(ChorArchManager chorArchManager, ChoreospecManager choreospecManager)
			throws TransformatorException {
		 config = new LoadPropertiesFile(CONFIG);
		
		// initialize Objects
		this.chorArchManager = chorArchManager;
		this.choreospecManager = choreospecManager;
		initChoreographySpecificationModel(choreospecManager);
	}


	private void initChoreographySpecificationModel(ChoreospecManager choreospecManager) {
		// Used to store all Coordination Delegates Component to set foreach CDs
		// the dependency to all other cds
		List<DeployableServiceSpec> coordinationDeletates = new ArrayList<DeployableServiceSpec>();
		Map<Component, ServiceSpec> allServiceSpecification = new HashMap<Component, ServiceSpec>();

		for (Component component : this.chorArchManager.getChorArchModel().getComponents()) {

			if (component instanceof BusinessComponent) {
				// legacy service
				LegacyServiceSpec legacyServiceSpec = new LegacyServiceSpec();
				legacyServiceSpec.setName(component.getName());
				legacyServiceSpec.setRoles(component.getRoles());
				legacyServiceSpec.setNativeURIs(Arrays.asList((((BusinessComponent) component).getUri().split(";"))));
				
				if (component instanceof ThingComponent) {
					legacyServiceSpec.setServiceType(eu.chorevolution.services.datamodel.ServiceType.COAP);
				} else if (component instanceof WebServiceComponent) {
					legacyServiceSpec.setServiceType(eu.chorevolution.services.datamodel.ServiceType.SOAP);
				} else if (component instanceof RestServiceComponent) {
					legacyServiceSpec.setServiceType(eu.chorevolution.services.datamodel.ServiceType.REST);
				}
				allServiceSpecification.put(component, legacyServiceSpec);
				this.choreospecManager.getChoreographySpecification().getLegacyServiceSpecs().add(legacyServiceSpec);
			} else if (component instanceof AdditionalComponent) {
				// deployable service
				DeployableServiceSpec deployableServiceSpec = new DeployableServiceSpec();
				deployableServiceSpec.setName(component.getName());
			// 
				deployableServiceSpec.setRoles(component.getRoles());
				deployableServiceSpec.setPackageUri(((AdditionalComponent) component).getLocation());
				deployableServiceSpec.setEndpointName(component.getName());

				// TODO for now we set SOAP service Type checking if the SOAP
				// service type is correct for the binding component, for the
				// rest component is correct
				deployableServiceSpec.setServiceType(eu.chorevolution.services.datamodel.ServiceType.SOAP);


				deployableServiceSpec.setPort(Integer.parseInt(config.get("deployableService.port")));
				deployableServiceSpec.setNumberOfInstances(Integer.parseInt(config.get("deployableService.numberOfInstances")));
				deployableServiceSpec.setPackageType(eu.chorevolution.services.datamodel.PackageType.TOMCAT);

				if (component instanceof CoordinationDelegate) {
					coordinationDeletates.add(deployableServiceSpec);
				}

				allServiceSpecification.put(component, deployableServiceSpec);
				this.choreospecManager.getChoreographySpecification().getDeployableServiceSpecs().add(deployableServiceSpec);
			}
		}

		// set all dependencies
		for (Map.Entry<Component, ServiceSpec> entry : allServiceSpecification.entrySet()) {
			for (Component componentDependency : entry.getKey().getDependencies()) {
				ServiceDependency serviceDependency = new ServiceDependency();
				serviceDependency.setServiceSpecName(componentDependency.getName());
				// TODO check the set .get(0) seams is not correct
				if (!componentDependency.getRoles().isEmpty()){
					serviceDependency.setServiceSpecRole(componentDependency.getRoles().get(0));
				}
				entry.getValue().addDependency(serviceDependency);
			}

			if (coordinationDeletates.contains(entry.getValue())) {
				for (DeployableServiceSpec coordinationDeletate : coordinationDeletates) {
					if (!coordinationDeletate.equals(entry.getValue())) {
						ServiceDependency serviceDependency = new ServiceDependency();
						serviceDependency.setServiceSpecName(coordinationDeletate.getName());
						if (!coordinationDeletate.getRoles().isEmpty()){
							serviceDependency.setServiceSpecRole(coordinationDeletate.getName());
						}
						entry.getValue().addDependency(serviceDependency);
					}
				}
			}

		}

	}

	
}
