/*
  * Copyright 2015 The CHOReVOLUTION project
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  *      http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */
package eu.chorevolution.transformations.chorarch2choreospec;

import java.io.File;

import javax.xml.bind.JAXBException;
import javax.xml.bind.PropertyException;

public class Transformator {

	private ChorArchManager chorArchManager;

	private ChoreospecManager choreospecManager;

	public Transformator(File chorArchFile) throws TransformatorException {
		chorArchManager = new ChorArchManager(chorArchFile);
		choreospecManager = new ChoreospecManager();
	}

	public String trasform() throws TransformatorException, PropertyException, JAXBException {
		new TransformatorManager(chorArchManager, choreospecManager);
		return choreospecManager.createXMLFile();
	}


	public ChoreospecManager getEnactmentChoreospecManager() {
		return choreospecManager;
	}

	public void setEnactmentChoreospecManager(ChoreospecManager enactmentChoreospecManager) {
		this.choreospecManager = enactmentChoreospecManager;
	}

}
