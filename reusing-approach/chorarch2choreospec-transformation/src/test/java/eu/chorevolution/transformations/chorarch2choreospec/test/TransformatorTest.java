/*
  * Copyright 2015 The CHOReVOLUTION project
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  *      http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */
package eu.chorevolution.transformations.chorarch2choreospec.test;

import java.io.File;

import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.chorevolution.transformations.chorarch2choreospec.Transformator;
import eu.chorevolution.transformations.chorarch2choreospec.TransformatorException;

public class TransformatorTest {
	private static final String TEST_RESOURCES = "." + File.separatorChar + "src" + File.separatorChar + "test"
			+ File.separatorChar + "resources" + File.separatorChar;

	private static Logger logger = LoggerFactory.getLogger(TransformatorTest.class);

	@Before
	public void setUp() {
	}

	@Test
	public void generateCOORD_01() {
		try {
			String chorarchIn = TEST_RESOURCES + "test_01" + File.separatorChar + "in_chorarch" + File.separatorChar
					+ "test_01.chorarch";
			
			Transformator t = new Transformator(new File(chorarchIn));
			FileUtils.writeStringToFile(new File (TEST_RESOURCES + "test_01" + File.separatorChar + "out_choreospec" + File.separatorChar + "choreospec.choreospec"), t.trasform());

			Assert.assertTrue(true);

		} catch (TransformatorException e) {
			logger.error("generateCOREOSPEC_01 > " + e.getMessage());
			Assert.assertTrue(false);
		} catch (Exception e) {
			logger.error("generateCOREOSPEC_01 > " + e.getMessage());
			Assert.assertTrue(false);
		}
	}
	
	@Test
	public void generateCOORD_02() {
		try {
			String chorarchIn = TEST_RESOURCES + "test_02" + File.separatorChar + "in_chorarch" + File.separatorChar
					+ "test_02.chorarch";
			
			Transformator t = new Transformator(new File(chorarchIn));
			FileUtils.writeStringToFile(new File (TEST_RESOURCES + "test_02" + File.separatorChar + "out_choreospec" + File.separatorChar + "choreospec.choreospec"), t.trasform());

			Assert.assertTrue(true);

		} catch (TransformatorException e) {
			logger.error("generateCOREOSPEC_02 > " + e.getMessage());
			Assert.assertTrue(false);
		} catch (Exception e) {
			logger.error("generateCOREOSPEC_02 > " + e.getMessage());
			Assert.assertTrue(false);
		}
	}

	@Test
	public void generateCOORD_03() {
		try {
			String chorarchIn = TEST_RESOURCES + "test_03_wp5" + File.separatorChar + "in_chorarch" + File.separatorChar
					+ "wp5.chorarch";
			
			Transformator t = new Transformator(new File(chorarchIn));
			FileUtils.writeStringToFile(new File (TEST_RESOURCES + "test_03_wp5" + File.separatorChar + "out_choreospec" + File.separatorChar + "choreospec.choreospec"), t.trasform());

			Assert.assertTrue(true);

		} catch (TransformatorException e) {
			logger.error("generateCOREOSPEC_03 > " + e.getMessage());
			Assert.assertTrue(false);
		} catch (Exception e) {
			logger.error("generateCOREOSPEC_03 > " + e.getMessage());
			Assert.assertTrue(false);
		}
	}

}
