/*
  * Copyright 2015 The CHOReVOLUTION project
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  *      http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */
package eu.chorevolution.transformations.clts2coord.util;

import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.text.WordUtils;

import eu.chorevolution.modelingnotations.coord.AllowedOperation;
import eu.chorevolution.modelingnotations.coord.COORDModel;
import eu.chorevolution.modelingnotations.coord.CoordFactory;
import eu.chorevolution.modelingnotations.coord.MessageType;
import eu.chorevolution.modelingnotations.coord.Notify;
import eu.chorevolution.modelingnotations.coord.NotifyElement;
import eu.chorevolution.modelingnotations.coord.Participant;
import eu.chorevolution.modelingnotations.coord.State;
import eu.chorevolution.modelingnotations.coord.Wait;
import eu.chorevolution.modelingnotations.coord.WaitElement;
import eu.chorevolution.transformations.clts2coord.TransformatorException;

/**
 * This class provides static utility methods for manipulate a COORD model
 * 
 */
public class COORDUtil {
	private static final String CONFIG = "clts2coord.config.properties";
	
	private static String convertToCamelCase(String string, boolean startsUpperCase){
		if (startsUpperCase){
			return StringUtils.deleteWhitespace(WordUtils.capitalizeFully(string));
		}else{
			return StringUtils.uncapitalize((StringUtils.deleteWhitespace(WordUtils.capitalizeFully(string))));
		}
	}

	public static String coordNameFormat(String initiatingParticipantName, String receivingParticipantName) {
		LoadPropertiesFile config = new LoadPropertiesFile(CONFIG);
		String initiatingParticipantNameCamelized = convertToCamelCase(initiatingParticipantName,true);
		String receivingParticipantNameCamelized = convertToCamelCase(receivingParticipantName,true);
		return config.get("coord.name.firstpart") + config.get("coord.name.separator") + initiatingParticipantNameCamelized + config.get("coord.name.separator") + receivingParticipantNameCamelized;
	}
	
	public static State getState(COORDModel coordModel, String stateName) {
		for (State state : coordModel.getStates()) {
			if (state.getName().equalsIgnoreCase(stateName.trim())) {
				return state;
			}
		}

		// create the State
		State state = CoordFactory.eINSTANCE.createState();
		state.setName(stateName);
		coordModel.getStates().add(state);
		return state;
	}

	public static Participant getParticipant(COORDModel coordModel, String participantName) {
		for (Participant participant : coordModel.getParticipants()) {
			if (participant.getName().equalsIgnoreCase(participantName)) {
				return participant;
			}
		}

		// create the participant
		Participant participant = CoordFactory.eINSTANCE.createParticipant();
		participant.setName(participantName);
		coordModel.getParticipants().add(participant);
		return participant;
	}
	
	

	public static AllowedOperation getAllowedOperation(COORDModel coordModel, String operationName) {
		//convertToCamelCaseOperation
		String operationNameCamelized = convertToCamelCase(operationName,false);
		
		for (AllowedOperation operation : coordModel.getOperations()) {
			
			if (convertToCamelCase(operation.getName(),false).equalsIgnoreCase(operationNameCamelized)) {
				return operation;
			}
		}

		// create the AllowedOperation
		AllowedOperation operation = CoordFactory.eINSTANCE.createAllowedOperation();
		operation.setName(operationNameCamelized);
		coordModel.getOperations().add(operation);
		return operation;
	}

	public static Notify getNotify(COORDModel coordModel, String stateName, List<String> participantsName) {
		Notify notify = CoordFactory.eINSTANCE.createNotify();
		NotifyElement notifyElement = CoordFactory.eINSTANCE.createNotifyElement();
		State state = getState(coordModel, stateName);
		notifyElement.setState(state);

		for (String participantName : participantsName) {
			Participant participant = getParticipant(coordModel, participantName);
			if (!notifyElement.getParticipants().contains(participant)) {
				notifyElement.getParticipants().add(participant);
			}
		}
		notify.getElements().add(notifyElement);
		return notify;
	}

	public static Wait getWait(COORDModel coordModel, Map<String, String> stateNameAndParticipantName) {
		Wait wait = CoordFactory.eINSTANCE.createWait();

		for (String stateName : stateNameAndParticipantName.keySet()) {
			WaitElement waitElement = CoordFactory.eINSTANCE.createWaitElement();
			State state = getState(coordModel, stateName);
			Participant participant = getParticipant(coordModel, stateNameAndParticipantName.get(stateName));
			waitElement.setState(state);
			if (!waitElement.getParticipants().contains(participant)) {
				waitElement.getParticipants().add(participant);
			}
			wait.getElements().add(waitElement);

		}
		return wait;
	}

	// TODO adjust this implementations
	public static String getContidion(String condition) {
		if (condition == null || condition.trim().isEmpty())
			return "true";

		return condition.trim();
	}
	
	
	public static MessageType getCOORDMessageType (eu.chorevolution.modelingnotations.clts.MessageType messageType) throws TransformatorException{
		if (messageType == eu.chorevolution.modelingnotations.clts.MessageType.XSD){
			return MessageType.XSD;
		}
		
		throw new TransformatorException("Error to detect the COORD Message Type for the CLTS Message Type: " + messageType.name());
	}

}
