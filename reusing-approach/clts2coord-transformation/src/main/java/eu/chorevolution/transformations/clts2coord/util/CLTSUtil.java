/*
  * Copyright 2015 The CHOReVOLUTION project
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  *      http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */
package eu.chorevolution.transformations.clts2coord.util;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;

import eu.chorevolution.modelingnotations.clts.CLTSModel;
import eu.chorevolution.modelingnotations.clts.Choreography;
import eu.chorevolution.modelingnotations.clts.ControlflowTransition;
import eu.chorevolution.modelingnotations.clts.StartEvent;
import eu.chorevolution.modelingnotations.clts.State;
import eu.chorevolution.modelingnotations.clts.TaskTransition;
import eu.chorevolution.modelingnotations.clts.Transition;
import eu.chorevolution.modelingnotations.clts.impl.CltsPackageImpl;
import eu.chorevolution.transformations.clts2coord.TransformatorException;

/**
 * This class provides static utility methods for manipulate a CLTS model
 */
public class CLTSUtil {

	/**
	 * Loads a CLTS model from the specified {@link URI}, checking if it can be loaded and it contains {@link Choreography}.
	 * <p>
	 * Returns the {@link CLTSModel} loaded.
	 * <p>
	 * A {@link TransformatorException} is thrown if the CLTS model can not be loaded. A {@link TransformatorException} is thrown if the CLTS model is loaded but not contains any {@link Choreography}.
	 * 
	 * @param cltsURI
	 *            the {@link URI} of the .clts file.
	 * @return the {@link CLTSModel} loaded.
	 * @throws TransformatorException
	 *             if the CLTS file can not be loaded or the CLTS model not contains any Choreography.
	 */
	public static CLTSModel loadCLTSModel(URI cltsURI) throws TransformatorException {
		CltsPackageImpl.init();

		Resource resource = new XMIResourceFactoryImpl().createResource(cltsURI);

		try {
			// load the resource
			resource.load(null);

		} catch (IOException e) {
			throw new TransformatorException("Error to load the resource: " + resource.getURI().toFileString());
		}

		CLTSModel cltsModel = (CLTSModel) resource.getContents().get(0);

		if (cltsModel == null || cltsModel.getChoreographies() == null || cltsModel.getChoreographies().isEmpty()) {
			throw new TransformatorException("None choreography founded in the model: " + resource.getURI().toFileString());
		}

		return cltsModel;
	}

	/**
	 * This method returns the {@link StartEvent} contained in the choreography
	 * 
	 * @param choreography
	 * @return the {@link StartEvent} in the choreography
	 * @throws TransformatorException
	 *             if the choreography not contains any {@link StartEvent}
	 */
	public static State getStartEvent(Choreography choreography) throws TransformatorException {
		for (State state : choreography.getStates()) {
			if (state instanceof StartEvent)
				return state;
		}

		throw new TransformatorException("None Start Event founded in the choreography: " + choreography.getChoreographyName());
	}
	
	/**
	 * This method returns the source {@link State} of the first {@link TaskTransition} appearing navigating the {@link CLTSModel} from the {@link State}
	 * 
	 * @param startEvent {@link State} from which to begin the search
	 * @return the source {@link State} of the first {@link TaskTransition} appearing navigating the {@link CLTSModel} from the {@link State}
	 * @throws TransformatorException if none TaskTransition appearing navigating the {@link CLTSModel} from the {@State}
	 * 
	 */
	public static State getSourceStateOfFirstTaskTransition(State state) throws TransformatorException{
		for (Transition transition : state.getOutgoingTransition()) {
			if(transition instanceof TaskTransition)
				return transition.getSource();
			else
				return  getSourceStateOfFirstTaskTransition(transition.getTarget());
		}
		
		throw new TransformatorException("None TaskTransition appearing navigating the CLTS model from a State: " + state.getName());
		
	}
	
	/**
	 * This method returns the {@link List} of {@link TaskTransition} that appear before the {@link Transition} given as input
	 * 
	 * @param transition
	 *            {@link Transition}
	 * @return {@link List} of {@link TaskTransition} that appear before the {@link Transition} given as input
	 */
	public static List<TaskTransition> getPreviousTaskTransition(Transition transition) {
		List<TaskTransition> taskTransitions = new ArrayList<TaskTransition>();

		if (transition instanceof ControlflowTransition) {
			for (Transition incomingTransition : transition.getSource().getIncomingTransition()) {
				// call recursively getPreviousTaskTransition
				for (TaskTransition transitionToAdd : getPreviousTaskTransition(incomingTransition)) {
					// add all TaskTransition if not already added
					if (!taskTransitions.contains(transitionToAdd))
						taskTransitions.add(transitionToAdd);
				}
			}
		} else {
			taskTransitions.add((TaskTransition) transition);
		}

		return taskTransitions;
	}

}
