/*
  * Copyright 2015 The CHOReVOLUTION project
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  *      http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */
package eu.chorevolution.transformations.clts2coord;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import eu.chorevolution.modelingnotations.clts.Choreography;
import eu.chorevolution.modelingnotations.clts.Join;
import eu.chorevolution.modelingnotations.clts.TaskTransition;
import eu.chorevolution.modelingnotations.clts.Transition;
import eu.chorevolution.modelingnotations.coord.COORDModel;
import eu.chorevolution.modelingnotations.coord.CoordFactory;
import eu.chorevolution.modelingnotations.coord.Message;
import eu.chorevolution.modelingnotations.coord.Participant;
import eu.chorevolution.modelingnotations.coord.Tuple;
import eu.chorevolution.transformations.clts2coord.util.CLTSUtil;
import eu.chorevolution.transformations.clts2coord.util.COORDUtil;

public class TransformatorManager {
	private COORDManager coordManager;

	public TransformatorManager(Choreography choreography) throws TransformatorException {
		coordManager = new COORDManager(choreography);
		generateCOORDModels(choreography);
	}

	private void generateCOORDModels(Choreography choreography) throws TransformatorException {
		for (Transition transition : choreography.getTransitions()) {
			createTuple(transition);
		}
	}

	private void createTuple(Transition transition) throws TransformatorException {
		// get all coord where add one tuple for this transition
		List<COORDModel> coordModels = new ArrayList<COORDModel>();
		for (TaskTransition taskTransition : CLTSUtil.getPreviousTaskTransition(transition)) {
			String coordName = COORDUtil.coordNameFormat(taskTransition.getInitiatingParticipant().getName(),
					taskTransition.getReceivingParticipant().getName());
			COORDModel coordModel = coordManager.getCOORDModel(coordName);
			if (!coordModels.contains(coordModel)) {
				coordModels.add(coordModel);
			}
		}

		for (COORDModel coordModel : coordModels) {
			Tuple tuple = CoordFactory.eINSTANCE.createTuple();
			tuple.setSourceState(COORDUtil.getState(coordModel, transition.getSource().getName()));
			tuple.setTargetState(COORDUtil.getState(coordModel, transition.getTarget().getName()));
			tuple.setCondition(COORDUtil.getContidion(transition.getCondition()));

			// set AllowedComponentInTargetState
			for (Transition transitionInTargetState : transition.getTarget().getOutgoingTransition()) {
				if (transitionInTargetState instanceof TaskTransition) {
					String coordName = COORDUtil.coordNameFormat(
							((TaskTransition) transitionInTargetState).getInitiatingParticipant().getName(),
							((TaskTransition) transitionInTargetState).getReceivingParticipant().getName());
					Participant participant = COORDUtil.getParticipant(coordModel, coordName);
					if (!tuple.getAllowedComponentInTargetState().contains(participant)) {
						tuple.getAllowedComponentInTargetState().add(participant);
					}
				}
			}

			// set AllowedOperation
			if (transition instanceof TaskTransition) {
				tuple.setAllowedOperation(
						COORDUtil.getAllowedOperation(coordModel, ((TaskTransition) transition).getTaskName()));
				// set I/O message if present
				if (((TaskTransition) transition).getInMessage() != null) {
					Message message = CoordFactory.eINSTANCE.createMessage();
					message.setType(
							COORDUtil.getCOORDMessageType(((TaskTransition) transition).getInMessage().getType()));
					message.setContent(((TaskTransition) transition).getInMessage().getContent());
					tuple.getAllowedOperation().setInMessage(message);
				}

				if (((TaskTransition) transition).getOutMessage() != null) {
					Message message = CoordFactory.eINSTANCE.createMessage();
					message.setType(
							COORDUtil.getCOORDMessageType(((TaskTransition) transition).getOutMessage().getType()));
					message.setContent(((TaskTransition) transition).getOutMessage().getContent());
					tuple.getAllowedOperation().setOutMessage(message);
				}
			}

			// set Notify and wait
			if (transition.getTarget() instanceof Join) {

				List<String> coordsNameToNotify = new ArrayList<String>();
				Map<String, String> coordsNameToWait = new HashMap<String, String>();
				for (Transition t : transition.getTarget().getIncomingTransition()) {
					if (!t.equals(transition)) {
						for (TaskTransition taskTransition : CLTSUtil.getPreviousTaskTransition(t)) {
							String coordName = COORDUtil.coordNameFormat(
									taskTransition.getInitiatingParticipant().getName(),
									taskTransition.getReceivingParticipant().getName());

							if (!coordsNameToNotify.contains(coordName))
								coordsNameToNotify.add(coordName);
							// this implementation consider that for a source
							// state there is just only participant to wait
							if (!coordsNameToWait.containsKey(taskTransition.getTarget().getName())) {
								coordsNameToWait.put(taskTransition.getTarget().getName(), coordName);
							}
						}
					}
				}
				tuple.setNotify(COORDUtil.getNotify(coordModel, transition.getSource().getName(), coordsNameToNotify));
				tuple.setWait(COORDUtil.getWait(coordModel, coordsNameToWait));
			}
			coordModel.getTuples().add(tuple);
		}
	}

	public COORDManager getCOORDManager() {
		return this.coordManager;
	}

}
