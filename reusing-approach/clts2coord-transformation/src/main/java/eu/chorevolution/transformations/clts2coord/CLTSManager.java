/*
  * Copyright 2015 The CHOReVOLUTION project
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  *      http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */
package eu.chorevolution.transformations.clts2coord;

import java.io.File;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.URI;

import eu.chorevolution.modelingnotations.clts.CLTSModel;
import eu.chorevolution.modelingnotations.clts.Choreography;
import eu.chorevolution.transformations.clts2coord.util.CLTSUtil;

/**
 * This class provides a manager for a {@link CLTSModel}
 */
public class CLTSManager {

	private File cltsFile;
	private CLTSModel cltsModel;

	public CLTSManager(File cltsFile) throws TransformatorException {
		this.cltsFile = cltsFile;
		URI cltsURI = URI.createURI(cltsFile.toURI().toString());
		cltsModel = CLTSUtil.loadCLTSModel(cltsURI);
	}

	public CLTSModel getCltsModel() {
		return cltsModel;
	}

	public EList<Choreography> getChoreographies() {
		return cltsModel.getChoreographies();
	}

	public File getCltsFile() {
		return cltsFile;
	}

}
