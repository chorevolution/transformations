/*
  * Copyright 2015 The CHOReVOLUTION project
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  *      http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */
package eu.chorevolution.transformations.clts2coord;

import java.util.ArrayList;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;

import eu.chorevolution.modelingnotations.clts.Choreography;
import eu.chorevolution.modelingnotations.clts.Participant;
import eu.chorevolution.modelingnotations.coord.COORDModel;
import eu.chorevolution.modelingnotations.coord.CoordFactory;
import eu.chorevolution.modelingnotations.coord.State;
import eu.chorevolution.transformations.clts2coord.util.CLTSUtil;

/**
 * This class provides a manager for the {@link COORDModel}s must be generated for a specific {@link Choreography} 
 *
 */
public class COORDManager {
	
	
	private Choreography choreography;
	private List<COORDModel> coordModels;
	
	public COORDManager(Choreography choreography) {
		this.choreography = choreography;
		coordModels = new ArrayList<COORDModel>();
	}
	
	public COORDModel getCOORDModel (String coordName) throws TransformatorException{
		for(COORDModel coordModel : coordModels){
			if (coordModel.getName().equalsIgnoreCase(coordName.trim())){
				return coordModel;
			}
		}
		
		// create the coordmodel
		COORDModel coordModel = CoordFactory.eINSTANCE.createCOORDModel();
		coordModel.setChoreographyName(choreography.getChoreographyName());
		coordModel.setName(coordName.trim());
		State initialState = CoordFactory.eINSTANCE.createState();
		initialState.setName(CLTSUtil.getSourceStateOfFirstTaskTransition(CLTSUtil.getStartEvent(choreography)).getName());
		coordModel.getStates().add(initialState);
		coordModel.setInitialState(initialState);
		coordModels.add(coordModel);
		return coordModel;
	}

	public List<COORDModel> getCoordModels() {
		return coordModels;
	}
	
	public SortedSet<String> getCoordinationPriorities(){
		SortedSet<String> priorities = new TreeSet<String>();
		for(COORDModel coordModel :coordModels){
			priorities.add(coordModel.getName());
		}	
		return priorities;
	}

	public Choreography getChoreography() {
		return choreography;
	}
	

}
