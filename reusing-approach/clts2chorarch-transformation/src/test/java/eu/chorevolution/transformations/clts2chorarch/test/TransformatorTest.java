/*
  * Copyright 2015 The CHOReVOLUTION project
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  *      http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */
package eu.chorevolution.transformations.clts2chorarch.test;

import java.io.File;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.chorevolution.modelingnotations.chorarch.ChorArchModel;
import eu.chorevolution.transformations.clts2chorarch.Transformator;
import eu.chorevolution.transformations.clts2chorarch.TransformatorException;
import eu.chorevolution.transformations.clts2chorarch.test.util.TestUtils;

public class TransformatorTest {
	private static final String TEST_RESOURCES = "." + File.separatorChar + "src" + File.separatorChar + "test"
			+ File.separatorChar + "resources" + File.separatorChar;

	private static Logger logger = LoggerFactory.getLogger(TransformatorTest.class);

	@Before
	public void setUp() {
	}

	@Test
	public void generateCHORARCH_01() {
		try {
			String cltsIn = TEST_RESOURCES + "test_01" + File.separatorChar + "in_clts" + File.separatorChar
					+ "test_01.clts";
			Transformator t = new Transformator(new File(cltsIn));
			for (ChorArchModel model : t.transform()) {
				TestUtils.save(model, TEST_RESOURCES + "test_01" + File.separatorChar + "out_chorarch"
						+ File.separatorChar + model.getChoreographyName() + ".chorarch");
			}

			Assert.assertTrue(true);
		} catch (TransformatorException e) {
			logger.error("generateCHORARCH_01 > " + e.getMessage());
			Assert.assertTrue(false);
		} catch (Exception e) {
			logger.error("generateCHORARCH_01 > " + e.getMessage());
			Assert.assertTrue(false);
		}
	}

	@Test
	public void generateCHORARCH_02() {
		try {
			String cltsIn = TEST_RESOURCES + "test_02" + File.separatorChar + "in_clts" + File.separatorChar
					+ "test_02.clts";
			Transformator t = new Transformator(new File(cltsIn));
			for (ChorArchModel model : t.transform()) {
				TestUtils.save(model, TEST_RESOURCES + "test_02" + File.separatorChar + "out_chorarch"
						+ File.separatorChar + model.getChoreographyName() + ".chorarch");
			}

			Assert.assertTrue(true);

		} catch (TransformatorException e) {
			logger.error("generateCHORARCH_02 > " + e.getMessage());
			Assert.assertTrue(false);
		} catch (Exception e) {
			logger.error("generateCHORARCH_02 > " + e.getMessage());
			Assert.assertTrue(false);
		}
	}

	@Test
	public void generateCHORARCH_03() {
		try {
			String cltsIn = TEST_RESOURCES + "test_03" + File.separatorChar + "in_clts" + File.separatorChar
					+ "test_03.clts";
			Transformator t = new Transformator(new File(cltsIn));
			for (ChorArchModel model : t.transform()) {
				TestUtils.save(model, TEST_RESOURCES + "test_03" + File.separatorChar + "out_chorarch"
						+ File.separatorChar + model.getChoreographyName() + ".chorarch");

			}
			Assert.assertTrue(true);

		} catch (TransformatorException e) {
			logger.error("generateCHORARCH_03 > " + e.getMessage());
			Assert.assertTrue(false);
		} catch (Exception e) {
			logger.error("generateCHORARCH_03 > " + e.getMessage());
			Assert.assertTrue(false);
		}
	}

	@Test
	public void generateCHORARCH_04() {
		try {
			String cltsIn = TEST_RESOURCES + "test_04" + File.separatorChar + "in_clts" + File.separatorChar
					+ "test_04.clts";
			Transformator t = new Transformator(new File(cltsIn));
			for (ChorArchModel model : t.transform()) {
				TestUtils.save(model, TEST_RESOURCES + "test_04" + File.separatorChar + "out_chorarch"
						+ File.separatorChar + model.getChoreographyName() + ".chorarch");

			}
			Assert.assertTrue(true);

		} catch (TransformatorException e) {
			logger.error("generateCHORARCH_04 > " + e.getMessage());
			Assert.assertTrue(false);
		} catch (Exception e) {
			logger.error("generateCHORARCH_04 > " + e.getMessage());
			Assert.assertTrue(false);
		}
	}

	@Test
	public void generateCHORARCH_05() {
		try {
			String cltsIn = TEST_RESOURCES + "test_05" + File.separatorChar + "in_clts" + File.separatorChar
					+ "test_05.clts";
			Transformator t = new Transformator(new File(cltsIn));
			for (ChorArchModel model : t.transform()) {
				TestUtils.save(model, TEST_RESOURCES + "test_05" + File.separatorChar + "out_chorarch"
						+ File.separatorChar + model.getChoreographyName() + ".chorarch");

			}
			Assert.assertTrue(true);

		} catch (TransformatorException e) {
			logger.error("generateCHORARCH_05 > " + e.getMessage());
			Assert.assertTrue(false);
		} catch (Exception e) {
			logger.error("generateCHORARCH_05 > " + e.getMessage());
			Assert.assertTrue(false);
		}
	}

	@Test
	public void generateCHORARCH_06() {
		try {
			String cltsIn = TEST_RESOURCES + "test_06" + File.separatorChar + "in_clts" + File.separatorChar
					+ "test_06.clts";
			Transformator t = new Transformator(new File(cltsIn));
			for (ChorArchModel model : t.transform()) {
				TestUtils.save(model, TEST_RESOURCES + "test_06" + File.separatorChar + "out_chorarch"
						+ File.separatorChar + model.getChoreographyName() + ".chorarch");

			}
			Assert.assertTrue(true);

		} catch (TransformatorException e) {
			logger.error("generateCHORARCH_06 > " + e.getMessage());
			Assert.assertTrue(false);
		} catch (Exception e) {
			logger.error("generateCHORARCH_06 > " + e.getMessage());
			Assert.assertTrue(false);
		}
	}

	@Test
	public void generateCHORARCH_07() {
		try {
			String cltsIn = TEST_RESOURCES + "test_07" + File.separatorChar + "in_clts" + File.separatorChar
					+ "test_07.clts";
			Transformator t = new Transformator(new File(cltsIn));
			for (ChorArchModel model : t.transform()) {
				TestUtils.save(model, TEST_RESOURCES + "test_07" + File.separatorChar + "out_chorarch"
						+ File.separatorChar + model.getChoreographyName() + ".chorarch");

			}
			Assert.assertTrue(true);

		} catch (TransformatorException e) {
			logger.error("generateCHORARCH_07 > " + e.getMessage());
			Assert.assertTrue(false);
		} catch (Exception e) {
			logger.error("generateCHORARCH_07 > " + e.getMessage());
			Assert.assertTrue(false);
		}
	}

	@Test
	public void generateCHORARCH_08() {
		try {
			String cltsIn = TEST_RESOURCES + "test_08" + File.separatorChar + "in_clts" + File.separatorChar
					+ "test_08.clts";
			Transformator t = new Transformator(new File(cltsIn));
			for (ChorArchModel model : t.transform()) {
				TestUtils.save(model, TEST_RESOURCES + "test_08" + File.separatorChar + "out_chorarch"
						+ File.separatorChar + model.getChoreographyName() + ".chorarch");

			}
			Assert.assertTrue(true);

		} catch (TransformatorException e) {
			logger.error("generateCHORARCH_08 > " + e.getMessage());
			Assert.assertTrue(false);
		} catch (Exception e) {
			logger.error("generateCHORARCH_08 > " + e.getMessage());
			Assert.assertTrue(false);
		}
	}

	@Test
	public void generateCHORARCH_09() {
		try {
			String cltsIn = TEST_RESOURCES + "test_09" + File.separatorChar + "in_clts" + File.separatorChar
					+ "test_09.clts";
			Transformator t = new Transformator(new File(cltsIn));
			for (ChorArchModel model : t.transform()) {
				TestUtils.save(model, TEST_RESOURCES + "test_09" + File.separatorChar + "out_chorarch"
						+ File.separatorChar + model.getChoreographyName() + ".chorarch");

			}
			Assert.assertTrue(true);

		} catch (TransformatorException e) {
			logger.error("generateCHORARCH_09 > " + e.getMessage());
			Assert.assertTrue(false);
		} catch (Exception e) {
			logger.error("generateCHORARCH_09 > " + e.getMessage());
			Assert.assertTrue(false);
		}
	}

	@Test
	public void generateCHORARCH_10() {
		try {
			String cltsIn = TEST_RESOURCES + "test_10" + File.separatorChar + "in_clts" + File.separatorChar
					+ "wp4.clts";
			Transformator t = new Transformator(new File(cltsIn));
			for (ChorArchModel model : t.transform()) {
				TestUtils.save(model, TEST_RESOURCES + "test_10" + File.separatorChar + "out_chorarch"
						+ File.separatorChar + model.getChoreographyName() + ".chorarch");
			}

			Assert.assertTrue(true);

		} catch (TransformatorException e) {
			logger.error("generateCHORARCH_10 > " + e.getMessage());
			Assert.assertTrue(false);
		} catch (Exception e) {
			logger.error("generateCHORARCH_10 > " + e.getMessage());
			Assert.assertTrue(false);
		}
	}
	
	@Test
	public void generateCHORARCH_11() {
		try {
			String cltsIn = TEST_RESOURCES + "test_11" + File.separatorChar + "in_clts" + File.separatorChar
					+ "choreography_1.clts";
			Transformator t = new Transformator(new File(cltsIn));
			for (ChorArchModel model : t.transform()) {
				TestUtils.save(model, TEST_RESOURCES + "test_11" + File.separatorChar + "out_chorarch"
						+ File.separatorChar + model.getChoreographyName() + ".chorarch");
			}

			Assert.assertTrue(true);

		} catch (TransformatorException e) {
			logger.error("generateCHORARCH_11 > " + e.getMessage());
			Assert.assertTrue(false);
		} catch (Exception e) {
			logger.error("generateCHORARCH_11 > " + e.getMessage());
			Assert.assertTrue(false);
		}
	}

	@Test
	public void generateCHORARCH_12_wp5() {
		try {
			String cltsIn = TEST_RESOURCES + "test_12_wp5" + File.separatorChar + "in_clts" + File.separatorChar
					+ "wp5.clts";
			Transformator t = new Transformator(new File(cltsIn));
			for (ChorArchModel model : t.transform()) {
				TestUtils.save(model, TEST_RESOURCES + "test_12_wp5" + File.separatorChar + "out_chorarch"
						+ File.separatorChar + model.getChoreographyName() + ".chorarch");
			}

			Assert.assertTrue(true);

		} catch (TransformatorException e) {
			logger.error("generateCHORARCH_12_wp5 > " + e.getMessage());
			Assert.assertTrue(false);
		} catch (Exception e) {
			logger.error("generateCHORARCH_12_wp5 > " + e.getMessage());
			Assert.assertTrue(false);
		}
	}
}
