/*
  * Copyright 2015 The CHOReVOLUTION project
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  *      http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */
package eu.chorevolution.transformations.clts2chorarch;

import eu.chorevolution.modelingnotations.chorarch.Component;
import eu.chorevolution.modelingnotations.chorarch.CoordinationDelegate;

public class CoordinationDependency {
	private CoordinationDelegate coordinationDelegate;
	private Component sourceComponent;
	private Component targetComponent;

	public CoordinationDependency(CoordinationDelegate coordinationDelegate, Component sourceComponent,
			Component targetComponent) {
		super();
		this.coordinationDelegate = coordinationDelegate;
		this.sourceComponent = sourceComponent;
		this.targetComponent = targetComponent;
		
	}

	public CoordinationDelegate getCoordinationDelegate() {
		return coordinationDelegate;
	}

	public void setCoordinationDelegate(CoordinationDelegate coordinationDelegate) {
		this.coordinationDelegate = coordinationDelegate;
	}

	public Component getSourceComponent() {
		return sourceComponent;
	}

	public void setSourceComponent(Component sourceComponent) {
		this.sourceComponent = sourceComponent;
	}

	public Component getTargetComponent() {
		return targetComponent;
	}

	public void setTargetComponent(Component targetComponent) {
		this.targetComponent = targetComponent;
	}

	@Override
	public boolean equals(Object object){
	    if (object == null) return false;
	    if (object == this) return true;
	    if (!(object instanceof CoordinationDependency))return false;
	    CoordinationDependency coordinationDependency = (CoordinationDependency)object;
	    return this.coordinationDelegate.getName().equals(coordinationDependency.getCoordinationDelegate().getName());
	}
	
}
