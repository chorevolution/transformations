/*
  * Copyright 2015 The CHOReVOLUTION project
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  *      http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */
package eu.chorevolution.transformations.clts2chorarch.util;

import java.io.IOException;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;

import eu.chorevolution.modelingnotations.clts.CLTSModel;
import eu.chorevolution.modelingnotations.clts.Choreography;
import eu.chorevolution.modelingnotations.clts.impl.CltsPackageImpl;
import eu.chorevolution.transformations.clts2chorarch.TransformatorException;

/**
 * This class provides static utility methods for manipulate a CLTS model
 */
public class CLTSUtil {

	/**
	 * Loads a CLTS model from the specified {@link URI}, checking if it can be
	 * loaded and it contains {@link Choreography}.
	 * <p>
	 * Returns the {@link CLTSModel} loaded.
	 * <p>
	 * A {@link TransformatorException} is thrown if the CLTS model can not be
	 * loaded. A {@link TransformatorException} is thrown if the CLTS model is
	 * loaded but not contains any {@link Choreography}.
	 * 
	 * @param cltsURI
	 *            the {@link URI} of the .clts file.
	 * @return the {@link CLTSModel} loaded.
	 * @throws TransformatorException
	 *             if the CLTS file can not be loaded or the CLTS model not
	 *             contains any Choreography.
	 */
	public static CLTSModel loadCLTSModel(URI cltsURI) throws TransformatorException {
		CltsPackageImpl.init();

		Resource resource = new XMIResourceFactoryImpl().createResource(cltsURI);

		try {
			// load the resource
			resource.load(null);

		} catch (IOException e) {
			throw new TransformatorException("Error to load the resource: " + resource.getURI().toFileString());
		}

		CLTSModel cltsModel = (CLTSModel) resource.getContents().get(0);

		if (cltsModel == null || cltsModel.getChoreographies() == null || cltsModel.getChoreographies().isEmpty()) {
			throw new TransformatorException(
					"None choreography founded in the model: " + resource.getURI().toFileString());
		}

		return cltsModel;
	}

}
