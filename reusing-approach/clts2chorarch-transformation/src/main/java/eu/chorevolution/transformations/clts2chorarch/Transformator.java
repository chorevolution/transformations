/*
  * Copyright 2015 The CHOReVOLUTION project
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  *      http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */
package eu.chorevolution.transformations.clts2chorarch;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.text.WordUtils;
import org.eclipse.emf.common.util.URI;

import eu.chorevolution.modelingnotations.chorarch.ChorArchModel;
import eu.chorevolution.modelingnotations.chorarch.ChorarchFactory;
import eu.chorevolution.modelingnotations.chorarch.Component;
import eu.chorevolution.modelingnotations.chorarch.CoordinationDelegate;
import eu.chorevolution.modelingnotations.chorarch.WebServiceComponent;
import eu.chorevolution.modelingnotations.clts.CLTSModel;
import eu.chorevolution.modelingnotations.clts.Choreography;
import eu.chorevolution.modelingnotations.clts.Participant;
import eu.chorevolution.modelingnotations.clts.TaskTransition;
import eu.chorevolution.modelingnotations.clts.Transition;
import eu.chorevolution.transformations.clts2chorarch.util.CLTSUtil;
import eu.chorevolution.transformations.clts2chorarch.util.LoadPropertiesFile;

public class Transformator {
	private static final String CONFIG = "clts2chorarch.config.properties";
	private LoadPropertiesFile config;
	private CLTSModel cltsModel;
	
	// the CLTSModel contains the list of Choreography
	private List<ChorArchModel> chorArchModels;

	private Map<Participant, Component> participantComponents;
	private Map<String, CoordinationDependency> coordinationDependencies;
	


	public Transformator(File cltsFile) throws TransformatorException {
		config  = new LoadPropertiesFile(CONFIG);
		URI cltsURI = URI.createURI(cltsFile.toURI().toString());
		cltsModel = CLTSUtil.loadCLTSModel(cltsURI);
		chorArchModels = new ArrayList<ChorArchModel>();
		
		participantComponents = new HashMap<Participant, Component>();
		coordinationDependencies = new HashMap<String, CoordinationDependency>();
		
	}

	public List<ChorArchModel> transform() throws TransformatorException {
		for (Choreography choreography : cltsModel.getChoreographies()){
			initializeComponents(choreography);
			ChorArchModel chorArchModel = ChorarchFactory.eINSTANCE.createChorArchModel();
			chorArchModel.setChoreographyID(choreography.getChoreographyID());
			chorArchModel.setChoreographyName(choreography.getChoreographyName());
			chorArchModel.getComponents().addAll(participantComponents.values());
			for (CoordinationDependency coordinationDependency : coordinationDependencies.values()) {
				chorArchModel.getComponents().add(coordinationDependency.getCoordinationDelegate());
			}
			chorArchModels.add(chorArchModel);
		}
		return chorArchModels;
	}
	
	
	
	public List<ChorArchModel> getChorArchModels() throws TransformatorException {
		if (chorArchModels.isEmpty()){
			transform();
		}
		return chorArchModels;
	}

	private void initializeComponents(Choreography choreography){
		for (Transition transition : choreography.getTransitions()){
			if (transition instanceof TaskTransition){
				//create source Component if not exists
				Participant source = ((TaskTransition) transition).getInitiatingParticipant();
				if (!participantComponents.containsKey(source)){
					WebServiceComponent component = ChorarchFactory.eINSTANCE.createWebServiceComponent();
					component.setName(convertToCamelCase(source.getName(),true));
					component.getRoles().add(source.getName());
					component.setUri(config.get("coord.location")+source.getName()+"/"+source.getName());
					participantComponents.put(source, component);
				}
				
				//create target Component if not exists
				Participant target = ((TaskTransition) transition).getReceivingParticipant();
				if (!participantComponents.containsKey(target)){
					WebServiceComponent component = ChorarchFactory.eINSTANCE.createWebServiceComponent();
					component.setName(convertToCamelCase(target.getName(),true));
					component.getRoles().add(target.getName());
					component.setUri(config.get("coord.location")+target.getName()+"/"+target.getName());
					participantComponents.put(target, component);
				}
				
				
				//create coordinationComponent
				String coordName = coordNameFormat(source.getName(),target.getName());
				if (!coordinationDependencies.containsKey(coordName)){
					CoordinationDelegate coordinationDelegate = ChorarchFactory.eINSTANCE.createCoordinationDelegate();
					coordinationDelegate.setName(coordName);
					coordinationDelegate.getRoles().add(target.getName());
					coordinationDelegate.setLocation(config.get("coord.location")+coordName+config.get("coord.location.separator")+coordName+config.get("deployableService.artifact.extension"));
					
					//set dependencies
					participantComponents.get(source).getDependencies().add(coordinationDelegate);
					coordinationDelegate.getDependencies().add(participantComponents.get(target));
					
					CoordinationDependency coordinationDependency = new CoordinationDependency(coordinationDelegate, participantComponents.get(source), participantComponents.get(target));
					coordinationDependencies.put(coordName, coordinationDependency);
				}	
			}
		}
	}
	
	private static String convertToCamelCase(String string, boolean startsUpperCase){
		if (startsUpperCase){
			return StringUtils.deleteWhitespace(WordUtils.capitalizeFully(string));
		}else{
			return StringUtils.uncapitalize((StringUtils.deleteWhitespace(WordUtils.capitalizeFully(string))));
		}
	}
	
	public static String coordNameFormat(String initiatingParticipantName, String receivingParticipantName) {
		LoadPropertiesFile config = new LoadPropertiesFile(CONFIG);
		String initiatingParticipantNameCamelized = convertToCamelCase(initiatingParticipantName,true);
		String receivingParticipantNameCamelized = convertToCamelCase(receivingParticipantName,true);
		return config.get("coord.name.firstpart") + config.get("coord.name.separator") + initiatingParticipantNameCamelized + config.get("coord.name.separator") + receivingParticipantNameCamelized;
	}

}
