<?xml version="1.0" encoding="UTF-8"?>
<!--
Copyright 2015 The CHOReVOLUTION project

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
-->

<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
	xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
	<modelVersion>4.0.0</modelVersion>


	<groupId>eu.chorevolution.transformations</groupId>
	<artifactId>clts2chorarch-transformation</artifactId>
	<version>1.0.0-SNAPSHOT</version>
	<name>CHOReVOLUTION CLTS2COORD - Choreography LTS to Choreography Architectural Style transformation Bundle</name>
	<packaging>jar</packaging>

	<properties>
		<java.version>1.8</java.version>
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
	</properties>

	<organization>
		<name>The CHOReVOLUTION project</name>
		<url>http://www.chorevolution.eu</url>
	</organization>

	<licenses>
		<license>
			<name>The Apache Software License, Version 2.0</name>
			<url>http://www.apache.org/licenses/LICENSE-2.0.txt</url>
			<distribution>repo</distribution>
		</license>
	</licenses>

	<distributionManagement>
	<!--	
		<snapshotRepository>
			<id>ow2-nexus-snapshots</id>
			<name>OW2 Snapshots Repository</name>
			<url>http://repository.ow2.org/nexus/content/repositories/snapshots/</url>
		</snapshotRepository>
		<repository>
			<id>ow2_chorevolution-1003</id>
			<name>OW2 Snapshots Repository</name>
			<url>http://repository.ow2.org/nexus/content/repositories/ow2_chorevolution-1004/</url>
		</repository>
	-->
		<snapshotRepository>
			<id>chorevolution</id>
			<name>Chorevolution</name>
			<url>http://nexus.disim.univaq.it/content/repositories/chorevolution/</url>
		</snapshotRepository>
	</distributionManagement>

	<repositories>
	<!--	<repository>
			<id>ow2-snapshots</id>
			<url>http://repository.ow2.org/nexus/content/repositories/snapshots/</url>
			<releases>
				<enabled>false</enabled>
			</releases>
			<snapshots>
				<enabled>true</enabled>
			</snapshots>
		</repository>
		<repository>
			<id>ow2-releases</id>
			<url>http://repository.ow2.org/nexus/content/repositories/releases/</url>
			<releases>
				<enabled>false</enabled>
			</releases>
			<snapshots>
				<enabled>true</enabled>
			</snapshots>
		</repository>
-->	
		<repository>
			<id>chorevolution</id>
			<url>http://nexus.disim.univaq.it/content/repositories/chorevolution/</url>
			<releases>
				<enabled>false</enabled>
			</releases>
			<snapshots>
				<enabled>true</enabled>
			</snapshots>
		</repository>
	</repositories>

	<dependencies>
		<!-- org.eclipse.emf -->
		<dependency>
			<groupId>org.eclipse.emf</groupId>
			<artifactId>org.eclipse.emf.common</artifactId>
			<version>2.11.0-v20150805-0538</version>
		</dependency>
		<dependency>
			<groupId>org.eclipse.emf</groupId>
			<artifactId>org.eclipse.emf.ecore</artifactId>
			<version>2.11.1-v20150805-0538</version>
		</dependency>
		<dependency>
			<groupId>org.eclipse.emf</groupId>
			<artifactId>org.eclipse.emf.ecore.xmi</artifactId>
			<version>2.11.1-v20150805-0538</version>
		</dependency>
		<!-- metamodels to be used in the transformation -->
		<dependency>
			<groupId>eu.chorevolution.modelingnotations</groupId>
			<artifactId>eu.chorevolution.modelingnotations.clts</artifactId>
			<version>1.0.0-SNAPSHOT</version>
		</dependency>
		<dependency>
			<groupId>eu.chorevolution.modelingnotations</groupId>
			<artifactId>eu.chorevolution.modelingnotations.chorarch</artifactId>
			<version>1.0.0-SNAPSHOT</version>
		</dependency>
		<!-- logs -->
		<dependency>
			<groupId>org.slf4j</groupId>
			<artifactId>slf4j-api</artifactId>
			<version>1.7.12</version>
		</dependency>
		<dependency>
			<groupId>org.slf4j</groupId>
			<artifactId>slf4j-log4j12</artifactId>
			<version>1.7.12</version>
		</dependency>
		<!-- Extra -->
		<dependency>
			<groupId>org.apache.commons</groupId>
			<artifactId>commons-lang3</artifactId>
			<version>3.4</version>
		</dependency>
		<dependency>
			<groupId>commons-io</groupId>
			<artifactId>commons-io</artifactId>
			<version>2.4</version>
		</dependency>
		<!-- Test -->
		<dependency>
			<groupId>junit</groupId>
			<artifactId>junit</artifactId>
			<version>4.12</version>
			<scope>test</scope>
		</dependency>
	</dependencies>

	<build>
		<plugins>
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-compiler-plugin</artifactId>
				<version>3.3</version>
				<configuration>
					<source>${java.version}</source>
					<target>${java.version}</target>
					<useIncrementalCompilation>false</useIncrementalCompilation>
					<showWarnings>true</showWarnings>
					<showDeprecation>true</showDeprecation>
					<compilerArgument>-Xlint:unchecked</compilerArgument>
				</configuration>
			</plugin>
			<!-- Disable default license check and enforce specific -->
			<plugin>
				<groupId>com.mycila.maven-license-plugin</groupId>
				<artifactId>maven-license-plugin</artifactId>
				<inherited>true</inherited>
				<configuration>
					<skip>true</skip>
				</configuration>
			</plugin>
			<plugin>
				<groupId>org.apache.rat</groupId>
				<artifactId>apache-rat-plugin</artifactId>
				<version>0.11</version>
				<configuration>
					<excludes>
						<exclude>**/rat.txt</exclude>
						<exclude>**/build-copy-javadoc-files.xml</exclude>
						<exclude>logs/**</exclude>
						<exclude>**/*.log</exclude>
						<exclude>**/*.chorarch</exclude>
						<exclude>.git/**</exclude>
						<exclude>**/.*</exclude>
					</excludes>
				</configuration>
				<executions>
					<execution>
						<id>rat-check</id>
						<phase>verify</phase>
						<goals>
							<goal>check</goal>
						</goals>
					</execution>
				</executions>
			</plugin>

			<!-- 
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-checkstyle-plugin</artifactId>
				<version>2.17</version>
				<configuration>
					<configLocation>${basedir}/src/main/resources/checkstyle.xml</configLocation>
					<targetJdk>${targetJdk}</targetJdk>
				</configuration>
				<executions>
					<execution>
						<id>checkstyle-check</id>
						<phase>verify</phase>
						<goals>
							<goal>check</goal>
						</goals>
					</execution>
				</executions>
			</plugin>
			-->
			<!-- Put NOTICE and LICENSE files in all artifacts and javadocs -->
			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-resources-plugin</artifactId>
				<version>2.7</version>
				<executions>
					<execution>
						<id>copy-artifact-legal-files</id>
						<phase>process-resources</phase>
						<goals>
							<goal>copy-resources</goal>
						</goals>
						<configuration>
							<outputDirectory>${project.build.directory}/classes/META-INF</outputDirectory>
							<resources>
								<resource>
									<directory>${basedir}</directory>
									<includes>
										<include>LICENSE</include>
										<include>NOTICE</include>
									</includes>
								</resource>
							</resources>
						</configuration>
					</execution>
					<execution>
						<id>copy-javadoc-legal-files</id>
						<phase>process-resources</phase>
						<goals>
							<goal>copy-resources</goal>
						</goals>
						<configuration>
							<outputDirectory>${project.build.directory}/apidocs/META-INF</outputDirectory>
							<resources>
								<resource>
									<directory>${basedir}</directory>
									<includes>
										<include>LICENSE</include>
										<include>NOTICE</include>
									</includes>
								</resource>
							</resources>
						</configuration>
					</execution>
				</executions>
			</plugin>

			<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-assembly-plugin</artifactId>
				<inherited>true</inherited>
				<configuration>
					<appendAssemblyId>true</appendAssemblyId>
					<descriptors>
						<descriptor>src/assemble/connector.xml</descriptor>
					</descriptors>
					<archive>
						<index>true</index>
						<manifestEntries>
							<ConnectorBundle-FrameworkVersion>${project.version}</ConnectorBundle-FrameworkVersion>
							<ConnectorBundle-Name>${project.artifactId}</ConnectorBundle-Name>
							<ConnectorBundle-Version>${project.version}</ConnectorBundle-Version>
						</manifestEntries>
					</archive>
				</configuration>
				<executions>
					<execution>
						<id>make-assembly</id>
						<phase>package</phase>
						<goals>
							<goal>single</goal>
						</goals>
					</execution>
				</executions>
			</plugin>

		</plugins>
	</build>

</project>