 /*
 * Copyright 2015 The CHOReVOLUTION project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.chorevolution.transformation.cdimpljaxws;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import eu.chorevolution.transformation.cdimpljaxws.utility.PropertyConfiguration;
import eu.chorevolution.transformation.cdimpljaxws.utility.Utilities;
import eu.chorevolution.modelingnotations.coord.COORDModel;
import eu.chorevolution.synthesisprocessor.coordinationprotocol.algorithm.CoordinationDelegateData;
import eu.chorevolution.synthesisprocessor.coordinationprotocol.algorithm.CoordinationDelegateFactory;
import eu.chorevolution.synthesisprocessor.coordinationprotocol.algorithm.impl.CoordinationChannelManagerImpl;
import eu.chorevolution.synthesisprocessor.coordinationprotocol.algorithm.utility.CoordmetamodelParser;

public class CDServletContextListener implements ServletContextListener{

	private static final String COORD_PATH = CDServletContextListener.class.getClassLoader().getResource(PropertyConfiguration.getInstance().getStringValue("coord_name")).getPath();
	
	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void contextInitialized(ServletContextEvent arg0) {
		COORDModel coord = CoordmetamodelParser.parse(COORD_PATH);
    	CoordinationDelegateData coordinationDelegateData = new CoordinationDelegateData();
    	coordinationDelegateData.setInitialState(coord.getInitialState());
    	CoordinationChannelManagerImpl coordinationChannelManager = new CoordinationChannelManagerImpl();
    	coordinationDelegateData.setCoordinationChannelManager(coordinationChannelManager); 	
    	coordinationDelegateData.setCoordModel(coord); 
    	coordinationDelegateData.setCdPriorities(Utilities.createCoordinationDelegatePriorityListFromProperties(PropertyConfiguration.getInstance().getMapValue("cds_priorities")));
     	CoordinationDelegateFactory.addCoordinationDelegateData(PropertyConfiguration.getInstance().getStringValue("cd_name"), coordinationDelegateData);
	}

}
