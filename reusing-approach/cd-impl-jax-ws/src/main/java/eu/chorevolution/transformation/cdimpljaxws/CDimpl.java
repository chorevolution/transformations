 /*
 * Copyright 2015 The CHOReVOLUTION project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package eu.chorevolution.transformation.cdimpljaxws;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.namespace.QName;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.ws.Dispatch;
import javax.xml.ws.Provider;
import javax.xml.ws.Service;
import javax.xml.ws.ServiceMode;
import javax.xml.ws.WebServiceProvider;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.DOMException;

import eu.chorevolution.transformation.cdimpljaxws.monitor.MonitorLogger;
import eu.chorevolution.transformation.cdimpljaxws.monitor.MonitorLoggerImpl;
import eu.chorevolution.transformation.cdimpljaxws.utility.PropertyConfiguration;
import eu.chorevolution.transformation.cdimpljaxws.utility.Utilities;
import eu.chorevolution.modelingnotations.coord.AllowedOperation;
import eu.chorevolution.modelingnotations.coord.CoordFactory;
import eu.chorevolution.modelingnotations.coord.Participant;
import eu.chorevolution.synthesisprocessor.coordinationprotocol.algorithm.CoordinationDelegateAlgorithm;
import eu.chorevolution.synthesisprocessor.coordinationprotocol.algorithm.CoordinationDelegateFactory;
import eu.chorevolution.synthesisprocessor.coordinationprotocol.algorithm.ForwardMessagesCallback;
import eu.chorevolution.synthesisprocessor.coordinationprotocol.algorithm.model.ContextData;
import eu.chorevolution.synthesisprocessor.coordinationprotocol.algorithm.model.CoordinationDelegateID;
import eu.chorevolution.synthesisprocessor.coordinationprotocol.algorithm.model.ResponseData;

@ServiceMode(value=Service.Mode.MESSAGE)
@WebServiceProvider()
public class CDimpl implements Provider<SOAPMessage>{

    private static Logger logger = LoggerFactory.getLogger(CDimpl.class);
    
	@Override
	public SOAPMessage invoke(SOAPMessage message) {

		SOAPMessage response = null;
		ResponseData responseData = null;
		String requestSOAP;
    	try {
    		requestSOAP=message.getSOAPBody().getChildNodes().item(0).getLocalName();      		
    		if(requestSOAP.equals("setInvocationAddress")){
    			String role = Utilities.getRoleSetInvocationAddressMessage(message);
    			String name = Utilities.getNameSetInvocationAddressMessage(message);
    			List<String> endpoints = Utilities.getEndpointsSetInvocationAddressMessage(message);
    			if(role != null && name != null && endpoints != null){
        			setInvocationAddress(role,name,endpoints);
        		    response = Utilities.createSetInvocationAddressMessageResponse(message);   				
    			}
    		}
    		else{
    			int length = message.getMimeHeaders().getHeader("soapaction")[0].split("/").length;
        		requestSOAP = message.getMimeHeaders().getHeader("soapaction")[0].split("/")[length-1];
    			requestSOAP = requestSOAP.replace("\"","");
    			
    			switch (requestSOAP) {
    				case "poiRequest":
    					requestSOAP="getPoiList";
    					break;
    				case "getBestRouteRequest":
    					requestSOAP="getBestRoute";
    					break;	
    				case "trafficInformationRequest":
    					requestSOAP="traffic";
    					break;	
    				case "journeyPlannerRequest":
    					requestSOAP="allTrips";
    					break;
    				case "osmParkingRequest":
    					requestSOAP="parking";
    					break; 
    				case "GetInfoRequest":
    					requestSOAP="getInfo";
    					break;   
    				case "weatherInfoRequest":
    					requestSOAP="weatherInformationRequest";
    					break; 
    				case "sendTrafficInformationRequest":
    					requestSOAP="sendTrafficInformation";
    					break; 
    				case "sendTripsInformationRequest":
    					requestSOAP="sendTripsInformation";
    					break;
    				case "sendParkingInformationRequest":
    					requestSOAP="sendParkingInformation";
    					break;  
    				case "sendPublictransportationInformationRequest":
    					requestSOAP="sendPublicTransportationInformation";
    					break; 
    				case "sendWeatherInformationRequest":
    					requestSOAP="sendWeatherInformation";
    					break;      					
    				case "getTripPlanRequest":
    					requestSOAP="getTripPlan";
    					break;
				}
    			
    			Map<String, Object> data = new HashMap<String, Object>();
    			String sessionID = message.getSOAPBody().getElementsByTagName("sessionId").item(0).getTextContent();
     			data.put("sessionId", sessionID);
    			data.put("message", message);	
    			ContextData contextData = new ContextData(data);       			
    			AllowedOperation request = CoordFactory.eINSTANCE.createAllowedOperation();
    			request.setName(requestSOAP);
    			Participant coordinationDelegateFrom = CoordFactory.eINSTANCE.createParticipant();
    			coordinationDelegateFrom.setName(PropertyConfiguration.getInstance().getStringValue("cd_name"));
    			data.put("cdName", coordinationDelegateFrom.getName());
    			data.put("request",request.getName());
    	    	// MONITOR log the start of coordination of the request
    	    	MonitorLogger monitorLogger = new MonitorLoggerImpl();
    	    	monitorLogger.sendTimeDataStartRequest("WP5",sessionID,coordinationDelegateFrom.getName(),request.getName(),System.currentTimeMillis());
    			CoordinationDelegateID coordinationDelegateID = new CoordinationDelegateID(PropertyConfiguration.getInstance().getStringValue("cd_name"), sessionID);
    			CoordinationDelegateAlgorithm algorithm = CoordinationDelegateFactory.getCoordinationDelegateAlgorithm(coordinationDelegateID,PropertyConfiguration.getInstance().getStringValue("cd_name"));
       			System.out.println("______________________ REQUEST SOAP: "+requestSOAP+" CD: "+coordinationDelegateFrom.getName()+" REQUEST: "+request.getName()+" ______________________");
    			responseData = algorithm.handleRules(request,coordinationDelegateFrom, new CDcallback(), contextData); 
    			response = (SOAPMessage) responseData.getData();   
     	    	// MONITOR log the end of coordination of the request
    	    	monitorLogger.sendTimeDataEndRequest("WP5",sessionID,coordinationDelegateFrom.getName(),request.getName(),System.currentTimeMillis());
    	    	// MONITOR log number of messages used for the coordination of the request
    	    	monitorLogger.sendNumberCoordMessageRequest("WP5",sessionID,coordinationDelegateFrom.getName(),request.getName(),Utilities.getNumberCoordinationMessagesOperation(CoordinationDelegateFactory.getCOORDModel(coordinationDelegateFrom.getName()),request.getName()));
    		}
		} catch (DOMException | SOAPException e) {
			logger.info(e.getMessage());
		}  
    	return response;
	}

	public void setInvocationAddress(String role, String name, List<String> endpoints) {

        logger.info("PERFORM -- setInvocationAddress - parameters: role: " + (role.isEmpty() ? "isEmpty parameter" : role) + " - name: " + (name.isEmpty() ? "isEmpty parameter" : name) + " - endpoints[0]: " + (endpoints.get(0).isEmpty() ? "isEmpty parameter" : endpoints.get(0)));
        if(CoordinationDelegateFactory.containsAddress(role)){
        	CoordinationDelegateFactory.removeAddress(role);
        }
        CoordinationDelegateFactory.addAddress(role, endpoints.get(0));
	}

	public class CDcallback implements ForwardMessagesCallback{

		@Override
		public ResponseData forwardMessage(ContextData contextData) {

			ResponseData response = null;
			Map<String, Object> data = (Map<String, Object>)contextData.getData();
			SOAPMessage message = (SOAPMessage) data.get("message");
			QName portQName = new QName(PropertyConfiguration.getInstance().getStringValue("port_namespaceURI"),PropertyConfiguration.getInstance().getStringValue("port_localPart"));
			try {
				Service service = Service.create((new URL(CoordinationDelegateFactory.getAddress(PropertyConfiguration.getInstance().getStringValue("cd_role"))+"?wsdl")), new QName(PropertyConfiguration.getInstance().getStringValue("service_namespaceURI"),PropertyConfiguration.getInstance().getStringValue("service_localPart")));
				// MONITOR log the start of forwarding of the request
    	    	MonitorLogger monitorLogger = new MonitorLoggerImpl();
    	    	monitorLogger.sendTimeDataForwardRequest("WP5",(String)data.get("sessionId"),(String)data.get("cdName"),(String)data.get("request"),System.currentTimeMillis());
				Dispatch<SOAPMessage> smDispatch = service.createDispatch(portQName, SOAPMessage.class, Service.Mode.MESSAGE);
				SOAPMessage responseMessage = smDispatch.invoke(message);
				response = new ResponseData(responseMessage);	
		    	// MONITOR log the end of forwarding of the request
    	    	monitorLogger.sendTimeDataForwardResponse("WP5",(String)data.get("sessionId"),(String)data.get("cdName"),(String)data.get("request"),System.currentTimeMillis());
			} catch (MalformedURLException e) {
				logger.info(e.getMessage());
			} 
			return response;
		}
	}

}
