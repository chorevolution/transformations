package eu.chorevolution.transformation.cdimpljaxws.monitor;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;

import eu.chorevolution.transformation.cdimpljaxws.utility.PropertyConfiguration;






public class MonitorLoggerImpl implements MonitorLogger{

	
	
	@Override
	public void sendTimeDataStartRequest(String choreographyName, String instanceID,String cdName, String operationName,
			long timestamp) {
		
		if(PropertyConfiguration.getInstance().getStringValue("monitor").equals("true")){
			StringEntity input;
			try {
				HttpClient client = HttpClientBuilder.create().build();
				HttpPost postRequest = new HttpPost("http://"+PropertyConfiguration.getInstance().getStringValue("monitor_host")+":"+PropertyConfiguration.getInstance().getStringValue("monitor_port")+"/syncope/rest/collector/timedatastartrequest");
				input = new StringEntity("{\"choreographyName\":\""+choreographyName+"\","
										+ "\"instanceName\":\""+instanceID+"\","
										+ "\"cdName\":\""+cdName+"\","
										+ "\"operationName\":\""+operationName+"\","
										+ "\"timestamp\":"+timestamp+"}");
				input.setContentType("application/json");
				postRequest.setEntity(input);
				client.execute(postRequest);
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ClientProtocolException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}	
		}
		System.out.println("------- Choreography: "+choreographyName+" Instance: "+instanceID+"--- CD: "+cdName+" Starts the coordination of the operation: "+operationName+" at: "+timestamp+"-------");	
	}

	@Override
	public void sendTimeDataEndRequest(String choreographyName, String instanceID,String cdName, String operationName,
			long timestamp) {
		if(PropertyConfiguration.getInstance().getStringValue("monitor").equals("true")){
			StringEntity input;
			try {
				HttpClient client = HttpClientBuilder.create().build();
				HttpPost postRequest = new HttpPost("http://"+PropertyConfiguration.getInstance().getStringValue("monitor_host")+":"+PropertyConfiguration.getInstance().getStringValue("monitor_port")+"/syncope/rest/collector/timedatastartrequest");
				input = new StringEntity("{\"choreographyName\":\""+choreographyName+"\","
										+ "\"instanceName\":\""+instanceID+"\","
										+ "\"cdName\":\""+cdName+"\","
										+ "\"operationName\":\""+operationName+"\","
										+ "\"timestamp\":"+timestamp+"}");
				input.setContentType("application/json");
				postRequest.setEntity(input);
				client.execute(postRequest);
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ClientProtocolException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}	
		}
		System.out.println("------- Choreography: "+choreographyName+" Instance: "+instanceID+"--- CD: "+cdName+" Ends the coordination of the operation: "+operationName+" at: "+timestamp+"-------");	
	}

	@Override
	public void sendTimeDataForwardRequest(String choreographyName, String instanceID,String cdName,String operationName,
			long timestamp) {
		if(PropertyConfiguration.getInstance().getStringValue("monitor").equals("true")){
			StringEntity input;
			try {
				HttpClient client = HttpClientBuilder.create().build();
				HttpPost postRequest = new HttpPost("http://"+PropertyConfiguration.getInstance().getStringValue("monitor_host")+":"+PropertyConfiguration.getInstance().getStringValue("monitor_port")+"/syncope/rest/collector/timedatastartrequest");
				input = new StringEntity("{\"choreographyName\":\""+choreographyName+"\","
										+ "\"instanceName\":\""+instanceID+"\","
										+ "\"cdName\":\""+cdName+"\","
										+ "\"operationName\":\""+operationName+"\","
										+ "\"timestamp\":"+timestamp+"}");
				input.setContentType("application/json");
				postRequest.setEntity(input);
				client.execute(postRequest);
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ClientProtocolException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}	
		}
		System.out.println("------- Choreography: "+choreographyName+" Instance: "+instanceID+"--- CD: "+cdName+" Starts the forwarding of the operation: "+operationName+" at: "+timestamp+"-------");	
	}

	@Override
	public void sendTimeDataForwardResponse(String choreographyName, String instanceID,String cdName, String operationName,
			long timestamp) {
		if(PropertyConfiguration.getInstance().getStringValue("monitor").equals("true")){
			StringEntity input;
			try {
				HttpClient client = HttpClientBuilder.create().build();
				HttpPost postRequest = new HttpPost("http://"+PropertyConfiguration.getInstance().getStringValue("monitor_host")+":"+PropertyConfiguration.getInstance().getStringValue("monitor_port")+"/syncope/rest/collector/timedatastartrequest");
				input = new StringEntity("{\"choreographyName\":\""+choreographyName+"\","
										+ "\"instanceName\":\""+instanceID+"\","
										+ "\"cdName\":\""+cdName+"\","
										+ "\"operationName\":\""+operationName+"\","
										+ "\"timestamp\":"+timestamp+"}");
				input.setContentType("application/json");
				postRequest.setEntity(input);
				client.execute(postRequest);
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ClientProtocolException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}	
		}
		System.out.println("------- Choreography: "+choreographyName+" Instance: "+instanceID+"--- CD: "+cdName+" Ends the forwarding of the operation: "+operationName+" at: "+timestamp+"-------");	
	}

	@Override
	public void sendNumberCoordMessageRequest(String choreographyName, String instanceID,String cdName, String operationName,
			int messagesNumber) {
		if(PropertyConfiguration.getInstance().getStringValue("monitor").equals("true")){
			StringEntity input;
			try {
				HttpClient client = HttpClientBuilder.create().build();
				HttpPost postRequest = new HttpPost("http://"+PropertyConfiguration.getInstance().getStringValue("monitor_host")+":"+PropertyConfiguration.getInstance().getStringValue("monitor_port")+"/syncope/rest/collector/timedatastartrequest");
				input = new StringEntity("{\"choreographyName\":\""+choreographyName+"\","
										+ "\"instanceName\":\""+instanceID+"\","
										+ "\"cdName\":\""+cdName+"\","
										+ "\"operationName\":\""+operationName+"\","
										+ "\"messageNumber\":"+messagesNumber+"}");
				input.setContentType("application/json");
				postRequest.setEntity(input);
				client.execute(postRequest);
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ClientProtocolException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("------- Choreography: "+choreographyName+" Instance: "+instanceID+"--- CD: "+cdName+" used "+messagesNumber+" coordination messages for the coordination of the operation: "+operationName+"-------");	
	}

}
