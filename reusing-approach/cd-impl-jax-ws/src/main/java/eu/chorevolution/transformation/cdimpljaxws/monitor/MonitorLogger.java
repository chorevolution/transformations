package eu.chorevolution.transformation.cdimpljaxws.monitor;

public interface MonitorLogger {

	public void sendTimeDataStartRequest(String choreographyName,String instanceID,String cdName,String operationName,long timestamp);
	
	public void sendTimeDataEndRequest(String choreographyName,String instanceID,String cdName,String operationName,long timestamp);
	
	public void sendTimeDataForwardRequest(String choreographyName,String instanceID,String cdName,String operationName,long timestamp);
	
	public void sendTimeDataForwardResponse(String choreographyName,String instanceID,String cdName,String operationName,long timestamp);
	
	public void sendNumberCoordMessageRequest(String choreographyName,String instanceID,String cdName,String operationName,int messagesNumber);
	
}
