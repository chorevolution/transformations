/*
* Copyright 2015 The CHOReVOLUTION project
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package eu.chorevolution.transformations.cdgenerator.impl.utility;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.jar.JarOutputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import eu.chorevolution.modelingnotations.coord.AllowedOperation;
import eu.chorevolution.modelingnotations.coord.COORDModel;
import eu.chorevolution.transformations.cdgenerator.CDGeneratorException;
import eu.chorevolution.transformations.cdgenerator.model.Operation;

public class Utilities {
	
	private static Logger logger = LoggerFactory.getLogger(Utilities.class);
	private final static String xmlLicenseHeader = new StringBuilder()
												.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>").append(System.getProperty("line.separator"))
												.append("<!--").append(System.getProperty("line.separator"))
												.append("Copyright 2015 The CHOReVOLUTION project").append(System.getProperty("line.separator"))
												.append("Licensed under the Apache License, Version 2.0 (the \"License\");").append(System.getProperty("line.separator"))
												.append("you may not use this file except in compliance with the License.").append(System.getProperty("line.separator"))
												.append("You may obtain a copy of the License at").append(System.getProperty("line.separator"))
												.append(System.getProperty("line.separator"))
												.append("\t http://www.apache.org/licenses/LICENSE-2.0").append(System.getProperty("line.separator"))
												.append(System.getProperty("line.separator"))
												.append("Unless required by applicable law or agreed to in writing, software").append(System.getProperty("line.separator"))
												.append("distributed under the License is distributed on an \"AS IS\" BASIS").append(System.getProperty("line.separator"))
												.append("WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.").append(System.getProperty("line.separator"))
												.append("See the License for the specific language governing permissions and ").append(System.getProperty("line.separator"))
												.append("limitations under the License.").append(System.getProperty("line.separator"))
												.append("-->").append(System.getProperty("line.separator"))
												.toString();

	private final static String propertiesLicenseHeader = new StringBuilder()
												.append("#").append(System.getProperty("line.separator"))
												.append("# Copyright 2015 The CHOReVOLUTION project").append(System.getProperty("line.separator"))
												.append(System.getProperty("line.separator"))
												.append("# Licensed under the Apache License, Version 2.0 (the \"License\");").append(System.getProperty("line.separator"))
												.append("# you may not use this file except in compliance with the License.").append(System.getProperty("line.separator"))
												.append("# You may obtain a copy of the License at").append(System.getProperty("line.separator"))
												.append(System.getProperty("line.separator"))
												.append("\t http://www.apache.org/licenses/LICENSE-2.0").append(System.getProperty("line.separator"))
												.append("#").append(System.getProperty("line.separator"))
												.append("# Unless required by applicable law or agreed to in writing, software").append(System.getProperty("line.separator"))
												.append("# distributed under the License is distributed on an \"AS IS\" BASIS,").append(System.getProperty("line.separator"))
												.append("# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.").append(System.getProperty("line.separator"))
												.append("# See the License for the specific language governing permissions and").append(System.getProperty("line.separator"))
												.append("# limitations under the License.").append(System.getProperty("line.separator"))
												.append("#").append(System.getProperty("line.separator"))												
												.toString();

	
	
	public static String getDestinationFolderPath(String destDir){
		return (destDir+System.currentTimeMillis()).replaceAll("\\s", "_");
	}
	
	
	
	public static void createWebXml(String projectDir,String cdName){

		File webxml = new File(projectDir+System.getProperty("file.separator")+"WEB-INF"+System.getProperty("file.separator")+"web.xml");
		String content = new StringBuilder(xmlLicenseHeader)
						.append("<web-app xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"").append(System.getProperty("line.separator"))
						.append("xmlns:web=\"http://java.sun.com/xml/ns/javaee/web-app_3_0.xsd\"").append(System.getProperty("line.separator"))
						.append("xsi:schemaLocation=\"http://java.sun.com/xml/ns/javaee\"").append(System.getProperty("line.separator"))
						.append("version=\"3.0\"> ").append(System.getProperty("line.separator"))
						.append("\t <display-name>"+cdName+"</display-name>").append(System.getProperty("line.separator"))
						.append("\t <listener>").append(System.getProperty("line.separator"))
						.append("\t \t <listener-class>com.sun.xml.ws.transport.http.servlet.WSServletContextListener</listener-class>").append(System.getProperty("line.separator"))
						.append("\t </listener>").append(System.getProperty("line.separator"))
						.append("\t <listener>").append(System.getProperty("line.separator"))
						.append("\t \t <listener-class>eu.chorevolution.transformation.cdimpljaxws.CDServletContextListener</listener-class>").append(System.getProperty("line.separator"))
						.append("\t </listener>").append(System.getProperty("line.separator"))
						.append("\t <servlet>").append(System.getProperty("line.separator"))
						.append("\t \t <servlet-name>"+cdName+"</servlet-name>").append(System.getProperty("line.separator"))
						.append("\t \t <servlet-class>com.sun.xml.ws.transport.http.servlet.WSServlet</servlet-class>").append(System.getProperty("line.separator"))
						.append("\t \t <load-on-startup>1</load-on-startup>").append(System.getProperty("line.separator"))
						.append("\t </servlet>").append(System.getProperty("line.separator"))
						.append("\t <servlet-mapping>").append(System.getProperty("line.separator"))
						.append("\t \t <servlet-name>"+cdName+"</servlet-name>").append(System.getProperty("line.separator"))
						.append("\t \t <url-pattern>/"+cdName+"/*</url-pattern>").append(System.getProperty("line.separator"))
						.append("\t </servlet-mapping>").append(System.getProperty("line.separator"))
						.append("</web-app>")
						.toString();			
		try {
			FileUtils.writeStringToFile(webxml,content,Charset.defaultCharset());
		} catch (IOException e) {
			logger.info(e.getMessage());		
		}
	}
	
	public static void createSunJaxWS(String projectDir,String name,String wsdlPath,String packageName, String targetNS, String wsdlName){
		
		 try {
			Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new FileInputStream(wsdlPath));	
			// TODO  take wsdlName from path
			String portTypeName = doc.getElementsByTagName("portType").item(0).getAttributes().getNamedItem("name").getTextContent();
			String portName = doc.getElementsByTagName("port").item(0).getAttributes().getNamedItem("name").getTextContent();
			String serviceName = doc.getElementsByTagName("service").item(0).getAttributes().getNamedItem("name").getTextContent();
			String interfaceName = packageName+"."+portTypeName;
			String implementation = "eu.chorevolution.transformation.cdimpljaxws.CDimpl";
			String wsdl = "WEB-INF"+System.getProperty("file.separator")+"wsdl"+System.getProperty("file.separator")+wsdlName;
			String service = "{"+targetNS+"}"+serviceName;
			String port = "{"+targetNS+"}"+portName;
			String urlpattern = "/"+name;
			File sunjaxws = new File(projectDir+System.getProperty("file.separator")+"WEB-INF"+System.getProperty("file.separator")+"sun-jaxws.xml"); 
			String content = new StringBuilder(xmlLicenseHeader)
							.append("<endpoints version=\"2.0\" xmlns=\"http://java.sun.com/xml/ns/jax-ws/ri/runtime\">").append(System.getProperty("line.separator"))
							.append("<endpoint").append(System.getProperty("line.separator"))
							.append("\t name=\""+name+"\"").append(System.getProperty("line.separator"))
							.append("\t interface=\""+interfaceName+"\" ").append(System.getProperty("line.separator"))
							.append("\t implementation=\""+implementation+"\"").append(System.getProperty("line.separator"))
							.append("\t wsdl=\""+wsdl+"\"").append(System.getProperty("line.separator"))
							.append("\t service=\""+service+"\"").append(System.getProperty("line.separator"))
							.append("\t port=\""+port+"\"").append(System.getProperty("line.separator"))						
							.append("\t url-pattern=\""+urlpattern+"\"/>").append(System.getProperty("line.separator"))
							.append("<endpoint").append(System.getProperty("line.separator"))
							.append("\t implementation=\"eu.chorevolution.synthesisprocessor.coordinationprotocol.channel.WSCoordinationChannel\"").append(System.getProperty("line.separator"))
							.append("\t name=\"WSCoordinationChannel\"").append(System.getProperty("line.separator"))
							.append("\t service=\"{http://eu.chorevolution.synthesisprocessor.coordinationprotocol.channel.client/}WSCoordinationChannelService\"").append(System.getProperty("line.separator"))
							.append("\t port=\"{http://eu.chorevolution.synthesisprocessor.coordinationprotocol.channel.client/}WSCoordinationChannelPort\"").append(System.getProperty("line.separator"))
							.append("\t url-pattern=\"/WSCoordinationChannel\"/>").append(System.getProperty("line.separator"))
							.append("</endpoints>")
							.toString();		
			FileUtils.writeStringToFile(sunjaxws,content,Charset.defaultCharset());
		} catch (IOException | SAXException | ParserConfigurationException e) {
			logger.info(e.getMessage());
		}
	}
	
	public static void copyLibs2(String projectDir){
		
		final String path = "resources/war-structure/WEB-INF/lib";
		final String pathIDE = "war-structure/WEB-INF/lib";
		File libsDestFolder = new File(projectDir+System.getProperty("file.separator")+"WEB-INF"+System.getProperty("file.separator")+"lib");
		final File jarFile = new File(Utilities.class.getProtectionDomain().getCodeSource().getLocation().getPath());

			try {
				if(jarFile.isFile()) {  // Run with JAR file
					JarFile jar;
					jar = new JarFile(jarFile);
					final Enumeration<JarEntry> entries = jar.entries(); //gives ALL entries in jar
					while(entries.hasMoreElements()) {
						final String name = entries.nextElement().getName();
						//System.out.println(name);
						if (name.startsWith(path + "/")) { //filter according to the path
							System.out.println(name);

						}
					}
					jar.close();
				} else { // Run with IDE
					final URL url = Utilities.class.getResource("/" + pathIDE);
					if (url != null) {
						try {
							final File apps = new File(url.toURI());
							for (File app : apps.listFiles()) {
								System.out.println(app);
							}
						} catch (URISyntaxException ex) {
			            // never happens
						}
					}
				}		
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

	}
	
	public static void copyLibsFolder(String projectDir) {
		
		File libs = new File(Utilities.class.getClassLoader().getResource("WEB-INF/lib").getPath());
		try {
			FileUtils.copyDirectory(libs, new File(projectDir+File.separatorChar+"WEB-INF"+File.separatorChar+"lib"));
		} catch (IOException e) {
			logger.info(e.getMessage());	
		}
	}
	
	public static void createWar(String cd_name, String projectDir){
		
		final File jarFile = new File(Utilities.class.getProtectionDomain().getCodeSource().getLocation().getPath());
		FileOutputStream fout;
		try {
			fout = new FileOutputStream(new File(projectDir).getParent()+System.getProperty("file.separator")+cd_name+".war");
			JarOutputStream jarOut = new JarOutputStream(fout);
			
			if(jarFile.isFile()) { 
				// Run with JAR file
				final String path = "WEB-INF/lib/";
				final JarFile jar = new JarFile(Utilities.class.getProtectionDomain().getCodeSource().getLocation().getPath());
				final Enumeration<JarEntry> entries = jar.entries(); //gives ALL entries in jar
				while(entries.hasMoreElements()) {
					JarEntry jarEntry = (JarEntry) entries.nextElement();
					final String name = jarEntry.getName();
					if (name.startsWith(path) && !name.equals(path)) { //filter according to the path
						jarOut.putNextEntry(jarEntry);
						InputStream jarIS = jar.getInputStream(jarEntry);
						byte[] buffer = new byte[1024];
						while(true){
					          int nRead = jarIS.read(buffer, 0, buffer.length);
					          if (nRead <= 0)
					            break;
					          jarOut.write(buffer, 0, nRead);
						}
						jarIS.close();
					}
				}		
				jar.close();
			}
			else{
				copyLibsFolder(projectDir);
			}
			List<File> fileList = new ArrayList<File>();
			getAllFiles(new File(projectDir), fileList);			
			for (File file : fileList) {
				jarOut.putNextEntry(new JarEntry(file.getPath().replace(projectDir+File.separatorChar, "")));
				FileInputStream in = new FileInputStream(file);
				byte[] buffer = new byte[1024];
				while(true){  
			          int nRead = in.read(buffer, 0, buffer.length);
			          if (nRead <= 0)
			            break;
			          jarOut.write(buffer, 0, nRead);
				}
				in.close();
			}
			jarOut.close();
			fout.close();
		} catch (IOException e) {
			logger.info(e.getMessage());
		}

	}

	
	public static void getAllFiles(File dir, List<File> fileList) {
		
		File[] files = dir.listFiles();
		for (File file : files) {
			if (file.isDirectory()) {
				getAllFiles(file, fileList);
			}
			else{
				fileList.add(file);
			}
		}
	}
	
	public static void copyCoordModel(File coord,String projectDir,String cdName){
		
		try {
			FileUtils.copyFile(coord, new File(projectDir+System.getProperty("file.separator")+"WEB-INF"+System.getProperty("file.separator")+"classes"+System.getProperty("file.separator")+cdName+".coord"));
		} catch (IOException e) {
			logger.info(e.getMessage());
		}
	}
	
	public static File createCoordModel(String projectDir,String cdName, byte[] coordModel){
					
		File coordFile = new File(projectDir+System.getProperty("file.separator")+"WEB-INF"+System.getProperty("file.separator")+"classes"+System.getProperty("file.separator")+cdName+".coord");
		try {
			FileUtils.writeByteArrayToFile(coordFile, coordModel);
		} catch (IOException e) {
			throw new CDGeneratorException(e.getMessage());
		}
		return coordFile;
	}
	
	public static String getOperationFromCoordModel(COORDModel coord){
		
		return coord.getOperations().get(0).getName();
	}
	
	public static List<Operation> getOperationsFromCoordModel(COORDModel coord){
		
		List<Operation> operations = new ArrayList<>();
		List<AllowedOperation> coordops = coord.getOperations();
		for (AllowedOperation allowedOperation : coordops) {
			Operation operation;
			if(allowedOperation.getOutMessage()!=null)
				operation = new Operation(allowedOperation.getName(), allowedOperation.getInMessage().getContent(), allowedOperation.getOutMessage().getContent());
			else
				operation = new Operation(allowedOperation.getName(), allowedOperation.getInMessage().getContent(), null);
			operations.add(operation);
		}
		return operations;
	}
	
	public static String getInMessageDefinitionFromCoordModel(COORDModel coord){
		
		return coord.getOperations().get(0).getInMessage().getContent();
	}
	
	public static String getOutMessageDefinitionFromCoordModel(COORDModel coord){
		
		if(coord.getOperations().get(0).getOutMessage()!=null)
			return coord.getOperations().get(0).getOutMessage().getContent();
		else
			return null;
	}
	
	public static void deleteProjectFolder(String projectDir){
		
		try {
			FileUtils.deleteDirectory(new File(projectDir));
		} catch (IOException e) {
			logger.info(e.getMessage());
		}
	}
	
	public static byte[] getBytesFromWar(String projectDir,String cd_name){
		
		byte[] bytes = null; 
		try {
			bytes = FileUtils.readFileToByteArray(new File(projectDir+System.getProperty("file.separator")+cd_name+".war"));
		} catch (IOException e) {
			logger.info(e.getMessage());
		}
		return bytes;
	}
	
	public static void deleteWar(String projectDir,String cd_name){

		try {
			FileUtils.forceDelete(new File(projectDir+cd_name+".war"));
		} catch (IOException e) {
			logger.info(e.getMessage());
		}
	}
	
	public static String getPathFromPackageName(String packageName){
		
		String path = "";
		String[] pathItems = packageName.split("[.]");
		for (int i = 0; i < pathItems.length; i++) {
			path+="/"+pathItems[i];
		}
		return path;
	}
		
	public static void createLog4jProperties(String projectDir, String cdName){
		
		File log4j = new File(projectDir+System.getProperty("file.separator")+"WEB-INF"+System.getProperty("file.separator")+"classes"+System.getProperty("file.separator")+"log4j.properties");
		String content = new StringBuilder(propertiesLicenseHeader)
						.append("# Global logging configuration").append(System.getProperty("line.separator"))
						.append("log4j.rootLogger=INFO,daily").append(System.getProperty("line.separator"))
						.append("#log4j.rootLogger=info, stdout").append(System.getProperty("line.separator"))
						.append("# Console output").append(System.getProperty("line.separator"))
						.append("log4j.appender.stdout=org.apache.log4j.ConsoleAppender").append(System.getProperty("line.separator"))
						.append("log4j.appender.stdout.layout=org.apache.log4j.PatternLayout").append(System.getProperty("line.separator"))
						.append("log4j.appender.stdout.layout.ConversionPattern=%5p [%t] %c - %m%n \n").append(System.getProperty("line.separator"))
						.append("# daily Appender").append(System.getProperty("line.separator"))
						.append("log4j.appender.daily=org.apache.log4j.DailyRollingFileAppender").append(System.getProperty("line.separator"))
						.append("log4j.appender.daily.File=${catalina.home}${file.separator}logs${file.separator}"+cdName+".log").append(System.getProperty("line.separator"))
						.append("log4j.appender.daily.DatePattern='.'yyyy-MM-dd").append(System.getProperty("line.separator"))
						.append("log4j.appender.daily.layout=org.apache.log4j.PatternLayout").append(System.getProperty("line.separator"))
						.append("log4j.appender.daily.layout.ConversionPattern=%d %-5p [%t] %c - %m%n \n")
						.toString();
		try {
			FileUtils.writeStringToFile(log4j,content,Charset.defaultCharset());
		} catch (IOException e) {
			logger.info(e.getMessage());
		}
	}
		
	public static void createConfigurationProperties(String projectDir,String targetNS,String serviceName, String portName, String cdName,String role,String cdsPriorities,boolean monitor,String monitor_host,String monitor_port){
		
		String coordName = cdName+".coord";
		File config = new File(projectDir+System.getProperty("file.separator")+"WEB-INF"+System.getProperty("file.separator")+"classes"+System.getProperty("file.separator")+"configuration.properties");		
		String content = new StringBuilder(propertiesLicenseHeader)
						.append("cd_name="+cdName).append(System.getProperty("line.separator"))
						.append("cd_role="+role).append(System.getProperty("line.separator"))
						.append("coord_name="+coordName).append(System.getProperty("line.separator"))
						.append("port_namespaceURI="+targetNS).append(System.getProperty("line.separator"))
						.append("port_localPart="+portName).append(System.getProperty("line.separator"))
						.append("service_namespaceURI="+targetNS).append(System.getProperty("line.separator"))
						.append("service_localPart="+serviceName).append(System.getProperty("line.separator"))
						.append("cds_priorities="+cdsPriorities).append(System.getProperty("line.separator"))
						.append("monitor="+monitor).append(System.getProperty("line.separator"))
						.append("monitor_host="+monitor_host).append(System.getProperty("line.separator"))
						.append("monitor_port="+monitor_port).append(System.getProperty("line.separator"))
						.toString();
		try {
			FileUtils.writeStringToFile(config,content,Charset.defaultCharset());
		} catch (IOException e) {
			logger.info(e.getMessage());
		}
	}
			
	public static void createWSDL(String projectDir,String targetNS, String cdName, String role,String portName,String serviceName, List<Operation> operations){
		
		File wsdl = new File(projectDir+System.getProperty("file.separator")+"WEB-INF"+System.getProperty("file.separator")+"wsdl"+System.getProperty("file.separator")+cdName+".wsdl");
		String portTypeName = portName+"Port";
		String outMessageDefinitionType;
		String elementOutMessageDefinitionType;
		
		String content = new StringBuilder(xmlLicenseHeader)
							.append("<definitions xmlns=\"http://schemas.xmlsoap.org/wsdl/\"").append(System.getProperty("line.separator"))
							.append("\t \t xmlns:soap=\"http://schemas.xmlsoap.org/wsdl/soap/\"").append(System.getProperty("line.separator"))
							.append("\t \t xmlns:tns=\""+targetNS+"\"").append(System.getProperty("line.separator"))
							.append("\t \t xmlns:wsam=\"http://www.w3.org/2007/05/addressing/metadata\"").append(System.getProperty("line.separator"))
							.append("\t \t xmlns:wsp=\"http://www.w3.org/ns/ws-policy\"").append(System.getProperty("line.separator"))
							.append("\t \t xmlns:wsp1_2=\"http://schemas.xmlsoap.org/ws/2004/09/policy\"").append(System.getProperty("line.separator"))
							.append("\t \t xmlns:wsu=\"http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd\"").append(System.getProperty("line.separator"))
							.append("\t \t xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"").append(System.getProperty("line.separator"))
							.append("\t \t targetNamespace=\""+targetNS+"\"").append(System.getProperty("line.separator"))
							.append("\t \t name=\""+serviceName+"\">").append(System.getProperty("line.separator"))
							.append("\t <types>").append(System.getProperty("line.separator"))
							.append("\t \t \t <xsd:schema version=\"1.0\" targetNamespace=\""+targetNS+"\">").append(System.getProperty("line.separator")).toString();
							
		for (Operation operation : operations) {
			String inTypeName = StringUtils.substringBetween(operation.getInMessageDefinition(), "<xsd:complexType name=\"", "\">");
			String outTypeName = StringUtils.substringBetween(operation.getOutMessageDefinition(), "<xsd:complexType name=\"", "\">");
			content = new StringBuilder(content)
					.append("\t \t \t <xsd:element name=\""+operation.getName()+"\" type=\"tns:"+inTypeName+"\"></xsd:element>").append(System.getProperty("line.separator")).toString();	
			if(operation.getOutMessageDefinition()!=null){
				elementOutMessageDefinitionType = new StringBuilder()
								.append("\t \t \t <xsd:element name=\""+operation.getName()+"Response"+"\" type=\"tns:"+outTypeName+"\"></xsd:element>").append(System.getProperty("line.separator")).toString();							
			}
			else{
				elementOutMessageDefinitionType = new StringBuilder()	
								.append("\t \t \t <xsd:element name=\""+operation.getName()+"Response"+"\" type=\"tns:"+operation.getName()+"Response"+"\"></xsd:element>").append(System.getProperty("line.separator")).toString();	
			}
			content = new StringBuilder(content)
					.append(elementOutMessageDefinitionType).append(System.getProperty("line.separator")).toString();
		}
		content = new StringBuilder(content)
					.append("\t \t \t <xsd:element name=\"setInvocationAddress\" type=\"tns:setInvocationAddress\"></xsd:element>").append(System.getProperty("line.separator"))
					.append("\t \t \t <xsd:element name=\"setInvocationAddressResponse\" type=\"tns:setInvocationAddressResponse\"></xsd:element>").append(System.getProperty("line.separator")).toString();

		for (Operation operation : operations) {
			content = new StringBuilder(content)	
						.append("\t \t \t").append(operation.getInMessageDefinition()).append(System.getProperty("line.separator")).toString();
			if(operation.getOutMessageDefinition()!=null){
				outMessageDefinitionType = new StringBuilder()
											.append("\t \t \t").append(operation.getOutMessageDefinition()).append(System.getProperty("line.separator")).toString();
			}
			else{
				outMessageDefinitionType = new StringBuilder()
											.append("\t \t \t <xsd:complexType name=\""+operation.getName()+"Response"+"\">").append(System.getProperty("line.separator"))
											.append("\t \t \t \t <xsd:sequence></xsd:sequence>").append(System.getProperty("line.separator"))
											.append("\t \t \t </xsd:complexType>").append(System.getProperty("line.separator")).toString();										
			}
			content = new StringBuilder(content)				
					.append(outMessageDefinitionType).append(System.getProperty("line.separator")).toString();	
		}
		content = new StringBuilder(content)															
							.append("\t \t \t <xsd:complexType name=\"setInvocationAddress\">").append(System.getProperty("line.separator"))
							.append("\t \t \t \t <xsd:sequence>").append(System.getProperty("line.separator"))
							.append("\t \t \t \t \t <xsd:element name=\"role\" type=\"xsd:string\"/>").append(System.getProperty("line.separator"))
							.append("\t \t \t \t \t <xsd:element name=\"name\" type=\"xsd:string\"/>").append(System.getProperty("line.separator"))
							.append("\t \t \t \t \t <xsd:element name=\"endpoints\" type=\"xsd:string\" maxOccurs=\"unbounded\"/>").append(System.getProperty("line.separator"))
							.append("\t \t \t \t </xsd:sequence>").append(System.getProperty("line.separator"))
							.append("\t \t \t </xsd:complexType>").append(System.getProperty("line.separator"))
							.append("\t \t \t <xsd:complexType name=\"setInvocationAddressResponse\">").append(System.getProperty("line.separator"))
							.append("\t \t \t \t <xsd:sequence></xsd:sequence>").append(System.getProperty("line.separator"))
							.append("\t \t \t </xsd:complexType>").append(System.getProperty("line.separator"))
							.append("\t \t \t </xsd:schema>").append(System.getProperty("line.separator"))
							.append("\t </types>").append(System.getProperty("line.separator")).toString();
					
		for (Operation operation : operations) {
			content = new StringBuilder(content)
							.append("\t <message name=\""+operation.getName()+"\">").append(System.getProperty("line.separator"))
							.append("\t \t <part name=\"parameters\" element=\"tns:"+operation.getName()+"\"/>").append(System.getProperty("line.separator"))
							.append("\t </message>").append(System.getProperty("line.separator"))
							.append("\t <message name=\""+operation.getName()+"Response\">").append(System.getProperty("line.separator"))
							.append("\t \t <part name=\"parameters\" element=\"tns:"+operation.getName()+"Response\"/>").append(System.getProperty("line.separator"))
							.append("\t </message>").append(System.getProperty("line.separator")).toString();
		}
		content = new StringBuilder(content)
							.append("\t <message name=\"setInvocationAddress\">").append(System.getProperty("line.separator"))
							.append("\t \t <part name=\"parameters\" element=\"tns:setInvocationAddress\"></part>").append(System.getProperty("line.separator"))
							.append("\t </message>").append(System.getProperty("line.separator"))
							.append("\t <message name=\"setInvocationAddressResponse\">").append(System.getProperty("line.separator"))
							.append("\t \t <part name=\"parameters\" element=\"tns:setInvocationAddressResponse\"></part>").append(System.getProperty("line.separator"))
							.append("\t </message>").append(System.getProperty("line.separator")).toString();
		content = new StringBuilder(content)					
							.append("\t <portType name=\""+portTypeName+"\">").append(System.getProperty("line.separator")).toString();
		for (Operation operation : operations) {
			content = new StringBuilder(content)	
							.append("\t \t <operation name=\""+operation.getName()+"\">").append(System.getProperty("line.separator"))
							.append("\t \t \t <input wsam:Action=\""+targetNS+operation.getName()+"\" message=\"tns:"+operation.getName()+"\"/>").append(System.getProperty("line.separator"))
							.append("\t \t \t <output wsam:Action=\""+targetNS+operation.getName()+"Response"+"\" message=\"tns:"+operation.getName()+"Response"+"\"/>").append(System.getProperty("line.separator"))
							.append("\t \t </operation>").append(System.getProperty("line.separator")).toString();
		}					
		content = new StringBuilder(content)					
							.append("\t \t <operation name=\"setInvocationAddress\">").append(System.getProperty("line.separator"))
							.append("\t \t \t <input wsam:Action=\""+targetNS+"setInvocationAddress\" message=\"tns:setInvocationAddress\"/>").append(System.getProperty("line.separator"))
							.append("\t \t \t <output wsam:Action=\""+targetNS+"setInvocationAddressResponse\" message=\"tns:setInvocationAddressResponse\"/>").append(System.getProperty("line.separator"))
							.append("\t \t </operation>").append(System.getProperty("line.separator"))	
							.append("\t </portType>").append(System.getProperty("line.separator"))
							.append("\t \t <binding name=\""+portTypeName+"Binding\" type=\"tns:"+portTypeName+"\">").append(System.getProperty("line.separator"))
							.append("\t \t \t <soap:binding transport=\"http://schemas.xmlsoap.org/soap/http\" style=\"document\"></soap:binding>").append(System.getProperty("line.separator")).toString();
							
		for (Operation operation : operations) {
			content = new StringBuilder(content)
					.append("\t \t \t <operation name=\""+operation.getName()+"\">").append(System.getProperty("line.separator"))
					.append("\t \t \t \t <soap:operation soapAction=\"\"/>").append(System.getProperty("line.separator"))
					.append("\t \t \t \t <input>").append(System.getProperty("line.separator"))		
					.append("\t \t \t \t \t <soap:body use=\"literal\"/>").append(System.getProperty("line.separator"))	
					.append("\t \t \t \t </input>").append(System.getProperty("line.separator"))	
					.append("\t \t \t \t <output>").append(System.getProperty("line.separator"))		
					.append("\t \t \t \t \t <soap:body use=\"literal\"/>").append(System.getProperty("line.separator"))	
					.append("\t \t \t \t </output>").append(System.getProperty("line.separator"))	
					.append("\t \t \t </operation>").append(System.getProperty("line.separator")).toString();
		}
		content = new StringBuilder(content)								
					.append("\t \t \t <operation name=\"setInvocationAddress\">").append(System.getProperty("line.separator"))
					.append("\t \t \t \t <soap:operation soapAction=\"\"/>").append(System.getProperty("line.separator"))
					.append("\t \t \t \t <input>").append(System.getProperty("line.separator"))		
					.append("\t \t \t \t \t <soap:body use=\"literal\"/>").append(System.getProperty("line.separator"))	
					.append("\t \t \t \t </input>").append(System.getProperty("line.separator"))	
					.append("\t \t \t \t <output>").append(System.getProperty("line.separator"))		
					.append("\t \t \t \t \t <soap:body use=\"literal\"/>").append(System.getProperty("line.separator"))	
					.append("\t \t \t \t </output>").append(System.getProperty("line.separator"))	
					.append("\t \t \t </operation>").append(System.getProperty("line.separator"))
					.append("\t \t </binding>").append(System.getProperty("line.separator"))
					.append("\t \t <service name=\""+serviceName+"\">").append(System.getProperty("line.separator"))
					.append("\t \t \t <port name=\""+portName+"\" binding=\"tns:"+portTypeName+"Binding\">").append(System.getProperty("line.separator"))
					.append("\t \t \t \t <soap:address location=\"http://localhost:8080/"+cdName+"/"+cdName+"\"></soap:address>").append(System.getProperty("line.separator"))
					.append("\t \t \t </port>").append(System.getProperty("line.separator"))
					.append("\t \t </service>").append(System.getProperty("line.separator"))
					.append("</definitions>").append(System.getProperty("line.separator"))
					.toString();
		try {
			FileUtils.writeStringToFile(wsdl,content,Charset.defaultCharset());
		} catch (IOException e) {
			logger.info(e.getMessage());
		}
	}	
	
}
