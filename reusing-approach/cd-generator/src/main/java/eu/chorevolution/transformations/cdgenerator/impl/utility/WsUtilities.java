/*
* Copyright 2015 The CHOReVOLUTION project
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package eu.chorevolution.transformations.cdgenerator.impl.utility;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.sun.tools.ws.WsImport;

public class WsUtilities {
	
	private static Logger logger = LoggerFactory.getLogger(WsUtilities.class);
	
	public static void generateWebServiceSkeletonCode(String destDir,String packageName, String wsdlLocation){

		String[] arg = {"-verbose",
				"-d",destDir+System.getProperty("file.separator")+"WEB-INF"+System.getProperty("file.separator")+"classes",
				"-p",packageName,
				wsdlLocation};
		try {
			WsImport.main(arg);
		} catch (Throwable e) {
			logger.info(e.getMessage());
		}
	}
	
	public static void generateWebServiceCodeSkeletonCodeExternalProcess(String destDir,String packageName, String wsdlLocation){
		
		Runtime rt = Runtime.getRuntime();
		try {
			rt.exec("wsimport "+wsdlLocation+" -p "+packageName+ " -d "+destDir+System.getProperty("file.separator")+"WEB-INF"+System.getProperty("file.separator")+"classes -verbose ").waitFor();
		} catch (IOException | InterruptedException e) {
			logger.info(e.getMessage());
		}
	}

}
