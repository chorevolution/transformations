/*
* Copyright 2015 The CHOReVOLUTION project
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package eu.chorevolution.transformations.cdgenerator.model;

public class CD {

	String name; 
	String role; 
	byte[] coord;
	byte[] war;
	
	public CD() {
	}

	public CD(String name, String role, byte[] coord) {
		this.name = name;
		this.role = role;
		this.coord = coord;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public byte[] getCoord() {
		return coord;
	}

	public void setCoord(byte[] coord) {
		this.coord = coord;
	}

	public byte[] getWar() {
		return war;
	}

	public void setWar(byte[] war) {
		this.war = war;
	}
	
}
