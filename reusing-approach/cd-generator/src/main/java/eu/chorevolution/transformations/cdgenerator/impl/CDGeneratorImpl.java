/*
* Copyright 2015 The CHOReVOLUTION project
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package eu.chorevolution.transformations.cdgenerator.impl;

import java.io.File;
import java.util.List;

import org.apache.commons.io.FileUtils;

import eu.chorevolution.modelingnotations.coord.COORDModel;
import eu.chorevolution.transformations.cdgenerator.CDGenerator;
import eu.chorevolution.transformations.cdgenerator.CDGeneratorException;
import eu.chorevolution.transformations.cdgenerator.impl.utility.CoordmetamodelParser;
import eu.chorevolution.transformations.cdgenerator.impl.utility.Utilities;
import eu.chorevolution.transformations.cdgenerator.impl.utility.WsUtilities;
import eu.chorevolution.transformations.cdgenerator.model.CD;
import eu.chorevolution.transformations.cdgenerator.model.Operation;

public class CDGeneratorImpl implements CDGenerator {
	
	private static final String PACKAGENAME_DEFAULT = "eu.chorevolution.cd.";
	private static final String TARGETNSCD_DEFAULT = "http://smt.chorevolution.softeco.it/";
	private static final String TARGETNS_SERVICES_WP4 = "http://eu.chorevolution.urbantrafficcoordination.searp/";	
	private static final String TARGETNS_SERVICES_WP5 = "http://smt.chorevolution.softeco.it/";		
	private static final String PORTNAME_DEFAULT_WP4 = "Port";
	private static final String SERVICENAME_DEFAULT = "Service";
	private static final boolean MONITOR_DEFAULT = false;
	private static final String MONITOR_HOST_DEFAULT = "localhost";
	private static final String MONITOR_PORT_DEFAULT = "9080";
	
	
	@Override
	public CD generateCoordinationDelegate(String choreographyName,String cdName, String role, byte[] coordModel, String cdsPriorities) throws CDGeneratorException {
		
		String targetNS_SA;
		String portName_SA="";
		String serviceName_SA="";
		String targetNS_CD = TARGETNSCD_DEFAULT;
		
		if(choreographyName.equalsIgnoreCase("wp4")){
			targetNS_SA = TARGETNS_SERVICES_WP4;
			switch (role) {
				case "CD-Bs-map-Dts-google":
					role="DtsGoogle";
					break;
				case "CD-Bs-map-Dts-here":
					role="DtsHere";
					break;
				case "CD-Bs-map-Nd":
					role="Nd";
					break;
				case "CD-Bs-map-Trvc":
					role="Trvc";
					break;
				case "CD-Dts-google-Bs-map":
					role="BsMap";
					break;
				case "CD-Dts-here-Bs-map":
					role="BsMap";
					break;
				case "CD-Dts-trv-acc-Trvc":
					role="Trvc";
					break;
				case "CD-Nd-Bs-map":
					role="BsMap";
					break;
				case "CD-Trvc-Bs-map":
					role="BsMap";
					break;
				case "CD-Trvc-Dts-trv-acc":
					role="DtsTrvAcc";
					break;				
			}
			portName_SA = role+PORTNAME_DEFAULT_WP4;
			serviceName_SA = role+SERVICENAME_DEFAULT;
		}
		else{			
			targetNS_SA = TARGETNS_SERVICES_WP5;
			switch (role) {
				case "CD-JourneyPlanner-TripPlanner":
					role="tripPlanner";
					portName_SA = "tripPlanner";
					serviceName_SA = "TripPlannerService";
					break;
				case "CD-OsmParking-TripPlanner":
					role="tripPlanner";
					portName_SA = "tripPlanner";
					serviceName_SA = "TripPlannerService";
					break;
				case "CD-PublicTransportation-TripPlanner":
					role="tripPlanner";
					portName_SA = "tripPlanner";
					serviceName_SA = "TripPlannerService";
					break;
				case "CD-Stapp-TouristAgent":
					role="touristAgent";
					portName_SA = "touristAgent";
					serviceName_SA = "TouristAgentService";
					break;
				case "CD-TouristAgent-News":
					role="news";
					portName_SA = "News";
					serviceName_SA = "NewsService";
					break;
				case "CD-TouristAgent-Poi":
					role="poi";
					portName_SA = "poi";
					serviceName_SA = "PoiService";
					break;	
				case "CD-TouristAgent-Stapp":
					role="stapp";
					portName_SA = "stapp";
					serviceName_SA = "StappService";
					break;
				case "CD-TouristAgent-TripPlanner":
					role="tripPlanner";
					portName_SA = "tripPlanner";
					serviceName_SA = "TripPlannerService";
					break;
				case "CD-TrafficInformation-TripPlanner":
					role="tripPlanner";
					portName_SA = "tripPlanner";
					serviceName_SA = "TripPlannerService";
					break;		
				case "CD-TripPlanner-JourneyPlanner":
					role="journeyPlanner";
					portName_SA = "journeyPlanner";
					serviceName_SA = "JourneyPlannerService";
					break;		
				case "CD-TripPlanner-OsmParking":
					role="osmParking";
					portName_SA = "osmParking";
					serviceName_SA = "OSMParkingService";
					break;		
				case "CD-TripPlanner-PublicTransportation":
					role="publicTransportationInformation";
					portName_SA = "publicTransportationInformation";
					serviceName_SA = "PublicTransportationInformationService";
					break;	
				case "CD-TripPlanner-TrafficInformation":
					role="trafficInformation";
					portName_SA = "trafficInformation";
					serviceName_SA = "TrafficInformationService";
					break;	
				case "CD-TripPlanner-Weather":
					role="weather";
					portName_SA = "weather";
					serviceName_SA = "WeatherService";
					break;	
				case "CD-Weather-TripPlanner":
					role="tripPlanner";
					portName_SA = "tripPlanner";
					serviceName_SA = "TripPlannerService";
					break;							
			}
			/*
			if(role.equals("CD-TripPlanner-OsmParking")){
				role="osmParking";
				portName_SA = role;
				serviceName_SA = "OSMParking"+SERVICENAME_DEFAULT;
			}
			else{
				if(role.equals("CD-Stapp-TouristAgent")){
					role="TouristAgent";
					portName_SA = "touristAgent";
					serviceName_SA = "TouristAgentService";
				}
				else{
					role = role.split("-")[2];	
					portName_SA = StringUtils.uncapitalize(role);
					serviceName_SA = role+SERVICENAME_DEFAULT;
				}	
			}
			targetNS_SA = TARGETNS_SERVICES_WP5;
			*/
		}
		String packageName = PACKAGENAME_DEFAULT+role.toLowerCase();
		CD cd = new CD(cdName,role,coordModel);
		String destDir = FileUtils.getTempDirectoryPath();
		String initialDestDir = destDir;

		
		destDir = Utilities.getDestinationFolderPath(destDir);
		String wsdlName = cd.getName()+".wsdl";
		String wsdlLocation = destDir+System.getProperty("file.separator")+"WEB-INF"+System.getProperty("file.separator")+"wsdl"+System.getProperty("file.separator")+wsdlName;
		
		File coordFile = Utilities.createCoordModel(destDir, cd.getName(), cd.getCoord());	
		COORDModel coord = CoordmetamodelParser.parse(coordFile);	
		List<Operation> operations = Utilities.getOperationsFromCoordModel(coord);
		Utilities.createWebXml(destDir, cd.getName());
		Utilities.createWSDL(destDir,targetNS_CD,cd.getName(),cd.getRole(),portName_SA,serviceName_SA,operations);
		
		
		Utilities.createSunJaxWS(destDir, cd.getName(), wsdlLocation,packageName, targetNS_CD, wsdlName);
		Utilities.createLog4jProperties(destDir, cd.getName());
		Utilities.createConfigurationProperties(destDir, targetNS_SA, serviceName_SA, portName_SA, cdName, role, cdsPriorities,MONITOR_DEFAULT,MONITOR_HOST_DEFAULT,MONITOR_PORT_DEFAULT);
		WsUtilities.generateWebServiceCodeSkeletonCodeExternalProcess(destDir, packageName, wsdlLocation);
		Utilities.createWar(cd.getName(), destDir);
		Utilities.deleteProjectFolder(destDir);
		
		cd.setWar(Utilities.getBytesFromWar(initialDestDir,cd.getName()));
		Utilities.deleteWar(initialDestDir,cd.getName());
		return cd;
	}

}
