/*
* Copyright 2015 The CHOReVOLUTION project
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package eu.chorevolution.transformations.cdgenerator.model;

public class Operation {

	private String name;
	private String inMessageDefinition;
	private String outMessageDefinition;
	
	public Operation(String name, String inMessageDefinition, String outMessageDefinition) {
		this.name = name;
		this.inMessageDefinition = inMessageDefinition;
		this.outMessageDefinition = outMessageDefinition;
	}
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getInMessageDefinition() {
		return inMessageDefinition;
	}
	
	public void setInMessageDefinition(String inMessageDefinition) {
		this.inMessageDefinition = inMessageDefinition;
	}
	
	public String getOutMessageDefinition() {
		return outMessageDefinition;
	}
	
	public void setOutMessageDefinition(String outMessageDefinition) {
		this.outMessageDefinition = outMessageDefinition;
	}
	
	
}
