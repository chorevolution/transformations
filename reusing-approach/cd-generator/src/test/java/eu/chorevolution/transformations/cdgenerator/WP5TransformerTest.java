/*
  * Copyright 2015 The CHOReVOLUTION project
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  *      http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */
package eu.chorevolution.transformations.cdgenerator;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.chorevolution.transformations.cdgenerator.impl.CDGeneratorImpl;
import eu.chorevolution.transformations.cdgenerator.model.CD;

public class WP5TransformerTest {

	private static final String TEST_RESOURCES = "." + File.separatorChar + "src" + File.separatorChar + "test" + File.separatorChar + "resources" + File.separatorChar;

	private static Logger logger = LoggerFactory.getLogger(WP5TransformerTest.class);
	String cdsPriorities = "CD-JourneyPlanner-TripPlanner:9,CD-OsmParking-TripPlanner:2,CD-PublicTransportation-TripPlanner:3,CD-Stapp-TouristAgent:4,CD-TouristAgent-News:5,CD-TouristAgent-Poi:6,CD-TouristAgent-Stapp:7,CD-TouristAgent-TripPlanner:8,CD-TrafficInformation-TripPlanner:1,CD-TripPlanner-JourneyPlanner:10,CD-TripPlanner-OsmParking:11,CD-TripPlanner-PublicTransportation:12,CD-TripPlanner-TrafficInformation:13,CD-TripPlanner-Weather:14,CD-Weather-TripPlanner:15";
	
	
	@Before
	public void setUp() {
	}

	@Test
	public void generateCD_01()  {
		try{
			CDGenerator cdGenerator = new CDGeneratorImpl();
			byte[] bytes = FileUtils.readFileToByteArray(new File(TEST_RESOURCES + "test_02" + File.separatorChar + "in_coord" + File.separatorChar+"CD-JourneyPlanner-TripPlanner.coord"));
			CD cd = cdGenerator.generateCoordinationDelegate("wp5","CD-JourneyPlanner-TripPlanner","CD-JourneyPlanner-TripPlanner",bytes,cdsPriorities);
			FileUtils.writeByteArrayToFile(new File(TEST_RESOURCES + "test_02" + File.separatorChar + "out_war" + File.separatorChar+"CD-JourneyPlanner-TripPlanner.war"), cd.getWar());
		}catch( IOException e){
			logger.error("generateCD_01 > " + e.getMessage());
			Assert.assertTrue(false);	
		}
		
	}
	@Test
	public void generateCD_02()  {
		try{
			CDGenerator cdGenerator = new CDGeneratorImpl();
			byte[] bytes = FileUtils.readFileToByteArray(new File(TEST_RESOURCES + "test_02" + File.separatorChar + "in_coord" + File.separatorChar+"CD-OsmParking-TripPlanner.coord"));
			CD cd = cdGenerator.generateCoordinationDelegate("wp5","CD-OsmParking-TripPlanner","CD-OsmParking-TripPlanner",bytes,cdsPriorities);
			FileUtils.writeByteArrayToFile(new File(TEST_RESOURCES + "test_02" + File.separatorChar + "out_war" + File.separatorChar+"CD-OsmParking-TripPlanner.war"), cd.getWar());
		}catch( IOException e){
			logger.error("generateCD_02 > " + e.getMessage());
			Assert.assertTrue(false);	
		}
		
	}
	
	@Test
	public void generateCD_03()  {
		try{
			CDGenerator cdGenerator = new CDGeneratorImpl();
			byte[] bytes = FileUtils.readFileToByteArray(new File(TEST_RESOURCES + "test_02" + File.separatorChar + "in_coord" + File.separatorChar+"CD-PublicTransportation-TripPlanner.coord"));
			CD cd = cdGenerator.generateCoordinationDelegate("wp5","CD-PublicTransportation-TripPlanner","CD-PublicTransportation-TripPlanner",bytes,cdsPriorities);
			FileUtils.writeByteArrayToFile(new File(TEST_RESOURCES + "test_02" + File.separatorChar + "out_war" + File.separatorChar+"CD-PublicTransportation-TripPlanner.war"), cd.getWar());
		}catch( IOException e){
			logger.error("generateCD_03 > " + e.getMessage());
			Assert.assertTrue(false);	
		}
		
	}

	@Test
	public void generateCD_04()  {
		try{
			CDGenerator cdGenerator = new CDGeneratorImpl();
			byte[] bytes = FileUtils.readFileToByteArray(new File(TEST_RESOURCES + "test_02" + File.separatorChar + "in_coord" + File.separatorChar+"CD-Stapp-TouristAgent.coord"));
			CD cd = cdGenerator.generateCoordinationDelegate("wp5","CD-Stapp-TouristAgent","CD-Stapp-TouristAgent",bytes,cdsPriorities);
			FileUtils.writeByteArrayToFile(new File(TEST_RESOURCES + "test_02" + File.separatorChar + "out_war" + File.separatorChar+"CD-Stapp-TouristAgent.war"), cd.getWar());
		}catch( IOException e){
			logger.error("generateCD_04 > " + e.getMessage());
			Assert.assertTrue(false);	
		}
		
	}
	
	@Test
	public void generateCD_05()  {
		try{
			CDGenerator cdGenerator = new CDGeneratorImpl();
			byte[] bytes = FileUtils.readFileToByteArray(new File(TEST_RESOURCES + "test_02" + File.separatorChar + "in_coord" + File.separatorChar+"CD-TouristAgent-News.coord"));
			CD cd = cdGenerator.generateCoordinationDelegate("wp5","CD-TouristAgent-News","CD-TouristAgent-News",bytes,cdsPriorities);
			FileUtils.writeByteArrayToFile(new File(TEST_RESOURCES + "test_02" + File.separatorChar + "out_war" + File.separatorChar+"CD-TouristAgent-News.war"), cd.getWar());
		}catch( IOException e){
			logger.error("generateCD_05 > " + e.getMessage());
			Assert.assertTrue(false);	
		}
		
	}
	
	@Test
	public void generateCD_06()  {
		try{
			CDGenerator cdGenerator = new CDGeneratorImpl();
			byte[] bytes = FileUtils.readFileToByteArray(new File(TEST_RESOURCES + "test_02" + File.separatorChar + "in_coord" + File.separatorChar+"CD-TouristAgent-Poi.coord"));
			CD cd = cdGenerator.generateCoordinationDelegate("wp5","CD-TouristAgent-Poi","CD-TouristAgent-Poi",bytes,cdsPriorities);
			FileUtils.writeByteArrayToFile(new File(TEST_RESOURCES + "test_02" + File.separatorChar + "out_war" + File.separatorChar+"CD-TouristAgent-Poi.war"), cd.getWar());
		}catch( IOException e){
			logger.error("generateCD_06 > " + e.getMessage());
			Assert.assertTrue(false);	
		}
		
	}
	
	@Test
	public void generateCD_07()  {
		try{
			CDGenerator cdGenerator = new CDGeneratorImpl();
			byte[] bytes = FileUtils.readFileToByteArray(new File(TEST_RESOURCES + "test_02" + File.separatorChar + "in_coord" + File.separatorChar+"CD-TouristAgent-Stapp.coord"));
			CD cd = cdGenerator.generateCoordinationDelegate("wp5","CD-TouristAgent-Stapp","CD-TouristAgent-Stapp",bytes,cdsPriorities);
			FileUtils.writeByteArrayToFile(new File(TEST_RESOURCES + "test_02" + File.separatorChar + "out_war" + File.separatorChar+"CD-TouristAgent-Stapp.war"), cd.getWar());
		}catch( IOException e){
			logger.error("generateCD_07 > " + e.getMessage());
			Assert.assertTrue(false);	
		}
		
	}
	
	@Test
	public void generateCD_08()  {
		try{
			CDGenerator cdGenerator = new CDGeneratorImpl();
			byte[] bytes = FileUtils.readFileToByteArray(new File(TEST_RESOURCES + "test_02" + File.separatorChar + "in_coord" + File.separatorChar+"CD-TouristAgent-TripPlanner.coord"));
			CD cd = cdGenerator.generateCoordinationDelegate("wp5","CD-TouristAgent-TripPlanner","CD-TouristAgent-TripPlanner",bytes,cdsPriorities);
			FileUtils.writeByteArrayToFile(new File(TEST_RESOURCES + "test_02" + File.separatorChar + "out_war" + File.separatorChar+"CD-TouristAgent-TripPlanner.war"), cd.getWar());
		}catch( IOException e){
			logger.error("generateCD_08 > " + e.getMessage());
			Assert.assertTrue(false);	
		}
		
	}	
	
	@Test
	public void generateCD_09()  {
		try{
			CDGenerator cdGenerator = new CDGeneratorImpl();
			byte[] bytes = FileUtils.readFileToByteArray(new File(TEST_RESOURCES + "test_02" + File.separatorChar + "in_coord" + File.separatorChar+"CD-TrafficInformation-TripPlanner.coord"));
			CD cd = cdGenerator.generateCoordinationDelegate("wp5","CD-TrafficInformation-TripPlanner","CD-TrafficInformation-TripPlanner",bytes,cdsPriorities);
			FileUtils.writeByteArrayToFile(new File(TEST_RESOURCES + "test_02" + File.separatorChar + "out_war" + File.separatorChar+"CD-TrafficInformation-TripPlanner.war"), cd.getWar());
		}catch( IOException e){
			logger.error("generateCD_09 > " + e.getMessage());
			Assert.assertTrue(false);	
		}
		
	}		
	
	@Test
	public void generateCD_10()  {
		try{
			CDGenerator cdGenerator = new CDGeneratorImpl();
			byte[] bytes = FileUtils.readFileToByteArray(new File(TEST_RESOURCES + "test_02" + File.separatorChar + "in_coord" + File.separatorChar+"CD-TripPlanner-JourneyPlanner.coord"));
			CD cd = cdGenerator.generateCoordinationDelegate("wp5","CD-TripPlanner-JourneyPlanner","CD-TripPlanner-JourneyPlanner",bytes,cdsPriorities);
			FileUtils.writeByteArrayToFile(new File(TEST_RESOURCES + "test_02" + File.separatorChar + "out_war" + File.separatorChar+"CD-TripPlanner-JourneyPlanner.war"), cd.getWar());
		}catch( IOException e){
			logger.error("generateCD_10 > " + e.getMessage());
			Assert.assertTrue(false);	
		}
		
	}	
	
	@Test
	public void generateCD_11()  {
		try{
			CDGenerator cdGenerator = new CDGeneratorImpl();
			byte[] bytes = FileUtils.readFileToByteArray(new File(TEST_RESOURCES + "test_02" + File.separatorChar + "in_coord" + File.separatorChar+"CD-TripPlanner-OsmParking.coord"));
			CD cd = cdGenerator.generateCoordinationDelegate("wp5","CD-TripPlanner-OsmParking","CD-TripPlanner-OsmParking",bytes,cdsPriorities);
			FileUtils.writeByteArrayToFile(new File(TEST_RESOURCES + "test_02" + File.separatorChar + "out_war" + File.separatorChar+"CD-TripPlanner-OsmParking.war"), cd.getWar());
		}catch( IOException e){
			logger.error("generateCD_11 > " + e.getMessage());
			Assert.assertTrue(false);	
		}
		
	}	
	
	@Test
	public void generateCD_12()  {
		try{
			CDGenerator cdGenerator = new CDGeneratorImpl();
			byte[] bytes = FileUtils.readFileToByteArray(new File(TEST_RESOURCES + "test_02" + File.separatorChar + "in_coord" + File.separatorChar+"CD-TripPlanner-PublicTransportation.coord"));
			CD cd = cdGenerator.generateCoordinationDelegate("wp5","CD-TripPlanner-PublicTransportation","CD-TripPlanner-PublicTransportation",bytes,cdsPriorities);
			FileUtils.writeByteArrayToFile(new File(TEST_RESOURCES + "test_02" + File.separatorChar + "out_war" + File.separatorChar+"CD-TripPlanner-PublicTransportation.war"), cd.getWar());
		}catch( IOException e){
			logger.error("generateCD_12 > " + e.getMessage());
			Assert.assertTrue(false);	
		}
		
	}	

	@Test
	public void generateCD_13()  {
		try{
			CDGenerator cdGenerator = new CDGeneratorImpl();
			byte[] bytes = FileUtils.readFileToByteArray(new File(TEST_RESOURCES + "test_02" + File.separatorChar + "in_coord" + File.separatorChar+"CD-TripPlanner-TrafficInformation.coord"));
			CD cd = cdGenerator.generateCoordinationDelegate("wp5","CD-TripPlanner-TrafficInformation","CD-TripPlanner-TrafficInformation",bytes,cdsPriorities);
			FileUtils.writeByteArrayToFile(new File(TEST_RESOURCES + "test_02" + File.separatorChar + "out_war" + File.separatorChar+"CD-TripPlanner-TrafficInformation.war"), cd.getWar());
		}catch( IOException e){
			logger.error("generateCD_13 > " + e.getMessage());
			Assert.assertTrue(false);	
		}
		
	}

	@Test
	public void generateCD_14()  {
		try{
			CDGenerator cdGenerator = new CDGeneratorImpl();
			byte[] bytes = FileUtils.readFileToByteArray(new File(TEST_RESOURCES + "test_02" + File.separatorChar + "in_coord" + File.separatorChar+"CD-TripPlanner-Weather.coord"));
			CD cd = cdGenerator.generateCoordinationDelegate("wp5","CD-TripPlanner-Weather","CD-TripPlanner-Weather",bytes,cdsPriorities);
			FileUtils.writeByteArrayToFile(new File(TEST_RESOURCES + "test_02" + File.separatorChar + "out_war" + File.separatorChar+"CD-TripPlanner-Weather.war"), cd.getWar());
		}catch( IOException e){
			logger.error("generateCD_14 > " + e.getMessage());
			Assert.assertTrue(false);	
		}
		
	}
	
	@Test
	public void generateCD_15()  {
		try{
			CDGenerator cdGenerator = new CDGeneratorImpl();
			byte[] bytes = FileUtils.readFileToByteArray(new File(TEST_RESOURCES + "test_02" + File.separatorChar + "in_coord" + File.separatorChar+"CD-Weather-TripPlanner.coord"));
			CD cd = cdGenerator.generateCoordinationDelegate("wp5","CD-Weather-TripPlanner","CD-Weather-TripPlanner",bytes,cdsPriorities);
			FileUtils.writeByteArrayToFile(new File(TEST_RESOURCES + "test_02" + File.separatorChar + "out_war" + File.separatorChar+"CD-Weather-TripPlanner.war"), cd.getWar());
		}catch( IOException e){
			logger.error("generateCD_15 > " + e.getMessage());
			Assert.assertTrue(false);	
		}
		
	}	
}
