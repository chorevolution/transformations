/*
  * Copyright 2015 The CHOReVOLUTION project
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  *      http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  */
package eu.chorevolution.transformations.cdgenerator;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import eu.chorevolution.transformations.cdgenerator.impl.CDGeneratorImpl;
import eu.chorevolution.transformations.cdgenerator.model.CD;



public class WP4SimplifiedVersionTransformatorTest {
	private static final String TEST_RESOURCES = "." + File.separatorChar + "src" + File.separatorChar + "test" + File.separatorChar + "resources" + File.separatorChar;

	private static Logger logger = LoggerFactory.getLogger(WP4SimplifiedVersionTransformatorTest.class);
	String cdsPriorities = "cd-bsmap-dtsgoogle:10,cd-bsmap-dtshere:12,cd-bsmap-nd:11,cd-bsmap-trvc:9,cd-dtsgoogle-bsmap:8,cd-dtshere-bsmap:7,cd-dtstrvacc-trvc:6,cd-nd-bsmap:5,cd-trvc-bsmap:4,cd-trvc-dtstrvacc:3";
	
	
	@Before
	public void setUp() {
	}

	@Test
	public void generateCD_01()  {
		try{
			CDGenerator cdGenerator = new CDGeneratorImpl();
			byte[] bytes = FileUtils.readFileToByteArray(new File(TEST_RESOURCES + "test_01" + File.separatorChar + "in_coord" + File.separatorChar+"CD-Bs-map-Dts-google.coord"));
			CD cd = cdGenerator.generateCoordinationDelegate("wp4","CD-Bs-map-Dts-google","CD-Bs-map-Dts-google",bytes,cdsPriorities);
			FileUtils.writeByteArrayToFile(new File(TEST_RESOURCES + "test_01" + File.separatorChar + "out_war" + File.separatorChar+"CD-Bs-map-Dts-google.war"), cd.getWar());
		}catch( IOException e){
			logger.error("generateCD_01 > " + e.getMessage());
			Assert.assertTrue(false);	
		}
		
	}
	@Test
	public void generateCD_02()  {
		try{
			CDGenerator cdGenerator = new CDGeneratorImpl();
			byte[] bytes = FileUtils.readFileToByteArray(new File(TEST_RESOURCES + "test_01" + File.separatorChar + "in_coord" + File.separatorChar+"CD-Bs-map-Dts-here.coord"));
			CD cd = cdGenerator.generateCoordinationDelegate("wp4","CD-Bs-map-Dts-here","CD-Bs-map-Dts-here",bytes,cdsPriorities);
			FileUtils.writeByteArrayToFile(new File(TEST_RESOURCES + "test_01" + File.separatorChar + "out_war" + File.separatorChar+"CD-Bs-map-Dts-here.war"), cd.getWar());
		}catch( IOException e){
			logger.error("generateCD_02 > " + e.getMessage());
			Assert.assertTrue(false);	
		}
		
	}
	
	@Test
	public void generateCD_03()  {
		try{
			CDGenerator cdGenerator = new CDGeneratorImpl();
			byte[] bytes = FileUtils.readFileToByteArray(new File(TEST_RESOURCES + "test_01" + File.separatorChar + "in_coord" + File.separatorChar+"CD-Bs-map-Nd.coord"));
			CD cd = cdGenerator.generateCoordinationDelegate("wp4","CD-Bs-map-Nd","CD-Bs-map-Nd",bytes,cdsPriorities);
			FileUtils.writeByteArrayToFile(new File(TEST_RESOURCES + "test_01" + File.separatorChar + "out_war" + File.separatorChar+"CD-Bs-map-Nd.war"), cd.getWar());
		}catch( IOException e){
			logger.error("generateCD_03 > " + e.getMessage());
			Assert.assertTrue(false);	
		}
		
	}

	@Test
	public void generateCD_04()  {
		try{
			CDGenerator cdGenerator = new CDGeneratorImpl();
			byte[] bytes = FileUtils.readFileToByteArray(new File(TEST_RESOURCES + "test_01" + File.separatorChar + "in_coord" + File.separatorChar+"CD-Bs-map-Trvc.coord"));
			CD cd = cdGenerator.generateCoordinationDelegate("wp4","CD-Bs-map-Trvc","CD-Bs-map-Trvc",bytes,cdsPriorities);
			FileUtils.writeByteArrayToFile(new File(TEST_RESOURCES + "test_01" + File.separatorChar + "out_war" + File.separatorChar+"CD-Bs-map-Trvc.war"), cd.getWar());
		}catch( IOException e){
			logger.error("generateCD_04 > " + e.getMessage());
			Assert.assertTrue(false);	
		}
		
	}
	
	@Test
	public void generateCD_05()  {
		try{
			CDGenerator cdGenerator = new CDGeneratorImpl();
			byte[] bytes = FileUtils.readFileToByteArray(new File(TEST_RESOURCES + "test_01" + File.separatorChar + "in_coord" + File.separatorChar+"CD-Dts-google-Bs-map.coord"));
			CD cd = cdGenerator.generateCoordinationDelegate("wp4","CD-Dts-google-Bs-map","CD-Dts-google-Bs-map",bytes,cdsPriorities);
			FileUtils.writeByteArrayToFile(new File(TEST_RESOURCES + "test_01" + File.separatorChar + "out_war" + File.separatorChar+"CD-Dts-google-Bs-map.war"), cd.getWar());
		}catch( IOException e){
			logger.error("generateCD_05 > " + e.getMessage());
			Assert.assertTrue(false);	
		}
		
	}
	
	@Test
	public void generateCD_06()  {
		try{
			CDGenerator cdGenerator = new CDGeneratorImpl();
			byte[] bytes = FileUtils.readFileToByteArray(new File(TEST_RESOURCES + "test_01" + File.separatorChar + "in_coord" + File.separatorChar+"CD-Dts-here-Bs-map.coord"));
			CD cd = cdGenerator.generateCoordinationDelegate("wp4","CD-Dts-here-Bs-map","CD-Dts-here-Bs-map",bytes,cdsPriorities);
			FileUtils.writeByteArrayToFile(new File(TEST_RESOURCES + "test_01" + File.separatorChar + "out_war" + File.separatorChar+"CD-Dts-here-Bs-map.war"), cd.getWar());
		}catch( IOException e){
			logger.error("generateCD_06 > " + e.getMessage());
			Assert.assertTrue(false);	
		}
		
	}
	
	@Test
	public void generateCD_07()  {
		try{
			CDGenerator cdGenerator = new CDGeneratorImpl();
			byte[] bytes = FileUtils.readFileToByteArray(new File(TEST_RESOURCES + "test_01" + File.separatorChar + "in_coord" + File.separatorChar+"CD-Dts-trv-acc-Trvc.coord"));
			CD cd = cdGenerator.generateCoordinationDelegate("wp4","CD-Dts-trv-acc-Trvc","CD-Dts-trv-acc-Trvc",bytes,cdsPriorities);
			FileUtils.writeByteArrayToFile(new File(TEST_RESOURCES + "test_01" + File.separatorChar + "out_war" + File.separatorChar+"CD-Dts-trv-acc-Trvc.war"), cd.getWar());
		}catch( IOException e){
			logger.error("generateCD_07 > " + e.getMessage());
			Assert.assertTrue(false);	
		}
		
	}
	
	@Test
	public void generateCD_08()  {
		try{
			CDGenerator cdGenerator = new CDGeneratorImpl();
			byte[] bytes = FileUtils.readFileToByteArray(new File(TEST_RESOURCES + "test_01" + File.separatorChar + "in_coord" + File.separatorChar+"CD-Nd-Bs-map.coord"));
			CD cd = cdGenerator.generateCoordinationDelegate("wp4","CD-Nd-Bs-map","CD-Nd-Bs-map",bytes,cdsPriorities);
			FileUtils.writeByteArrayToFile(new File(TEST_RESOURCES + "test_01" + File.separatorChar + "out_war" + File.separatorChar+"CD-Nd-Bs-map.war"), cd.getWar());
		}catch( IOException e){
			logger.error("generateCD_08 > " + e.getMessage());
			Assert.assertTrue(false);	
		}
		
	}	
	
	@Test
	public void generateCD_09()  {
		try{
			CDGenerator cdGenerator = new CDGeneratorImpl();
			byte[] bytes = FileUtils.readFileToByteArray(new File(TEST_RESOURCES + "test_01" + File.separatorChar + "in_coord" + File.separatorChar+"CD-Trvc-Bs-map.coord"));
			CD cd = cdGenerator.generateCoordinationDelegate("wp4","CD-Trvc-Bs-map","CD-Trvc-Bs-map",bytes,cdsPriorities);
			FileUtils.writeByteArrayToFile(new File(TEST_RESOURCES + "test_01" + File.separatorChar + "out_war" + File.separatorChar+"CD-Trvc-Bs-map.war"), cd.getWar());
		}catch( IOException e){
			logger.error("generateCD_09 > " + e.getMessage());
			Assert.assertTrue(false);	
		}
		
	}		
	
	@Test
	public void generateCD_10()  {
		try{
			CDGenerator cdGenerator = new CDGeneratorImpl();
			byte[] bytes = FileUtils.readFileToByteArray(new File(TEST_RESOURCES + "test_01" + File.separatorChar + "in_coord" + File.separatorChar+"CD-Trvc-Dts-trv-acc.coord"));
			CD cd = cdGenerator.generateCoordinationDelegate("wp4","CD-Trvc-Dts-trv-acc","CD-Trvc-Dts-trv-acc",bytes,cdsPriorities);
			FileUtils.writeByteArrayToFile(new File(TEST_RESOURCES + "test_01" + File.separatorChar + "out_war" + File.separatorChar+"CD-Trvc-Dts-trv-acc.war"), cd.getWar());
		}catch( IOException e){
			logger.error("generateCD_10 > " + e.getMessage());
			Assert.assertTrue(false);	
		}
		
	}	
	
}
