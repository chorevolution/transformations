/*
* Copyright 2017 The CHOReVOLUTION project
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package eu.chorevolution.transformations.generativeapproach.bcgenerator;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.junit.Test;

public class GenerateWP4Test {
	
	private static final String DTS_ACCIDENTS_NAME = "bcDTS-Accidents";
	private static final String DTS_ACCIDENTS_INPUT_GIDL_NAME = "dts-accidents.gidl";

	private static final String DTS_BRIDGE_NAME = "bcDTS-Bridge";
	private static final String DTS_BRIDGE_INPUT_GIDL_NAME = "dts-bridge.gidl";

	private static final String DTS_CONGESTION_NAME = "bcDTS-Congestion";
	private static final String DTS_CONGESTION_INPUT_GIDL_NAME = "dts-congestion.gidl";

	private static final String DTS_GOOGLE_NAME = "bcDTS-Google";
	private static final String DTS_GOOGLE_INPUT_GIDL_NAME = "dts-google.gidl";

	private static final String DTS_HERE_NAME = "bcDTS-Here";
	private static final String DTS_HERE_INPUT_GIDL_NAME = "dts-here.gidl";

	private static final String DTS_WEATHER_NAME = "bcDTS-Weather";
	private static final String DTS_WEATHER_INPUT_GIDL_NAME = "dts-weather.gidl";
	
	private static final String WP4_RESOURCES_FOLDER = BCGenerationTest.TEST_RESOURCES + File.separatorChar + "WP4";

	@Test
	public void generateDTSAccidents() {

		try {
			byte[] gidl = FileUtils.readFileToByteArray(new File(WP4_RESOURCES_FOLDER + File.separatorChar + DTS_ACCIDENTS_INPUT_GIDL_NAME));

			BCGenerationTest.testGeneration(DTS_ACCIDENTS_NAME, gidl, BCProtocolType.SOAP);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void generateDTSBridge() {

		try {
			byte[] gidl = FileUtils.readFileToByteArray(new File(WP4_RESOURCES_FOLDER + File.separatorChar + DTS_BRIDGE_INPUT_GIDL_NAME));

			BCGenerationTest.testGeneration(DTS_BRIDGE_NAME, gidl, BCProtocolType.SOAP);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void generateDTSCongestion() {

		try {
			byte[] gidl = FileUtils.readFileToByteArray(new File(WP4_RESOURCES_FOLDER + File.separatorChar + DTS_CONGESTION_INPUT_GIDL_NAME));

			BCGenerationTest.testGeneration(DTS_CONGESTION_NAME, gidl, BCProtocolType.SOAP);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void generateDTSGoogle() {

		try {
			byte[] gidl = FileUtils.readFileToByteArray(new File(WP4_RESOURCES_FOLDER + File.separatorChar + DTS_GOOGLE_INPUT_GIDL_NAME));

			BCGenerationTest.testGeneration(DTS_GOOGLE_NAME, gidl, BCProtocolType.SOAP);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void generateDTSHere() {

		try {
			byte[] gidl = FileUtils.readFileToByteArray(new File(WP4_RESOURCES_FOLDER + File.separatorChar + DTS_HERE_INPUT_GIDL_NAME));

			BCGenerationTest.testGeneration(DTS_HERE_NAME, gidl, BCProtocolType.SOAP);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void generateDTSWeather() {

		try {
			byte[] gidl = FileUtils.readFileToByteArray(new File(WP4_RESOURCES_FOLDER + File.separatorChar + DTS_WEATHER_INPUT_GIDL_NAME));

			BCGenerationTest.testGeneration(DTS_WEATHER_NAME, gidl, BCProtocolType.SOAP);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
