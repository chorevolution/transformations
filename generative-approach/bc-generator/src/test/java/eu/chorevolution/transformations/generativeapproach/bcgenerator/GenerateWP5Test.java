/*
* Copyright 2017 The CHOReVOLUTION project
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package eu.chorevolution.transformations.generativeapproach.bcgenerator;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.junit.Test;

public class GenerateWP5Test {
	
	private static final String JOURNEY_PLANNER_NAME = "bcJourneyPlanner";
	private static final String JOURNEY_PLANNER_INPUT_GIDL_NAME = "bcJourneyPlanner.gidl";

	private static final String NEWS_NAME = "bcNews";
	private static final String NEWS_INPUT_GIDL_NAME = "bcNews.gidl";

	private static final String PARKING_NAME = "bcParking";
	private static final String PARKING_INPUT_GIDL_NAME = "bcParking.gidl";
	
	private static final String POI_NAME = "bcPoi";
	private static final String POI_INPUT_GIDL_NAME = "bcPoi.gidl";

	private static final String PUBLICTRANSPORTATION_NAME = "bcPublicTransportation";	
	private static final String PUBLICTRANSPORTATION_INPUT_GIDL_NAME = "bcPublicTransportation.gidl";

	private static final String PWS_NAME = "bcPersonalWeatherStations";
	private static final String PWS_INPUT_GIDL_NAME = "bcPersonalWeatherStations.gidl";

	private static final String TRAFFIC_NAME = "bcTraffic";
	private static final String TRAFFIC_INPUT_GIDL_NAME = "bcTraffic.gidl";

	private static final String STAPP_NAME = "bcSTApp";
	private static final String STAPP_INPUT_WSDL_NAME = "cdSTApp.wsdl";
	
	private static final String WP5_RESOURCES_FOLDER = BCGenerationTest.TEST_RESOURCES + File.separatorChar + "WP5";

	@Test
	public void generateJourneyPlanner() {

		try {
			byte[] gidl = FileUtils.readFileToByteArray(new File(WP5_RESOURCES_FOLDER + File.separatorChar + JOURNEY_PLANNER_INPUT_GIDL_NAME));

			BCGenerationTest.testGeneration(JOURNEY_PLANNER_NAME, gidl, BCProtocolType.SOAP);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void generateNews() {
		try {
			byte[] gidl = FileUtils.readFileToByteArray(new File(WP5_RESOURCES_FOLDER + File.separatorChar + NEWS_INPUT_GIDL_NAME));

			BCGenerationTest.testGeneration(NEWS_NAME, gidl, BCProtocolType.SOAP);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void generateParking() {
		try {
			byte[] gidl = FileUtils.readFileToByteArray(new File(WP5_RESOURCES_FOLDER + File.separatorChar + PARKING_INPUT_GIDL_NAME));

			BCGenerationTest.testGeneration(PARKING_NAME, gidl, BCProtocolType.SOAP);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void generatePoi() {
		try {
			byte[] gidl = FileUtils.readFileToByteArray(new File(WP5_RESOURCES_FOLDER + File.separatorChar + POI_INPUT_GIDL_NAME));

			BCGenerationTest.testGeneration(POI_NAME, gidl, BCProtocolType.SOAP);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void generatePublicTransportation() {
		try {
			byte[] gidl = FileUtils.readFileToByteArray(new File(WP5_RESOURCES_FOLDER + File.separatorChar + PUBLICTRANSPORTATION_INPUT_GIDL_NAME));

			BCGenerationTest.testGeneration(PUBLICTRANSPORTATION_NAME, gidl, BCProtocolType.SOAP);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void generatePersonalWeatherStations() {
		try {
			byte[] gidl = FileUtils.readFileToByteArray(new File(WP5_RESOURCES_FOLDER + File.separatorChar + PWS_INPUT_GIDL_NAME));

			BCGenerationTest.testGeneration(PWS_NAME, gidl, BCProtocolType.SOAP);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Test
	public void generateTraffic() {
		try {
			byte[] gidl = FileUtils.readFileToByteArray(new File(WP5_RESOURCES_FOLDER + File.separatorChar + TRAFFIC_INPUT_GIDL_NAME));

			BCGenerationTest.testGeneration(TRAFFIC_NAME, gidl, BCProtocolType.SOAP);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}	

	@Test
	public void generateSTApp() {
		try {
			byte[] wsdl = FileUtils.readFileToByteArray(new File(WP5_RESOURCES_FOLDER + File.separatorChar + STAPP_INPUT_WSDL_NAME));

			BCGenerationTest.testGeneration(STAPP_NAME, wsdl, BCProtocolType.REST);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}	
}
